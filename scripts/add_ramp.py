import sys, serial, argparse
from time import sleep

file = sys.argv[1]
ramp = []
f = open(file)
mod_number, prev_point, sig_points = f.readline().split(" ")
mod_number,prev_point,sig_points = [int(mod_number),int(prev_point), int(sig_points)]
print(mod_number,prev_point,sig_points)
for l in f:
	values = l.split(' ')
	values = [int(x) for x in values[:-1]]
	ramp.extend(values)
prev_ramp = ramp[:prev_point]
ramp = ramp[prev_point:]
# ramp.reverse()
print(prev_ramp, len(prev_ramp))
print(ramp, len(ramp))
points = len(ramp)
if points != sig_points or prev_point != len(prev_ramp):
	print("ERROR en el archivo, numero de puntos diferente") 
	print(points)
	sys.exit()

sleep_time = 0.15

ser = serial.Serial('COM7', 230400)

############## CREATE MODULATION ###################
ser.write(("newmodulation "+ str(mod_number) + " "+str(prev_point)+ " " + str(sig_points) +"\n").encode())
print(("newmodulation "+ str(mod_number) + " "+str(prev_point)+ " " + str(sig_points) +"\n"))
sleep(sleep_time)

############## WRITE PREMOD POINTS ###################
for i in range(0, int(prev_point/10)):
	chop = prev_ramp[i*10:i*10+10]
	chop = [str(x) for x in chop]
	chop = " ".join(chop)
	# print(("addpointssig "+ str(mod_number) + " 10 "+chop+"\n"))
	ser.write(("addpointspre "+ str(mod_number) + " 10 "+chop+"\n").encode())
	sleep(sleep_time)
	print(i)

resto = prev_point - 10*int(prev_point/10)
chop = prev_ramp[int(prev_point/10)*10:int(prev_point/10)*10+resto]
chop = [str(x) for x in chop]
chop = " ".join(chop)
if(resto != 0):
	ser.write(("addpointspre "+ str(mod_number) + " " + str(resto)+ " " +chop+"\n").encode())
	sleep(sleep_time)
	
############## WRITE SIGNAL POINTS ###################
for j in range(0, int(points/10)):
	chop = ramp[j*10:j*10+10]
	chop = [str(x) for x in chop]
	chop = " ".join(chop)
	# print(("addpointssig "+ str(mod_number) + " 10 "+chop+"\n"))
	ser.write(("addpointssig "+ str(mod_number) + " 10 "+chop+"\n").encode())
	sleep(sleep_time)
	print(j)

resto = points - 10*int(points/10)
chop = ramp[int(points/10)*10:int(points/10)*10+resto]
chop = [str(x) for x in chop]
chop = " ".join(chop)
ser.write(("addpointssig "+ str(mod_number) + " " + str(resto)+ " " +chop+"\n").encode())
sleep(sleep_time)
# wait = input("PRESS ENTER TO CONTINUE.")

############## ADD and SET MODULATION ###################
ser.write(("addmodulation "+ str(mod_number) + "\n").encode())
sleep(sleep_time)
ser.write(("setmodulation "+ str(mod_number) + "\n").encode())
print("Set modulation")
# wait = input("PRESS ENTER TO CONTINUE.")
ser.close()