%% Generating an n-bit sine wave
% Modifiable parameters: step, bits, offset
close; clear; clc;

points = 288;                            % number of points between sin(0) to sin(2*pi)
bits   = 12;                             % 12-bit sine wave for 12-bit DAC
offset = 500;                             % limiting DAC output voltage

t = 0:((2*pi/(points-1))):(2*pi);        % creating a vector from 0 to 2*pi
t = 0:1/(points-1):1;
y = t;                                  % getting the sine values
% y =  sin(t);
% y = y + 1;                               % getting rid of negative values (shifting up by 1)
y = y*((2^bits-1)-2*offset)+offset;    % limiting the range (0+offset) to (2^bits-offset)
y = round(y);                            % rounding the values
y = fliplr(y);
plot(t, y); grid                         % plotting for visual confirmation

% fprintf('%d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, \n', y);
fprintf('%d ', y)