/*
 * Copyright (c) 2016, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * main_control.h
 *
 *  Created on: 1/6/2016
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@die.upm.es>
 *
 */
/**
 * @file main_control.h
 */

#ifndef APPLICATION_USER_S4BIM_DSP_CODE_PROCESSES_MAIN_CONTROL_H_
#define APPLICATION_USER_S4BIM_DSP_CODE_PROCESSES_MAIN_CONTROL_H_

#include "DSP_conf.h"
#include "state.h"
#include "leds.h"
#include "adc_comm.h"
#include "state.h"
#include "adc_iq_signals.h"
#include "sdram.h"
#include "leds.h"
#include "preprocessing.h"
#include "data_storage.h"

typedef enum {
	NO_ACTION = 65535,
	PROCESS_NEXT = 1,
}action_t;

typedef struct control{
	action_t current_action;
	action_t next_action;
	state_t* adc_state;
	state_t* dsp_state;
	uint64_t last_elapsed_time;
	uint64_t timestamp;
	uint32_t mseconds;
	uint32_t seconds;
	uint32_t minutes;
	uint32_t hours;
	uint32_t days;
	float32_t pos_x;
	float32_t pos_y;
	float32_t pos_z;
	float32_t ori_x;
	float32_t ori_y;
	float32_t ori_z;
	uint32_t radar_id;
	uint32_t sonar_id;
}control_t;

osMessageQId  adc_comm_queue;

control_t* new_control();
retval_t remove_control(control_t* control);

retval_t next_action_decision(control_t* control, command_t* cmd, uint16_t* speed_counter, uint16_t* updown_counter);
retval_t process_next_action(control_t* control, command_t* cmd);

#endif /* APPLICATION_USER_S4BIM_DSP_CODE_PROCESSES_MAIN_CONTROL_H_ */
