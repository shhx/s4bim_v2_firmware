/*
 * Copyright (c) 2016, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * wifi_process.c
 *
 *  Created on: 22/7/2016
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@die.upm.es>
 *
 */
/**
 * @file wifi_process.c
 */


#include "DSP_conf.h"
#include "esp8266_input.h"
#include "wifi_commands.h"

extern output_data_t* output_data;

uint8_t wifi_proc_first = 1;

void wifiCommandReceive_thread(void const * argument){

	input_data_t* input_data = new_input_data(MAX_CMD_INPUT_BUFFER);


	start_esp8266_listening(input_data);

	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_7,  GPIO_PIN_SET);
	osDelay(5000);

	HAL_UART_Transmit(&huart6,(uint8_t*) "&\r\n", strlen("&\r\n"), 50);
	osDelay(100);
	HAL_UART_Transmit(&huart6,(uint8_t*) "&\r\n", strlen("&\r\n"), 50);
	osDelay(100);
	HAL_UART_Transmit(&huart6,(uint8_t*) "&\r\n", strlen("&\r\n"), 50);
	osDelay(100);
	HAL_UART_Transmit(&huart6,(uint8_t*) "dofile(\"script2.lua\");\r\n", strlen("dofile(\"script2.lua\");\r\n"), 50);
//	HAL_UART_Transmit(&huart6,(uint8_t*) "wifi.sta.eventMonStart();\r\n", strlen("wifi.sta.eventMonStart();\r\n"), 50);
	osDelay(100);
//	HAL_UART_Transmit(&huart6,(uint8_t*) "wifi.sta.config(\"B105_net\",\"FiNaO=17\", 1);\r\n", strlen("wifi.sta.config(\"B105_net\",\"FiNaO=17\", 1);\r\n"), 50);
//	HAL_UART_Transmit(&huart6,(uint8_t*) "&\r\n", strlen("&\r\n"), 50);
//	osDelay(100);
//	HAL_UART_Transmit(&huart6,(uint8_t*) "dofile(\"script2.lua\");\r\n", strlen("dofile(\"script2.lua\");\r\n"), 50);

	while(1){

		if((wifi_proc_first) && (output_data->wifi_ap_state == WIFI_AP_CONNECTED)){
			wifi_proc_first = 0;
			HAL_UART_Transmit(&huart6,(uint8_t*) "dofile(\"script1.lua\");\r\n", strlen("dofile(\"script1.lua\");\r\n"), 50);
		}

		get_new_esp8266_data(input_data);
		check_esp8266_newline(input_data);
		select_wifi_command(input_data->command);
		execute_wifi_command(input_data->command);

		osDelay(5);
	}
}
