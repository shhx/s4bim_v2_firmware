/*
 * Copyright (c) 2016, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * postprocess_thread.c
 *
 *  Created on: 5/7/2016
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@die.upm.es>
 *
 */
/**
 * @file postprocess_thread.c
 */

#include "postprocessing.h"
#include "distances_processing.h"
#include "data_storage.h"
#include "final_distances.h"

extern data_storage_t* data_storage;
extern osMutexId  data_storage_mutex_id;
extern osMutexId  postprocessing_mutex_id;
extern postprocessing_block_t* postprocessing_block;

void postprocess_thread(void const * argument){


	while(1){

		osMutexWait (data_storage_mutex_id, osWaitForever);
		data_ready_check(postprocessing_block->distances_processing, data_storage);
		osMutexRelease (data_storage_mutex_id);

		if(postprocessing_block->distances_processing->data_ready_flag != DATA_READY){
			osDelay(2);
		}
		else{


			osMutexWait (postprocessing_mutex_id, osWaitForever);

			round_samples(postprocessing_block->distances_processing, data_storage);

			postprocessing_block->post_os_cfar->post_os_cfar_func_ptr(postprocessing_block->post_os_cfar, postprocessing_block->distances_processing);

			do_post_distances_selection(postprocessing_block->post_distances_selection, postprocessing_block->distances_processing, postprocessing_block->post_os_cfar);

			do_final_distances(postprocessing_block->final_distances, postprocessing_block->post_distances_selection);

			if(postprocessing_block->distances_processing->current_index < data_storage->buffer_size-1){
				postprocessing_block->distances_processing->prev_index = postprocessing_block->distances_processing->current_index + 1;
			}
			else{
				postprocessing_block->distances_processing->prev_index = 0;
			}

			postprocessing_block->distances_processing->output_data_ready_flag = DATA_READY;
			postprocessing_block->distances_processing->data_ready_flag = NO_DATA_READY;

			osMutexRelease (postprocessing_mutex_id);

		}


	}
}
