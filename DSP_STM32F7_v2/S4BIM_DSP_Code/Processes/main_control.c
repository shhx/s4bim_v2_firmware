/*
 * Copyright (c) 2016, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * main_control.c
 *
 *  Created on: 1/6/2016
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@die.upm.es>
 *
 */
/**
 * @file main_control.c
 */

#include "main_control.h"
#include "output_data.h"

extern preprocessing_block_t* preprocessing_block;
extern postprocessing_block_t* postprocessing_block;
extern output_data_t* output_data;
extern data_storage_t* data_storage;

extern osThreadId preprocessingTaskHandle;
extern osMutexId  preprocessing_mutex_id;
extern osMutexId  data_storage_mutex_id;



control_t* new_control(uint32_t delay){
	control_t* new_control = pvPortMalloc(sizeof(control_t));
	new_control->current_action = NO_ACTION;
	new_control->next_action = NO_ACTION;
	new_control->adc_state = new_state();
	new_control->dsp_state = new_state();
	new_control->last_elapsed_time = 0;
	new_control->timestamp = 0;
	new_control->mseconds = 0;
	new_control->seconds = 0;
	new_control->minutes = 0;
	new_control->hours = 0;
	new_control->days = 0;

	new_control->pos_x = 1;
	new_control->pos_y = 1;
	new_control->pos_z = 1;
	new_control->ori_x = 1;
	new_control->ori_y = 1;
	new_control->ori_z = 1;

	new_control->radar_id = 3;
	new_control->sonar_id = 2;

	return new_control;
}
retval_t remove_control(control_t* control){
	if(control != NULL){
		remove_state(control->adc_state);
		remove_state(control->dsp_state);
		vPortFree(control);
		return RET_OK;
	}
	else{
		return RET_ERROR;
	}
}


retval_t process_next_action(control_t* control, command_t* cmd){

	// SE PIDEN GENERAR Y ENVIAR LAS SIGUIENTES MUESTRAS IQ//
	write_command_to_spi(cmd, control->adc_state);
	read_command_from_spi(cmd, control->dsp_state);

	if(cmd->command == ACK_NEXT){
		preprocessing_block->adc_iq_signal->adc_sample_number = cmd->params[0];
		preprocessing_block->adc_iq_signal->guard_samples = cmd->params[1];


		//AQUI SE REALIZA EL PROCESAMIENTO DE LA ANTERIOR SE�AL//
		//Se activa la se�al de procesamiento
		osThreadResume(preprocessingTaskHandle);				//Highest priority thread. None interrupt processing

		osMutexWait (preprocessing_mutex_id, osWaitForever);
		outputData(output_data, preprocessing_block, control, postprocessing_block);

		osMutexWait (data_storage_mutex_id, osWaitForever);
		store_data(data_storage, preprocessing_block, control->timestamp);
		osMutexRelease (data_storage_mutex_id);

		osMutexRelease (preprocessing_mutex_id);

		osDelay(preprocessing_block->delay);

		//A CONTINUACI�N SE RECIBEN LAS SIGUIENTES SE�ALES IQ//
		receive_iq_signal(preprocessing_block->adc_iq_signal);

		cmd->command = ACK;
		write_command_to_spi(cmd, control->adc_state);


	}

	return RET_OK;
}



retval_t next_action_decision(control_t* control, command_t* cmd, uint16_t* speed_counter, uint16_t* updown_counter){

	uint8_t speed_flag = 0;
	uint8_t down_flag = 0;

	control->current_action = control->next_action;
	preprocessing_block->modulation_schemes->current_scheme = preprocessing_block->modulation_schemes->next_scheme;

	cmd->command = SET_CURRENT_AND_NEXT;

	uint16_t speed_calc_rate;
	uint16_t updown_calc_rate;
	//Decide the next scheme to generate

	osMutexWait (preprocessing_mutex_id, osWaitForever);
	speed_calc_rate = preprocessing_block->speed_calc_rate;
	updown_calc_rate = preprocessing_block->updown_calc_rate;
	osMutexRelease (preprocessing_mutex_id);

	//CHECK SPEED CONSTANT MODULATION//
	if(speed_calc_rate == 0){
		speed_flag = 0;
		*speed_counter = 0;
	}

	else{
		if((*speed_counter) < (uint16_t) (100/speed_calc_rate)){
			speed_flag = 0;
		}
		else{
			speed_flag = 1;
			*speed_counter = 0;
		}

	}

	(*speed_counter)++;

	if(speed_flag){
		cmd->params[0] = 2;
		osMutexWait (preprocessing_mutex_id, osWaitForever);
		preprocessing_block->modulation_schemes->next_scheme = 2;
		osMutexRelease (preprocessing_mutex_id);
		return RET_OK;
	}



	//CHECK UP OR DOWN RAMP//
	if(updown_calc_rate == 0){
		down_flag = 0;
		*updown_counter = 0;
	}

	else{
		if((*updown_counter) < (uint16_t) (100/updown_calc_rate)){
			down_flag = 0;
		}
		else{
			down_flag = 1;
			*updown_counter = 0;
		}

	}


	(*updown_counter)++;


	if(down_flag){
		cmd->params[0] = 1;
		osMutexWait (preprocessing_mutex_id, osWaitForever);
		preprocessing_block->modulation_schemes->next_scheme = 1;
		osMutexRelease (preprocessing_mutex_id);
	}
	else{
		cmd->params[0] = 0;
		osMutexWait (preprocessing_mutex_id, osWaitForever);
		preprocessing_block->modulation_schemes->next_scheme = 0;
		osMutexRelease (preprocessing_mutex_id);
	}
	return RET_OK;

}
