/*
 * Copyright (c) 2016, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * main_process.c
 *
 *  Created on: 18/5/2016
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@die.upm.es>
 *
 */
/**
 * @file main_process.c
 */

#include "DSP_conf.h"
#include "gpio.h"
#include "adc_comm.h"
#include "state.h"
#include "adc_iq_signals.h"
#include "sdram.h"
#include "leds.h"
#include "preprocessing.h"
#include "main_control.h"
#include "time.h"
#include "output_data.h"
#include "data_storage.h"
#include <stdio.h>

#define	DEFAULT_ADC_SAMPLES				256
#define	DEFAULT_FFT_SAMPLES				1024
#define DEFAULT_EFFECTIVE_FREQ_SAMPLES	160

#define DEFAULT_DATA_STORAGE_SIZE		128

osThreadId preprocessingTaskHandle;
osThreadId commandReceiveTaskHandle;
osThreadId sonarTaskHandle;
osThreadId postprocessTaskHandle;
osThreadId temperatureTaskHandle;
osThreadId wifiCommandReceiveTaskHandle;

void preprocessing_thread(void const * argument);
void commandReceive_thread(void const * argument);
void wifiCommandReceive_thread(void const * argument);
void sonar_thread(void const * argument);
void postprocess_thread(void const * argument);
void temperature_thread(void const * argument);

//Declaro los mutexes pero de momento no se usan, ya que en principio no deberia haber condiciones de carrera
osMutexDef (preprocessing_mutex);   // Declare mutex
osMutexId  preprocessing_mutex_id;	// Mutex ID

osMutexDef (data_storage_mutex);   // Declare mutex
osMutexId  data_storage_mutex_id;	// Mutex ID

osMutexDef (postprocessing_mutex);   // Declare mutex
osMutexId  postprocessing_mutex_id;	// Mutex ID

osMutexDef (main_control_mutex);   // Declare mutex
osMutexId  main_control_mutex_id;	// Mutex ID

osMutexDef (uart6_mutex);			// Declare mutex
osMutexId  uart6_mutex_id;			// Mutex ID

preprocessing_block_t* preprocessing_block;
postprocessing_block_t* postprocessing_block;

output_data_t* output_data;

data_storage_t* data_storage;

control_t* control;

void sonar_pin_callback(void);
void adc_comm_pin_callback(void);
void adc_comm_spi_callback(void);
void geth_comm_pin_callback(void);
void geth_comm_spi_callback(void);
void temperature_callback(void);

void main_control_process(void){

	/*//////////////////////INIT//////////////////////////////*/
	FMC_SDRAM_CommandTypeDef sdram_command;

	leds_init();
	HAL_TIM_Base_Start(&htim2);									//INIT TIMER 2 Used for measuring times
	SDRAM_Initialization_Sequence(&hsdram2, &sdram_command);	//Initialize SDRAM for future use

	control = new_control();						//Create the control block structure
	control->dsp_state->state_0 = S1_IDLE;
	main_control_mutex_id = osMutexCreate(osMutex(main_control_mutex));

	preprocessing_block = new_preprocessing_block(DEFAULT_ADC_SAMPLES, DEFAULT_FFT_SAMPLES, DEFAULT_EFFECTIVE_FREQ_SAMPLES, 0);
	preprocessing_mutex_id = osMutexCreate(osMutex(preprocessing_mutex));

	postprocessing_block = new_postprocessing_block();
	postprocessing_mutex_id = osMutexCreate(osMutex(postprocessing_mutex));

	uart6_mutex_id = osMutexCreate(osMutex(uart6_mutex));

	output_data = new_output_data(FREQ, UART);
	//output_data = new_output_data(DISTANCES, WIFI);//WTFFFF

	data_storage = new_data_storage(DEFAULT_DATA_STORAGE_SIZE);
	data_storage_mutex_id = osMutexCreate(osMutex(data_storage_mutex));

	leds_toggle(LEDS_GREEN);								//On Green LED; BLUE LED only used for failure

	//Create the necessary threads
	osThreadDef(preprocessingTask, preprocessing_thread, osPriorityRealtime, 0, 384);			//Preprocessing Task
	preprocessingTaskHandle = osThreadCreate(osThread(preprocessingTask), NULL);

	osThreadDef(commandReceiveTask, commandReceive_thread, osPriorityNormal, 0, 384);			//Receive Commands task
	commandReceiveTaskHandle = osThreadCreate(osThread(commandReceiveTask), NULL);

	osThreadDef(wifiCommandReceiveTask, wifiCommandReceive_thread, osPriorityNormal, 0, 384);	//Wifi Receive Commands task
	wifiCommandReceiveTaskHandle = osThreadCreate(osThread(wifiCommandReceiveTask), NULL);

	osThreadDef(sonarTask, sonar_thread, osPriorityNormal, 0, 128);								//Sonar task
	sonarTaskHandle = osThreadCreate(osThread(sonarTask), NULL);

	osThreadDef(postprocessTask, postprocess_thread, osPriorityBelowNormal, 0, 384);			//Post Process task
	postprocessTaskHandle = osThreadCreate(osThread(postprocessTask), NULL);

	osThreadDef(temperatureTask, temperature_thread, osPriorityBelowNormal, 0, 128);			//Temperature task
	temperatureTaskHandle = osThreadCreate(osThread(temperatureTask), NULL);

	osSemaphoreDef(adc_comm_semaphore);
	adc_comm_semaphore = osSemaphoreCreate(osSemaphore(adc_comm_semaphore), 1);
	osSemaphoreWait (adc_comm_semaphore, osWaitForever);		//Wait for the semaphore		//Initially the semaphore token is captured


	osSemaphoreDef(geth_comm_semaphore);
	geth_comm_semaphore = osSemaphoreCreate(osSemaphore(geth_comm_semaphore), 1);
	osSemaphoreWait (geth_comm_semaphore, osWaitForever);		//Wait for the semaphore		//Initially the semaphore token is captured

	control->dsp_state->state_0 = S1_RUNNING;
	control->next_action = PROCESS_NEXT;

	command_t* cmd = new_command(CURRENT_NEXT,0, 0, 0);
	uint16_t speed_counter = 1;
	uint16_t updown_counter = 1;

	uint32_t t1 = 0;
	uint32_t t2 = 0;

	/* ///////////////////END INIT////////////////////// */

	while(1){

		osMutexWait (main_control_mutex_id, osWaitForever);
		t2 = __HAL_TIM_GetCounter(&htim2);
		control->last_elapsed_time = (uint64_t) (t2-t1);
		__HAL_TIM_SetCounter(&htim2,0);
		t1 = __HAL_TIM_GetCounter(&htim2);

		control->timestamp +=  control->last_elapsed_time;

		control->mseconds =(uint32_t)  ((control->timestamp/1000)%100);
		control->seconds =(uint32_t)  ((control->timestamp/1000000)%60);
		control->minutes =(uint32_t)  ((control->timestamp/60000000)%60);
		control->hours =(uint32_t)  ((control->timestamp/3600000000)%24);
		control->days =(uint32_t)  ((control->timestamp/86400000000)%365);
		osMutexRelease (main_control_mutex_id);

		size_t size = xPortGetFreeHeapSize();

		next_action_decision(control, cmd, &speed_counter, &updown_counter);				//Decide which one is the next action to execute

		switch(control->current_action){	//Execute the action
		case NO_ACTION:
			break;
		case PROCESS_NEXT:
			process_next_action(control, cmd);
			break;
		default:
			break;
		}

	}
}


void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin){
	if(GPIO_Pin == GPIO_PIN_3){
		sonar_pin_callback();
	}

	if(GPIO_Pin == GPIO_PIN_13){
		adc_comm_pin_callback();
	}

	if(GPIO_Pin == GPIO_PIN_8){
		geth_comm_pin_callback();
	}
}

void HAL_SPI_ErrorCallback(SPI_HandleTypeDef *hspi){
	leds_on(LEDS_BLUE);
	while(1);
}

void HAL_SPI_TxRxCpltCallback(SPI_HandleTypeDef *hspi){
	if(hspi->Instance == SPI1){
		adc_comm_spi_callback();
	}
	if(hspi->Instance == SPI4){
		geth_comm_spi_callback();
	}
}

void  HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* hadc){
	if (hadc->Instance == ADC1){
		temperature_callback();
	}
}
