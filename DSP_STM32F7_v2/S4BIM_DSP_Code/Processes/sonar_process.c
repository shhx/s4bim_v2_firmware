/*
 * Copyright (c) 2016, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * sonar_process.c
 *
 *  Created on: 27/6/2016
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@die.upm.es>
 *
 */
/**
 * @file sonar_process.c
 */


#include "DSP_conf.h"
#include "tim.h"
#include "time.h"
#include "arm_math.h"

uint32_t time1= 0;
uint32_t time2= 0;


osMutexDef (sonar_mutex);   // Declare mutex
osMutexId  sonar_mutex_id;	// Mutex ID
float32_t sonar_dist = 0;
float32_t sonar_cal = 1;

void sonar_thread(void const * argument){

	sonar_mutex_id = osMutexCreate(osMutex(sonar_mutex));

	HAL_TIM_Base_Start(&htim5);									//INIT TIMER 5 Used for us delays

	uint32_t time = 0;

	while(1){

		HAL_GPIO_WritePin(GPIOD, GPIO_PIN_13, GPIO_PIN_SET);		//Start 10uS pulse
		delay_us(15);
		HAL_GPIO_WritePin(GPIOD, GPIO_PIN_13, GPIO_PIN_RESET);		//Stop 10uS pulse

		osDelay(250);
		time = time2 - time1;
		osMutexWait (sonar_mutex_id, osWaitForever);
		sonar_dist = (float32_t) ((float32_t)time/5800);
		sonar_dist = sonar_dist *sonar_cal;
		if(sonar_dist > 5){
			sonar_dist = -1;
		}
		osMutexRelease (sonar_mutex_id);
	}
}


void sonar_pin_callback(void){
	if (HAL_GPIO_ReadPin(GPIOD, GPIO_PIN_3) == GPIO_PIN_SET){
		time1 = timer_now();
	}
	if (HAL_GPIO_ReadPin(GPIOD, GPIO_PIN_3) == GPIO_PIN_RESET){
		time2 = timer_now();
	}
}
