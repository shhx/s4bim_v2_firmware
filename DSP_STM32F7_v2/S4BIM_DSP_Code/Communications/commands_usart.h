/*
 * commands_usart.h
 *
 *  Created on: 4 jun. 2017
 *      Author: LuisAlberto
 */

#ifndef APPLICATION_USER_S4BIM_DSP_CODE_COMMUNICATIONS_COMMANDS_USART_H_
#define APPLICATION_USER_S4BIM_DSP_CODE_COMMUNICATIONS_COMMANDS_USART_H_

#include "types.h"
#include "usart.h"
#include "modulation.h"
#include "gpio.h"
//#include "cmsis_os.h"

#define UART_DELAY 1

/**
 * @brief	Commands available to be sent or received by the ADC uC
 */
typedef enum {
	SET_VCO_LIMIT,	 	 //!< Command set the DAC output range voltage
	SET_MODULATION,
	ADD_CHANNEL,
	RMV_CHANNEL,
	SET_GAIN,
	SET_FILTER_FREQ,
	SET_FILTER_TYPE,
	SET_ADC_TYPE,
	ADD_POINTS_MOD,
	ADD_POINTS_MOD_PRE,
	ADD_MODULATION,
	NEW_MODULATION,
	TEST = 254,
	NOPP = 255,
} commands_usart;

/**
 * @brief	Command packet struct. It contains a commands and 6 bytes of params that could be sent with the command. It has the same size (8) as state_t
 */
typedef struct command_usart {
	commands_usart command;	//!< Current command stored in the packet
	uint8_t size;			//!< size of the data that can be sent or received in a command packet
	void* data;				//!< data that will be sent or received in a command packet
} command_usart_t;

typedef struct channel_config {
	uint8_t number;
	void* config;
} channel_config_t;

typedef struct filter_config {			//todo tipos
	uint8_t type;					//!< Filter used
	float32_t filter_value;			//!< Filter frequency
} filter_config_t;

command_usart_t* new_command_usart(commands_usart command, void* data);
retval_t remove_command_usart(command_usart_t* cmd);
retval_t write_command_usart(command_usart_t* cmd);

#endif /* APPLICATION_USER_S4BIM_DSP_CODE_COMMUNICATIONS_COMMANDS_USART_H_ */
