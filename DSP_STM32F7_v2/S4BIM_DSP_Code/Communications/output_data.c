/*
 * Copyright (c) 2016, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * output_data.c
 *
 *  Created on: 1/6/2016
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@die.upm.es>
 *
 */
/**
 * @file output_data.c
 */

#include "output_data.h"

#define DEFAULT_GETH_SEM_WAIT	3000

retval_t uart_null_output(struct output_data* output_data, preprocessing_block_t* preprocessing_block, control_t* main_control, postprocessing_block_t* postprocessing_block);
retval_t uart_output(struct output_data* output_data, preprocessing_block_t* preprocessing_block, control_t* main_control, postprocessing_block_t* postprocessing_block);
retval_t wifi_output(struct output_data* output_data, preprocessing_block_t* preprocessing_block, control_t* main_control, postprocessing_block_t* postprocessing_block);

retval_t spi_null_output(struct output_data* output_data, preprocessing_block_t* preprocessing_block, control_t* main_control, postprocessing_block_t* postprocessing_block);
retval_t spi_output(struct output_data* output_data, preprocessing_block_t* preprocessing_block, control_t* main_control, postprocessing_block_t* postprocessing_block);

retval_t create_distances_data_packet(output_data_t* output_data, preprocessing_block_t* preprocessing_block, control_t* main_control, postprocessing_block_t* postprocessing_block);
retval_t remove_distances_data_packet(output_data_t* output_data);

extern osMutexId  postprocessing_mutex_id;
extern osMutexId  sonar_mutex_id;
extern osMutexId  temperature_mutex_id;
extern osMutexId  main_control_mutex_id;
extern osMutexId  uart6_mutex_id;

extern float32_t sonar_dist;
extern float32_t temperature;
output_data_t* new_output_data(output_data_mode_t output_data_mode, output_data_interface_t output_data_interface){

	output_data_t* new_output_data = (output_data_t*) pvPortMalloc(sizeof(output_data_t));
	new_output_data->output_data_interface = output_data_interface;
	new_output_data->output_data_mode = output_data_mode;

	new_output_data->wifi_ap_state = WIFI_AP_DISCONNECTED;
	new_output_data->wifi_output_state = WIFI_OUTPUT_STOPPED;

	switch(output_data_interface){
	case NONE:
		new_output_data->uart_output_func = uart_null_output;
		new_output_data->spi_output_func = spi_null_output;
		break;
	case UART:
		new_output_data->uart_output_func = uart_output;
		new_output_data->spi_output_func = spi_null_output;
		break;
	case SPI:
		new_output_data->uart_output_func = uart_null_output;
		new_output_data->spi_output_func = spi_output;
		break;
	case SPI_UART:
		new_output_data->uart_output_func = uart_output;
		new_output_data->spi_output_func = spi_output;
		break;
	case WIFI:
		new_output_data->uart_output_func = wifi_output;
		new_output_data->spi_output_func = spi_null_output;
		break;
	default:
		new_output_data->uart_output_func = uart_null_output;
		new_output_data->spi_output_func = spi_null_output;
		break;
	}

	return new_output_data;
}
retval_t remove_output_data(output_data_t* output_data){
	if(output_data != NULL){
		vPortFree(output_data);
		return RET_OK;
	}
	else{
		return RET_ERROR;
	}
}

retval_t change_outputData_interface(output_data_t* output_data, output_data_interface_t new_output_data_interface){
	if(output_data != NULL){

		output_data->output_data_interface = new_output_data_interface;


		switch(new_output_data_interface){
		case NONE:
			output_data->uart_output_func = uart_null_output;
			output_data->spi_output_func = spi_null_output;
			break;
		case UART:
			output_data->uart_output_func = uart_output;
			output_data->spi_output_func = spi_null_output;
			break;
		case SPI:
			output_data->uart_output_func = uart_null_output;
			output_data->spi_output_func = spi_output;
			break;
		case SPI_UART:
			output_data->uart_output_func = uart_output;
			output_data->spi_output_func = spi_output;
			break;
		case WIFI:
			output_data->uart_output_func = wifi_output;
			output_data->spi_output_func = spi_null_output;
			break;
		default:
			output_data->uart_output_func = uart_null_output;
			output_data->spi_output_func = spi_null_output;
			break;
		}

		return RET_OK;
	}
	else{
		return RET_ERROR;
	}
}


retval_t outputData(output_data_t* output_data, preprocessing_block_t* preprocessing_block, control_t* main_control, postprocessing_block_t* postprocessing_block){

	output_data->uart_output_func(output_data, preprocessing_block, main_control, postprocessing_block);
	output_data->spi_output_func(output_data, preprocessing_block, main_control, postprocessing_block);

	return RET_OK;
}


retval_t uart_null_output(struct output_data* output_data, preprocessing_block_t* preprocessing_block, control_t* main_control, postprocessing_block_t* postprocessing_block){
	return RET_OK;
}

retval_t spi_null_output(struct output_data* output_data, preprocessing_block_t* preprocessing_block, control_t* main_control, postprocessing_block_t* postprocessing_block){
	return RET_OK;
}

retval_t uart_output(struct output_data* output_data, preprocessing_block_t* preprocessing_block, control_t* main_control, postprocessing_block_t* postprocessing_block){

	char* outstr;
	static uint64_t t1, t2;
	uint16_t numTargets;
	uint16_t i;

	switch(output_data->output_data_mode){

	case DISTANCES:

		numTargets = preprocessing_block->distances_selection->current_num_distances;
		outstr = (char*) pvPortMalloc(MAX_UART_PACKET_SIZE*sizeof(char));

		sprintf(outstr, "#\r\n");
		HAL_UART_Transmit(&huart2,(uint8_t*) outstr, strlen(outstr), 50);
		sprintf(outstr, "%d\r\n",(int) numTargets);
		HAL_UART_Transmit(&huart2,(uint8_t*) outstr, strlen(outstr), 50);

		for(i=0; i<numTargets; i++){

		sprintf(outstr, "%.2f\r\n", preprocessing_block->distances_selection->distances[i]);
		HAL_UART_Transmit(&huart2,(uint8_t*) outstr, strlen(outstr), 50);
		sprintf(outstr, "%.2f\r\n", preprocessing_block->distances_selection->distances_levels[i]);
		HAL_UART_Transmit(&huart2,(uint8_t*) outstr, strlen(outstr), 50);
		sprintf(outstr, "%.2f\r\n", preprocessing_block->distances_selection->distances_snr[i]);
		HAL_UART_Transmit(&huart2,(uint8_t*) outstr, strlen(outstr), 50);

		}

		osMutexWait (sonar_mutex_id, osWaitForever);
		float32_t sonar = sonar_dist;
		osMutexRelease (sonar_mutex_id);
		sprintf(outstr, "%.2f\r\n", sonar);
		HAL_UART_Transmit(&huart2,(uint8_t*) outstr, strlen(outstr), 50);

		sprintf(outstr, "%d\r\n",(int) (main_control->last_elapsed_time));
		HAL_UART_Transmit(&huart2,(uint8_t*) outstr, strlen(outstr), 50);

		vPortFree(outstr);

		break;

	case FREQ:

		outstr = (char*) pvPortMalloc(MAX_UART_PACKET_SIZE*sizeof(char));

		sprintf(outstr, "$\r\n");
		HAL_UART_Transmit(&huart2,(uint8_t*) outstr, strlen(outstr), 50);		//SEND END OF FRAME SYMBOL

		sprintf(outstr, "%d\r\n",(int) preprocessing_block->peaks_selection->current_num_peaks);
		HAL_UART_Transmit(&huart2,(uint8_t*) outstr, strlen(outstr), 50);		//SEND NUMBER OF PEAKS

		for(i=0; i<preprocessing_block->peaks_selection->current_num_peaks;i++){									//SEND PEAK POSITIONS
			sprintf(outstr, "%d\r\n",(int) preprocessing_block->peaks_selection->peak_positions[i]);
			HAL_UART_Transmit(&huart2,(uint8_t*) outstr, strlen(outstr), 50);

			sprintf(outstr, "%.2f\r\n", preprocessing_block->peaks_selection->peak_levels[i]);
			HAL_UART_Transmit(&huart2,(uint8_t*) outstr, strlen(outstr), 50);
		}

		//SEND EFFECTIVE SAMPLE NUMBER
		sprintf(outstr, "%d\r\n", preprocessing_block->freq_valid_samples);
		HAL_UART_Transmit(&huart2,(uint8_t*) outstr, strlen(outstr), 50);

		//SEND NOISE
		sprintf(outstr, "%.2f\r\n", preprocessing_block->snr->noise_lvl);
		HAL_UART_Transmit(&huart2,(uint8_t*) outstr, strlen(outstr), 50);

		for(i=0; i<preprocessing_block->snr->effective_sample_number;i++){				//SEND SNR
			sprintf(outstr, "%.2f\r\n", preprocessing_block->snr->snr_signal[i]);
			HAL_UART_Transmit(&huart2,(uint8_t*) outstr, strlen(outstr), 50);
		}

		for(i=0; i<preprocessing_block->fft_block->effective_sample_number;i++){				//SEND SIGNAL
			sprintf(outstr, "%.2f\r\n", preprocessing_block->fft_block->fft_mag_output_ordered_signal[i]);
			HAL_UART_Transmit(&huart2,(uint8_t*) outstr, strlen(outstr), 50);
		}

		for(i=0; i<preprocessing_block->os_cfar->effective_sample_number;i++){				//SEND THREESHOLD
			sprintf(outstr, "%.2f\r\n", preprocessing_block->os_cfar->signal_threshold_output[i]);
			HAL_UART_Transmit(&huart2,(uint8_t*) outstr, strlen(outstr), 50);
		}

		sprintf(outstr, "%d\r\n", (int) (main_control->last_elapsed_time));
		HAL_UART_Transmit(&huart2,(uint8_t*) outstr, strlen(outstr), 50);

		vPortFree(outstr);
		leds_toggle(LEDS_GREEN);

		break;
	case TIME:
		break;
	case POST_PROC_DIST_SIGNAL:

		osMutexWait (postprocessing_mutex_id, osWaitForever);

		if(postprocessing_block->distances_processing->output_data_ready_flag == DATA_READY){

			postprocessing_block->distances_processing->output_data_ready_flag = NO_DATA_READY;

			outstr = (char*) pvPortMalloc(MAX_UART_PACKET_SIZE*sizeof(char));

			sprintf(outstr, "&\r\n");
			HAL_UART_Transmit(&huart2,(uint8_t*) outstr, strlen(outstr), 50);		//SEND END OF FRAME SYMBOL


			//SEND SAMPLE NUMBER
			sprintf(outstr, "%d\r\n", postprocessing_block->distances_processing->num_distance_signal_samples);
			HAL_UART_Transmit(&huart2,(uint8_t*) outstr, strlen(outstr), 50);


			for(i=0; i<postprocessing_block->distances_processing->num_distance_signal_samples;i++){				//SEND DISTANCE SIGNAL
				sprintf(outstr, "%d\r\n", postprocessing_block->distances_processing->clustered_distances_signal[i]);
				HAL_UART_Transmit(&huart2,(uint8_t*) outstr, strlen(outstr), 50);
			}

			for(i=0; i<postprocessing_block->post_os_cfar->effective_sample_number;i++){							//SEND THREESHOLD
				sprintf(outstr, "%.2f\r\n", postprocessing_block->post_os_cfar->signal_threshold_output[i]);
				HAL_UART_Transmit(&huart2,(uint8_t*) outstr, strlen(outstr), 50);
			}

			//SEND NUM DISTANCES
			sprintf(outstr, "%d\r\n", postprocessing_block->final_distances->current_num_distances);
			HAL_UART_Transmit(&huart2,(uint8_t*) outstr, strlen(outstr), 50);

			for(i=0; i<postprocessing_block->final_distances->current_num_distances; i++){				//SEND DISTANCES
				sprintf(outstr, "%.1f\r\n", postprocessing_block->final_distances->final_distances[i]);
				HAL_UART_Transmit(&huart2,(uint8_t*) outstr, strlen(outstr), 50);
			}

			osMutexWait (sonar_mutex_id, osWaitForever);
			float32_t sonar = sonar_dist;
			osMutexRelease (sonar_mutex_id);
			sprintf(outstr, "%.2f\r\n", sonar);
			HAL_UART_Transmit(&huart2,(uint8_t*) outstr, strlen(outstr), 50);

			t2 = main_control->timestamp;
			sprintf(outstr, "%d\r\n", (int) (t2-t1));
			HAL_UART_Transmit(&huart2,(uint8_t*) outstr, strlen(outstr), 50);
			t1 = main_control->timestamp;

			leds_toggle(LEDS_GREEN);
			vPortFree(outstr);
		}
		osMutexRelease (postprocessing_mutex_id);

		break;
	default:
		break;
	}

	return RET_OK;
}


retval_t spi_output(struct output_data* output_data, preprocessing_block_t* preprocessing_block, control_t* main_control, postprocessing_block_t* postprocessing_block){


	switch(output_data->output_data_mode){

	case DISTANCES:
		osMutexWait (postprocessing_mutex_id, osWaitForever);

		if(postprocessing_block->distances_processing->output_data_ready_flag == DATA_READY){

			postprocessing_block->distances_processing->output_data_ready_flag = NO_DATA_READY;
			create_distances_data_packet(output_data, preprocessing_block, main_control, postprocessing_block);

			osMutexRelease (postprocessing_mutex_id);

			while(HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_8) != GPIO_PIN_RESET){
				if(osSemaphoreWait (geth_comm_semaphore, DEFAULT_GETH_SEM_WAIT) == osErrorOS){
					goto END;
					break;
				}
			}

			HAL_SPI_Transmit(&hspi4, output_data->output_data_dist_packet, SPI_DATA_PACKET_SIZE, 4);


			while(HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_8) != GPIO_PIN_SET){
				if(osSemaphoreWait (geth_comm_semaphore, DEFAULT_GETH_SEM_WAIT) == osErrorOS){
					break;
				}
			}
END:		remove_distances_data_packet(output_data);

			leds_toggle(LEDS_GREEN);
		}
		else{
			osMutexRelease (postprocessing_mutex_id);
		}


		break;

	case FREQ:
		break;
	case TIME:
		break;
	default:
		break;
}
	return RET_OK;

}



retval_t wifi_output(struct output_data* output_data, preprocessing_block_t* preprocessing_block, control_t* main_control, postprocessing_block_t* postprocessing_block){

//	static uint64_t t1, t2;
	char* outstr;
	char* outstr2;
	char* current_ptr;
	uint16_t i;

	switch(output_data->output_data_mode){

	case DISTANCES:


		osMutexWait (postprocessing_mutex_id, osWaitForever);

		if(postprocessing_block->distances_processing->output_data_ready_flag == DATA_READY){

			postprocessing_block->distances_processing->output_data_ready_flag = NO_DATA_READY;

			if((output_data->wifi_ap_state == WIFI_AP_CONNECTED)){

				if((output_data->wifi_output_state == WIFI_OUTPUT_STARTED)){

					outstr = (char*) pvPortMalloc(MAX_UART_PACKET_SIZE*sizeof(char));
					outstr2 = (char*) pvPortMalloc(MAX_UART_PACKET_SIZE*sizeof(char));

					current_ptr = outstr;

					sprintf(current_ptr, "$#");			//START OF FRAME
					current_ptr += strlen(current_ptr);

					osMutexWait (main_control_mutex_id, osWaitForever);

					sprintf(current_ptr, "%d#%d#",(int) main_control->sonar_id,(int) main_control->radar_id);
					current_ptr += strlen(current_ptr);

					sprintf(current_ptr, "%.3f#%.3f#%.3f#",(float32_t) main_control->pos_x,(float32_t) main_control->pos_y,(float32_t) main_control->pos_z);
					current_ptr += strlen(current_ptr);
					sprintf(current_ptr, "%.3f#%.3f#%.3f#",(float32_t) main_control->ori_x,(float32_t) main_control->ori_y,(float32_t) main_control->ori_z);
					current_ptr += strlen(current_ptr);

					sprintf(current_ptr, "%u#%u#", (uint32_t)(main_control->timestamp/1000000), (uint32_t)(main_control->timestamp - ((main_control->timestamp/1000000)*1000000)));
					current_ptr += strlen(current_ptr);

					sprintf(current_ptr, "%d:%d:%d#", (int)  main_control->hours, (int)  main_control->minutes, (int)  main_control->seconds);
					current_ptr += strlen(current_ptr);

					osMutexRelease (main_control_mutex_id);


					osMutexWait (sonar_mutex_id, osWaitForever);
					float32_t sonar = sonar_dist;
					osMutexRelease (sonar_mutex_id);

					sprintf(current_ptr, "%.2f#", sonar);
					current_ptr += strlen(current_ptr);

					osMutexWait (temperature_mutex_id, osWaitForever);
					float32_t tempt = temperature;
					osMutexRelease (temperature_mutex_id);

					sprintf(current_ptr, "%.2f#", tempt);
					current_ptr += strlen(current_ptr);

					//SEND NUM DISTANCES
					sprintf(current_ptr, "%d#", postprocessing_block->final_distances->current_num_distances);
					current_ptr += strlen(current_ptr);

					for(i=0; i<postprocessing_block->final_distances->current_num_distances; i++){
						sprintf(current_ptr, "%.1f#", postprocessing_block->final_distances->final_distances[i]);
						current_ptr += strlen(current_ptr);

						sprintf(current_ptr, "%.1f#", postprocessing_block->final_distances->final_levels[i]);
						current_ptr += strlen(current_ptr);

						sprintf(current_ptr, "%.1f#", postprocessing_block->distances_processing->noise_lvl);
						current_ptr += strlen(current_ptr);
					}

					sprintf(current_ptr, "\r");

					osMutexWait (uart6_mutex_id, osWaitForever);
					HAL_UART_Transmit(&huart6,(uint8_t*) outstr, strlen(outstr), 50);
					osMutexRelease (uart6_mutex_id);

					leds_toggle(LEDS_GREEN);
					vPortFree(outstr);
					vPortFree(outstr2);
				}

				else{
	//				HAL_UART_Transmit(&huart6,"$#\r\n", 4, 50);
					leds_toggle(LEDS_GREEN);
				}


			}

			else{
				leds_off(LEDS_GREEN);
			}
			osMutexRelease (postprocessing_mutex_id);

		}
		else{
			osMutexRelease (postprocessing_mutex_id);
		}


		break;

	case FREQ:
		break;
	case TIME:
		break;
	default:
		break;
}
	return RET_OK;

}



retval_t create_distances_data_packet(output_data_t* output_data, preprocessing_block_t* preprocessing_block, control_t* main_control, postprocessing_block_t* postprocessing_block){

	uint16_t i;

	uint16_t numTargets = postprocessing_block->final_distances->current_num_distances;

	uint16_t numBytesSend = 6 + 1 + (numTargets*3*4) + 4 + 4 + 8;		//Preamb; numbytes; sonar; temp; time; Num targets; distance,lvl,snr each target;

	uint16_t numByte = 0;

	output_data->output_data_dist_packet = (uint8_t*) pvPortMalloc(SPI_DATA_PACKET_SIZE*sizeof(uint8_t));

	uint8_t*  dataPacket =  output_data->output_data_dist_packet;

	for(i=0; i<SPI_DATA_PACKET_SIZE; i++){
		dataPacket[i] = 0xAA;
	}

//	uint8_t *sendDistances  = (uint8_t *)preprocessing_block->distances_selection->distances;
//
//	uint8_t *sendSnrDistances  = (uint8_t *)preprocessing_block->distances_selection->distances_snr;

	uint8_t *sendDistances  = (uint8_t *)postprocessing_block->final_distances->final_distances;
	uint8_t *sendLevels  = (uint8_t *)postprocessing_block->final_distances->final_levels;
	uint8_t *noise = (uint8_t *) &postprocessing_block->distances_processing->noise_lvl;

	dataPacket[numByte] = PRE1;
	dataPacket[numByte + 1] = PRE2;
	dataPacket[numByte + 2] = PRE3;
	dataPacket[numByte + 3] = PRE4;
	dataPacket[numByte + 4] = (uint8_t) numBytesSend;
	dataPacket[numByte + 5] = (uint8_t) numBytesSend >> 8;

	numByte += 6;

#define SONAR 1
#if SONAR
	osMutexWait (sonar_mutex_id, osWaitForever);
	float32_t sonar = sonar_dist;
	uint8_t *sonarData = (uint8_t *) &sonar;
	osMutexRelease (sonar_mutex_id);
	dataPacket[numByte] = sonarData[0];
	dataPacket[numByte + 1] = sonarData[1];
	dataPacket[numByte + 2] = sonarData[2] ;
	dataPacket[numByte + 3] = sonarData[3];
	numByte += 4;
#else
	dataPacket[numByte] = 0;
	dataPacket[numByte + 1] = 0;
	dataPacket[numByte + 2] = 0;
	dataPacket[numByte + 3] = 0;
	numByte += 4;
#endif

#define TEMPERATURE 1
#if TEMPERATURE
	osMutexWait (temperature_mutex_id, osWaitForever);
	float32_t tempt = temperature;
	uint8_t *tempData = (uint8_t *) &tempt;
	osMutexRelease (temperature_mutex_id);
	dataPacket[numByte] = tempData[0];
	dataPacket[numByte + 1] = tempData[1];
	dataPacket[numByte + 2] = tempData[2];
	dataPacket[numByte + 3] = tempData[3];
	numByte += 4;
#else
	float32_t cte_temp = 25.5f;
	uint8_t *tempData = (uint8_t *) &cte_temp;
	dataPacket[numByte] = tempData[0];
	dataPacket[numByte + 1] = tempData[1];
	dataPacket[numByte + 2] = tempData[2];
	dataPacket[numByte + 3] = tempData[3];
	numByte += 4;
#endif

	uint64_t timestamp = main_control->timestamp;

	dataPacket[numByte] = timestamp;
	dataPacket[numByte + 1] = timestamp >> 8;
	dataPacket[numByte + 2] = timestamp >> 16;
	dataPacket[numByte + 3] = timestamp >> 24;
	dataPacket[numByte + 4] = timestamp >> 32;
	dataPacket[numByte + 5] = timestamp >> 40;
	dataPacket[numByte + 6] = timestamp >> 48;
	dataPacket[numByte + 7] = timestamp >> 56;
	numByte += 8;

	dataPacket[numByte] = (uint8_t) numTargets;
	numByte++;


	for(i=0; i<numTargets; i++){
		dataPacket[numByte] = sendDistances[(i*4)];
		dataPacket[numByte + 1] = sendDistances[(i*4) + 1];
		dataPacket[numByte + 2] = sendDistances[(i*4) + 2];
	    dataPacket[numByte + 3] = sendDistances[(i*4) + 3];

		dataPacket[numByte + 4] = sendLevels[(i*4)];
		dataPacket[numByte + 5] = sendLevels[(i*4) + 1];
		dataPacket[numByte + 6] = sendLevels[(i*4) + 2];
		dataPacket[numByte + 7] = sendLevels[(i*4) + 3];

		dataPacket[numByte + 8] = noise[0];
		dataPacket[numByte + 9] = noise[1];
		dataPacket[numByte + 10] = noise[2];
		dataPacket[numByte + 11] = noise[3];
		numByte += 12;
	}

	return RET_OK;

}

retval_t remove_distances_data_packet(output_data_t* output_data){
	if(output_data->output_data_dist_packet != NULL){
		vPortFree(output_data->output_data_dist_packet);
		return RET_OK;
	}
	else{
		return RET_ERROR;
	}
}


void geth_comm_spi_callback(void){
//	osSemaphoreRelease (geth_comm_semaphore);
}

void geth_comm_pin_callback(void){
	osSemaphoreRelease (geth_comm_semaphore);
}






//Copiada estructura de paquetes de guille

//#define RALPH_MSSG_SIZE 144
//
//#define RALPH_PREAMB_SIZE 4
//
//#define MAX_NUMBER_OBJECTS 10
//
//typedef struct _ralph_meass_t
//
//{
//
//	float32_t distance;
//
//	float32_t level;
//
//	float32_t noise;
//
//}ralph_meass_t;
//
//
//typedef union _ralph_buffer_t
//
//{
//
//	uint8_t val[RALPH_MSSG_SIZE];
//
//	struct
//
//	{
//
//		uint8_t preamb[RALPH_PREAMB_SIZE];
//
//		uint8_t tmssg[RALPH_MSSG_SIZE];
//
//	}fields;
//
//	struct __packed
//
//	{
//
//		uint8_t preamb[RALPH_PREAMB_SIZE];
//
//		uint16_t bytes2read;
//
//		float32_t sonar;
//
//		float32_t temp;
//
//		uint64_t time;
//
//		uint8_t num_objects;
//
//		ralph_meass_t content[MAX_NUMBER_OBJECTS];
//
//	}sub_fields;
//
//}ralph_buffer_t;
//
//
//ralph_buffer_t* ralph_buffer_aux; /*Si llegan los datos desfasados, utilizaremos este puntero para colocarlo al principio de los datos.*/
