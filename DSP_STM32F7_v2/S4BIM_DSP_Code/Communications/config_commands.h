/*
 * Copyright (c) 2016, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * config_commands.h
 *
 *  Created on: 7/6/2016
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@die.upm.es>
 *
 */
/**
 * @file config_commands.h
 */

#ifndef APPLICATION_USER_S4BIM_DSP_CODE_COMMUNICATIONS_CONFIG_COMMANDS_H_
#define APPLICATION_USER_S4BIM_DSP_CODE_COMMUNICATIONS_CONFIG_COMMANDS_H_

#include "DSP_conf.h"
#include "preprocessing.h"
#include "output_data.h"
#include "channel.h"
#include "commands_usart.h"

/**
 * COMMANDS STRING DEFINITIONS
 */
#define CHANGE_ADC_SAMPLES				"adcsamp"
#define CHANGE_ADC_GUARD_SAMPLES		"adcguard"

#define CHANGE_WINDOW_STR				"wind"
#define CHANGE_TUKEY_ALPHA				"tukalp"

#define CHANGE_FFT_SAMPLES				"fftnum"
#define CHANGE_FFT_EFFECTIVE_SAMPLES	"fftef"

#define CHANGE_OSCFAR_WINDOW			"oscfarwd"
#define CHANGE_OSCFAR_KORDER			"oscfark"
#define CHANGE_OSCFAR_ALPHA				"oscfaralp"

#define CHANGE_POSTOSCFAR_WINDOW		"postoscfarwd"
#define CHANGE_POSTOSCFAR_KORDER		"postoscfark"
#define CHANGE_POSTOSCFAR_ALPHA			"postoscfaralp"

#define CHANGE_FAR_FREQ_SAMPLE_SNR		"snrfarsamp"
#define CHANGE_FAR_FREQ_VALUE_SNR		"snrfarval"

#define CHANGE_MAX_NUM_PEAKS			"maxpeaks"

#define CHANGE_MAX_NUM_SPEEDS			"maxspeeds"
#define CHANGE_SPEEDS_CALIBRATION		"spdcal"
#define CHANGE_SPEEDS_GUARD				"spdguard"
#define CHANGE_SPEEDS_NEAR_LVL_THR		"spdnlvl"
#define CHANGE_SPEEDS_MID_LVL_THR		"spdmlvl"
#define CHANGE_SPEEDS_FAR_LVL_THR		"spdflvl"
#define CHANGE_SPEEDS_VFAR_LVL_THR		"spdvflvl"
#define CHANGE_SPEEDS_NEAR_SNR_THR		"spdnsnr"
#define CHANGE_SPEEDS_MID_SNR_THR		"spdmsnr"
#define CHANGE_SPEEDS_FAR_SNR_THR		"spdfsnr"
#define CHANGE_SPEEDS_VFAR_SNR_THR		"spdvfsnr"

#define CHANGE_MAX_NUM_DISTANCES		"maxdists"
#define CHANGE_DISTANCE_CALIBRATION		"distcal"
#define CHANGE_DOWNRAMP_CALIBRATION		"downcal"
#define CHANGE_DISTANCE_GUARD			"distguard"
#define CHANGE_DISTANCE_NEAR_LVL_THR	"distnlvl"
#define CHANGE_DISTANCE_MID_LVL_THR		"distmlvl"
#define CHANGE_DISTANCE_FAR_LVL_THR		"distflvl"
#define CHANGE_DISTANCE_VFAR_LVL_THR	"distvflvl"
#define CHANGE_DISTANCE_NEAR_SNR_THR	"distnsnr"
#define CHANGE_DISTANCE_MID_SNR_THR		"distmsnr"
#define CHANGE_DISTANCE_FAR_SNR_THR		"distfsnr"
#define CHANGE_DISTANCE_VFAR_SNR_THR	"distvfsnr"
#define ENABLE_SPEED_FILTER				"enspd"
#define DISABLE_SPEED_FILTER			"disspd"

#define CHANGE_SPEED_CALCULATION_RATE	"spdrate"
#define CHANGE_UPDOWN_RATE				"updownrate"
#define CHANGE_DELAY					"delay"

#define CHANGE_OUTPUT_MODE				"outmode"
#define CHANGE_OUTPUT_INTERFACE			"outint"

#define CHANGE_DIST_PROC_CLUSTER_SIZE	"distcluster"
#define CHANGE_DIST_PROC_MAX_DIST		"maxdistance"
#define CHANGE_DIST_ROUND_STEP			"distround"
#define CHANGE_POST_PROC_SAMPLES		"postprocsamp"

#define CHANGE_POST_PROC_THREESHOLD		"postthres"
#define CHANGE_FINAL_MIN_DIST			"finalmindist"
#define CHANGE_FINAL_DIST_RES			"finaldistres"


/**
 * WINDOWS STRING DEFINITION
 */
#define WINDOW_HANNING_STR				"hann"
#define WINDOW_HAMMING_STR				"hamm"
#define WINDOW_TUKEY_STR				"tukey"
#define NOWINDOW_STR					"nowd"

/**
 * OUTPUT MODE STRING DEFINITION
 */
#define OUTPUT_MODE_DISTANCES			"dist"
#define OUTPUT_MODE_FREQ				"freq"
#define OUTPUT_MODE_TIME				"time"
#define OUTPUT_MODE_DIST_SIGNAL			"dist_signal"

/**
 * OUTPUT INTERFACE STRING DEFINITION
 */
#define OUTPUT_INTERFACE_NONE			"none"
#define OUTPUT_INTERFACE_UART			"uart"
#define OUTPUT_INTERFACE_SPI			"spi"
#define OUTPUT_INTERFACE_SPI_UART		"spiuart"

/**
 * UART STRING DEFINITION
 */
#define TOGGLE_GPIO						"togglegpio"
#define ADD_NEW_CHANNEL					"addchannel"
#define SET_CHANNEL_GAIN				"setgain"
#define SET_CHANNEL_FILTER_FREQ			"setfreq"
#define SET_CHANNEL_FILTER_TYPE			"setfilter"
#define SET_CHANNEL_ADC_TYPE			"setadc"
#define REMOVE_CHANNEL					"removechannel"
#define CREATE_MODULATION				"newmodulation"
#define ADD_POINTS_SIG					"addpointssig"
#define ADD_POINTS_PRE					"addpointspre"
#define CMD_SET_MODULATION				"setmodulation"
#define CMD_ADD_MODULATION					"addmodulation"

#define SET_WIFI_SSID_PASS				"wifi_ssid"
/*****************************************************/
/*****************************************************/



typedef struct {
	uint16_t argc;
	char** argv;
	retval_t (* command_func)(uint16_t argc, char** argv);
}
command_data_t;


command_data_t* new_command_data();
retval_t remove_command_data(command_data_t* command_data);

retval_t select_command(command_data_t* command_data);
retval_t execute_command(command_data_t* command_data);

#endif /* APPLICATION_USER_S4BIM_DSP_CODE_COMMUNICATIONS_CONFIG_COMMANDS_H_ */
