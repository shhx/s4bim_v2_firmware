/*
 * channel.c
 *
 *  Created on: 18 jun. 2017
 */

#include "channel.h"

channel_t* create_channel(uint8_t number, uint16_t gain, uint32_t freq, filter_t filter_type, ADC_t adc_type) {

	channel_t* new = (channel_t*) pvPortMalloc(sizeof(channel_t));
	if (number > 4 || number < 1 || gain > 1024) {
		return NULL;
	}
	new->number = number;
	new->gain = gain;
	new->frequency = freq;
	new->filter_type = filter_type;
	new->adc_type = adc_type;
	return new;
}

retval_t remove_channel(channel_t* chn) {
	if (chn != NULL) {
		vPortFree(chn);
		return RET_OK;
	} else {
		return RET_ERROR;
	}
}
