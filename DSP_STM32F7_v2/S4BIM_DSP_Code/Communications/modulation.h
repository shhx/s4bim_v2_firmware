/*
 * modulation.h
 *
 *  Created on: 4 jun. 2017
 */

#ifndef APPLICATION_USER_S4BIM_DSP_CODE_COMMUNICATIONS_MODULATION_H_
#define APPLICATION_USER_S4BIM_DSP_CODE_COMMUNICATIONS_MODULATION_H_

#include "cmsis_os.h"
#include "arm_math.h"
#include "types.h"

#define VMAX	10					//!< Max Voltage level in V
#define	VMIN	0					//!< Min Voltage level in V

#define VMAX_10V_NUMBER	3735		//!< Binary number corresponding to 10 V max value
#define VAVG_5V_NUMBER	2150		//!< Binary number corresponding to 5 V avg value
#define	VMIN_0V_NUMBER	350			//!< Binary number corresponding to 0 V min value

#define	V_SAMPLE_DAC	0.003		//!< Resolution of the DAC: 3mV

#define TIME_ADC_SAMPLE_US	20		//!< ADC sample time 50KHz in us: 20
#define TIME_ADC_SAMPLE_MS	0.02f	//!< ADC sample time 50KHz in ms: 0.02

#define TIME_DAC_SAMPLE_US	20		//!< DAC sample time 50KHz in us: 20
#define TIME_DAC_SAMPLE_MS	0.02f	//!< DAC sample time 50KHz in ms: 0.02

#define PREMOD_CONSTANT_TIME_MS	1.26f		//!< Constant signal premodulation time in ms
#define PREMOD_CONSTANT_TIME_US	1260		//!< Constant signal premodulation time in us
#define PREMOD_CONSTANT_SAMPLES	PREMOD_CONSTANT_TIME_US/TIME_DAC_SAMPLE_US	//!< Constant signal premodulation sample number

#define PREMOD_RAMP_TIME_MS	1.26f			//!< Ramp signal premodulation time in ms
#define PREMOD_RAMP_TIME_US	1260			//!< Ramp signal premodulation time in us
#define PREMOD_RAMP_SAMPLES	PREMOD_RAMP_TIME_US/TIME_DAC_SAMPLE_US	//!< Ramp signal premodulation sample number

#define PREMOD_SAMPLES	126			//!< Total number of samples used in the premodulation signal

#define UPRAMP		1				//!< Upramp flag
#define DOWNRAMP	2				//!< Downramp flag

/**
 * @brief Modulation struct.
 */
typedef struct modulation_outline {
	uint16_t sig_prev_size;
	uint16_t signal_size;
	uint16_t* sig_prev;				//!< Pointer to the premodulation signal samples of each scheme
	uint16_t* signal;					//!< Pointer to the modulation signal samples of each scheme
	uint16_t num_samples_dac;					//!< Number of dac samples
	uint16_t num_samples_adc;					//!< Number of adc samples
} modulation_outline_t;

#include "adc_iq_signals.h"

modulation_outline_t* new_modulation(uint16_t sig_prev_size, uint16_t signal_size,
		uint16_t* sig_prev, uint16_t* signal, uint16_t num_samples_dac, uint16_t num_samples_adc);

#endif /* APPLICATION_USER_S4BIM_DSP_CODE_COMMUNICATIONS_MODULATION_H_ */
