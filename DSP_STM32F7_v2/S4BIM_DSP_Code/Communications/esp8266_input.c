/*
 * Copyright (c) 2016, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * esp8266_input.c
 *
 *  Created on: 19/7/2016
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@die.upm.es>
 *
 */
/**
 * @file esp8266_input.c
 */


#include "esp8266_input.h"



retval_t start_esp8266_listening(input_data_t* input_data){
	if(input_data != NULL){

		HAL_UART_Receive_DMA(&huart6, input_data->circular_buffer, CIRCULAR_BUFFER_SIZE);

		return RET_OK;
	}
	else{
		return RET_ERROR;
	}
}

retval_t stop_esp8266_listening(input_data_t* input_data){
	if(input_data != NULL){

		HAL_UART_DMAStop(&huart6);
		return RET_OK;
	}
	else{
		return RET_ERROR;
	}
}

retval_t pause_esp8266_listening(input_data_t* input_data){
	if(input_data != NULL){

		HAL_UART_DMAPause(&huart6);
		return RET_OK;
	}
	else{
		return RET_ERROR;
	}
}

retval_t resume_esp8266_listening(input_data_t* input_data){
	if(input_data != NULL){

		HAL_UART_DMAResume(&huart6);

		return RET_OK;
	}
	else{
		return RET_ERROR;
	}
}


retval_t get_new_esp8266_data(input_data_t* input_data){

	if(input_data != NULL){

		input_data->head = input_data->circular_buffer + CIRCULAR_BUFFER_SIZE - __HAL_DMA_GET_COUNTER(huart6.hdmarx);

		if(input_data->head > input_data->tail){
			uint32_t new_chars_number = input_data->head - input_data->tail;
			uint16_t i;

			if((input_data->input_command_str_count + new_chars_number) > input_data->max_cmd_buffer_size){
				input_data->input_command_str_count = 0;
				input_data->input_command_str_current_count = 0;
				return RET_ERROR;	//COMMAND BUFFER OVERFLOW
			}

			for(i=0; i<new_chars_number; i++){
				input_data->input_commands_str[i+input_data->input_command_str_count] = input_data->tail[i];
			}

			input_data->input_command_str_count = input_data->input_command_str_count + new_chars_number;
			input_data->tail = input_data->head;
		}

		else if(input_data->head < input_data->tail){

			uint16_t i;
			uint32_t diff_end = input_data->circular_buffer + CIRCULAR_BUFFER_SIZE - input_data->tail;
			uint32_t diff_start = input_data->head - input_data->circular_buffer;
			uint32_t new_chars_number = diff_end + diff_start;

			if((input_data->input_command_str_count + new_chars_number) > input_data->max_cmd_buffer_size){
				input_data->input_command_str_count = 0;
				input_data->input_command_str_current_count = 0;
				return RET_ERROR;	//COMMAND BUFFER OVERFLOW
			}

			for(i=0; i<new_chars_number; i++){
				if( i < diff_end){
					input_data->input_commands_str[i+input_data->input_command_str_count] = input_data->tail[i];
				}
				else{
					input_data->input_commands_str[i+input_data->input_command_str_count] = input_data->circular_buffer[i-diff_end];
				}

			}

			input_data->input_command_str_count = input_data->input_command_str_count + new_chars_number;
			input_data->tail = input_data->head;


		}

		return RET_OK;
	}
	else{
		return RET_ERROR;
	}
}


retval_t check_esp8266_newline(input_data_t* input_data){

	if(input_data != NULL){

		uint16_t i;
		uint16_t j;

		for(i=input_data->input_command_str_current_count; i <input_data->input_command_str_count; i++){

			if(input_data->input_commands_str[i] == '\b'){										//First check if there exists a backspace
				for(j=i; j<input_data->input_command_str_count-1; j++){
					input_data->input_commands_str[j] = input_data->input_commands_str[j+1];
					input_data->input_command_str_count--;
				}
				i--;

			}
		}

		for(i=input_data->input_command_str_current_count; i <input_data->input_command_str_count; i++){		//Now check for a newline. The new line could be '\r' '\n' or '\r\n'

			if((input_data->input_commands_str[i] == '\r') || (input_data->input_commands_str[i] == '\n') || (input_data->input_commands_str[i] == '\0')){

				input_data->input_commands_str[i] = '\0';

				input_data->command->argv = str_split((char*) input_data->input_commands_str, ' ', (int*) &input_data->command->argc );	//Memory for argv is reserved inside str_split function. It is necessary to free it later

				uint16_t extra_bytes = input_data->input_command_str_count-i-1;		//Number of existing bytes after '\r' or '\n'

				for(j=0; j<extra_bytes; j++){				//Moves everything (if exists) after '\r' or '\n' to the start of the buffer

					input_data->input_commands_str[j] = input_data->input_commands_str[i+j+1];

				}

				input_data->input_command_str_current_count = 0;
				input_data->input_command_str_count = extra_bytes;

				return RET_OK;

			}
		}

		input_data->input_command_str_current_count = input_data->input_command_str_count;

		return RET_OK;
	}
	else{
		return RET_ERROR;
	}
}
retval_t process_esp8266_command(input_data_t* input_data){

	if(input_data != NULL){

		return RET_OK;
	}
	else{
		return RET_ERROR;
	}
}
