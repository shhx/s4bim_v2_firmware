/*
 * Copyright (c) 2016, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * wifi_commands.c
 *
 *  Created on: 22/7/2016
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@die.upm.es>
 *
 */
/**
 * @file wifi_commands.c
 */


#include "wifi_commands.h"


extern output_data_t* output_data;
extern control_t* control;
extern preprocessing_block_t* preprocessing_block;

extern float32_t sonar_cal;

extern osMutexId  sonar_mutex_id;
extern osMutexId  main_control_mutex_id;
extern osMutexId  preprocessing_mutex_id;

retval_t wifi_null_func(uint16_t argc, char** argv);

retval_t wifi_ap_connected(uint16_t argc, char** argv);
retval_t wifi_ap_disconnected(uint16_t argc, char** argv);

retval_t wifi_out_start(uint16_t argc, char** argv);
retval_t wifi_out_stop(uint16_t argc, char** argv);

retval_t wifi_set_time(uint16_t argc, char** argv);
retval_t wifi_calibration(uint16_t argc, char** argv);
retval_t wifi_calibration_sonar(uint16_t argc, char** argv);
retval_t wifi_position(uint16_t argc, char** argv);
retval_t wifi_orientation(uint16_t argc, char** argv);

retval_t module_reset(uint16_t argc, char** argv);

#include "leds.h"
retval_t select_wifi_command(command_data_t* command_data){
	if(command_data != NULL){
		if(command_data->argc != 0){
			leds_toggle(LEDS_GREEN);
			/////////////////////////////////////////////////////////////////////
			if(strcmp(command_data->argv[0],WIFI_AP_CONNECTED_CMD) == 0){
				command_data->command_func = wifi_ap_connected;
			}
			else if(strcmp(command_data->argv[0],WIFI_AP_DISCONNECTED_CMD) == 0){
				command_data->command_func = wifi_ap_disconnected;
			}
			else if(strcmp(command_data->argv[0],WIFI_OUT_START_CMD) == 0){
				command_data->command_func = wifi_out_start;
			}
			else if(strcmp(command_data->argv[0],WIFI_OUT_STOP_CMD) == 0){
				command_data->command_func = wifi_out_stop;
			}
			else if(strcmp(command_data->argv[0],WIFI_SET_TIME) == 0){
				command_data->command_func = wifi_set_time;
			}
			else if(strcmp(command_data->argv[0],WIFI_CALIBRATION) == 0){
				command_data->command_func = wifi_calibration;
			}
			else if(strcmp(command_data->argv[0],WIFI_CALIBRATION_SONAR) == 0){
				command_data->command_func = wifi_calibration_sonar;
			}
			else if(strcmp(command_data->argv[0],WIFI_POSITION) == 0){
				command_data->command_func = wifi_position;
			}
			else if(strcmp(command_data->argv[0],WIFI_ORIENTATION) == 0){
				command_data->command_func = wifi_orientation;
			}

			/////////////////////////////////////////////////////////////////////
			else if(command_data->argv[0][0] == '$'){						//RESET
				command_data->command_func = module_reset;
			}
			else{
				command_data->command_func = wifi_null_func;
				return RET_ERROR;
			}
			return RET_OK;
		}
		else{
			return RET_OK;
		}

		return RET_OK;
	}
	else{
		return RET_ERROR;
	}
}

retval_t execute_wifi_command(command_data_t* command_data){
	if(command_data != NULL){
		uint16_t i = 0;

		command_data->command_func(command_data->argc, command_data->argv);

		command_data->command_func = wifi_null_func;

		for (i=0; i<command_data->argc; i++){			//When a command is executed, the memory occupied by the args must be freed
			if(command_data->argv[i] != NULL){
				vPortFree(command_data->argv[i]);
				command_data->argv[i] = NULL;
			}
		}
		if(command_data->argv != NULL){
			vPortFree(command_data->argv);
			command_data->argv = NULL;
		}

		command_data->argc = 0;

		return RET_OK;

	}
	else{
		return RET_ERROR;
	}
}


retval_t wifi_null_func(uint16_t argc, char** argv){
	return RET_OK;
}


retval_t wifi_ap_connected(uint16_t argc, char** argv){

	if(argc == 1){
		output_data->wifi_ap_state = WIFI_AP_CONNECTED;
		return RET_OK;
	}
	else{
		return RET_ERROR;
	}
}

retval_t wifi_ap_disconnected(uint16_t argc, char** argv){
	if(argc == 1){
		output_data->wifi_ap_state = WIFI_AP_DISCONNECTED;
		return RET_OK;
	}
	else{
		return RET_ERROR;
	}
}



retval_t wifi_out_start(uint16_t argc, char** argv){

	if(argc == 1){
		output_data->wifi_output_state = WIFI_OUTPUT_STARTED;
		return RET_OK;
	}
	else{
		return RET_ERROR;
	}
}

retval_t wifi_out_stop(uint16_t argc, char** argv){
	if(argc == 1){
		output_data->wifi_output_state = WIFI_OUTPUT_STOPPED;
		return RET_OK;
	}
	else{
		return RET_ERROR;
	}
}
retval_t wifi_set_time(uint16_t argc, char** argv){

	if(control != NULL){
		if(argc==2){
			char* local = argv[1];
			uint64_t new_timestamp = (uint64_t) atoll(argv[1]);
			osMutexWait (main_control_mutex_id, osWaitForever);
			control->timestamp = new_timestamp;
			osMutexRelease (main_control_mutex_id);
			return RET_OK;
		}
		else{
			return RET_ERROR;
		}
	}
	else{
		return RET_ERROR;
	}
}


retval_t wifi_calibration(uint16_t argc, char** argv){

	if(preprocessing_block != NULL){
			if(argc==2){
				float32_t new_calibration = (float32_t) atof(argv[1]);
				osMutexWait (preprocessing_mutex_id, osWaitForever);
				preprocessing_block->distances_selection->calibration_factor = new_calibration;
				osMutexRelease (preprocessing_mutex_id);
				return RET_OK;
			}
			else{
				return RET_ERROR;
			}
		}
		else{
			return RET_ERROR;
		}
}

retval_t wifi_calibration_sonar(uint16_t argc, char** argv){

	if(preprocessing_block != NULL){
			if(argc==2){
				float32_t new_calibration_sonar = (float32_t) atof(argv[1]);
				osMutexWait (sonar_mutex_id, osWaitForever);
				sonar_cal = new_calibration_sonar;
				osMutexRelease (sonar_mutex_id);
				return RET_OK;
			}
			else{
				return RET_ERROR;
			}
		}
		else{
			return RET_ERROR;
		}
}

retval_t wifi_position(uint16_t argc, char** argv){
	if(control != NULL){
		if(argc==4){
			float32_t new_x = (float32_t) atof(argv[1]);
			float32_t new_y = (float32_t) atof(argv[2]);
			float32_t new_z = (float32_t) atof(argv[3]);
			osMutexWait (main_control_mutex_id, osWaitForever);
			control->pos_x = new_x;
			control->pos_y = new_y;
			control->pos_z = new_z;
			osMutexRelease (main_control_mutex_id);
			return RET_OK;
		}
		else{
			return RET_ERROR;
		}
	}
	else{
		return RET_ERROR;
	}
}
retval_t wifi_orientation(uint16_t argc, char** argv){
	if(control != NULL){
		if(argc==4){
			float32_t new_x = (float32_t) atof(argv[1]);
			float32_t new_y = (float32_t) atof(argv[2]);
			float32_t new_z = (float32_t) atof(argv[3]);
			osMutexWait (main_control_mutex_id, osWaitForever);
			control->ori_x = new_x;
			control->ori_y = new_y;
			control->ori_z = new_z;
			osMutexRelease (main_control_mutex_id);
			return RET_OK;
		}
		else{
			return RET_ERROR;
		}
	}
	else{
		return RET_ERROR;
	}
}


retval_t module_reset(uint16_t argc, char** argv){

//	output_data->wifi_ap_state = WIFI_AP_DISCONNECTED;
//	output_data->wifi_output_state = WIFI_OUTPUT_STOPPED;

	return RET_OK;
}
