/*
 * channel.h
 *
 *  Created on: 18 jun. 2017
 */

#ifndef APPLICATION_USER_S4BIM_DSP_CODE_COMMUNICATIONS_CHANNEL_H_
#define APPLICATION_USER_S4BIM_DSP_CODE_COMMUNICATIONS_CHANNEL_H_

#include "types.h"
#include "cmsis_os.h"

typedef enum {
	SCF_FILTER,
	ALT_FILTER,
} filter_t;

typedef enum {
	SAR,
	SDADC,
} ADC_t;

typedef struct channel {
	uint8_t		number;
	uint16_t 	gain;
	uint32_t 		frequency;
	filter_t 	filter_type;
	ADC_t		adc_type;
} channel_t;

channel_t* create_channel(uint8_t number, uint16_t gain, uint32_t freq, filter_t filter_type, ADC_t adc_type);
retval_t remove_channel(channel_t* chn);

#endif /* APPLICATION_USER_S4BIM_DSP_CODE_COMMUNICATIONS_CHANNEL_H_ */
