/*
 * modulation.c
 *
 *  Created on: 4 jun. 2017
 */

#include "modulation.h"


/**
 * @brief	Constructor to create a new modulation structure
 * @return	Returns the created modulation structure
 */
modulation_outline_t* new_modulation(uint16_t sig_prev_size, uint16_t signal_size,
		uint16_t* sig_prev, uint16_t* signal, uint16_t num_samples_dac, uint16_t num_samples_adc){

	modulation_outline_t* modulation = (modulation_outline_t*) pvPortMalloc(sizeof(modulation_outline_t));
	modulation->sig_prev_size = sig_prev_size;
	modulation->signal_size = signal_size;
	modulation->sig_prev = sig_prev;
	modulation->signal= signal;
	modulation->num_samples_dac = num_samples_dac;
	modulation->num_samples_adc = num_samples_adc;
	return modulation;
}




