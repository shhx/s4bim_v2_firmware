/*
 * commands_usart.c
 *
 *  Created on: 1 jun. 2017
 */

#include "commands_usart.h"

/**
 * @brief			Constructor to create a new command packet
 * @param command	The initial command of the packet
 * @return			Returns a pointer to the new command packet
 */
command_usart_t* new_command_usart(commands_usart command, void* data) {

	command_usart_t* new_cmd = (command_usart_t*) pvPortMalloc(sizeof(command_usart_t));
	new_cmd->command = command;
	switch (command) {
	case SET_GAIN:
	case SET_ADC_TYPE:
	case SET_FILTER_FREQ:
		new_cmd->size = sizeof(uint16_t);
		break;
//		new_cmd->size = sizeof(filter_config_t);
//		break;
	case SET_VCO_LIMIT:
		new_cmd->size = sizeof(float32_t);
		break;
	case ADD_MODULATION:
	case SET_MODULATION:
		new_cmd->size = sizeof(uint16_t); //TODO solo un scheme
		break;
	case NEW_MODULATION:
		new_cmd->size = 3 * sizeof(uint16_t);
		break;
	case ADD_POINTS_MOD:
	case ADD_POINTS_MOD_PRE: {
		uint16_t* args = (uint16_t*) data;
		new_cmd->size = (args[1] + 2) * sizeof(uint16_t);
		break;
	}
	case TEST:
		new_cmd->size = sizeof(uint8_t);
		break;
	default:
		break;
	}

	new_cmd->data = data;
	return new_cmd;
}

retval_t write_command_usart(command_usart_t* cmd) {
	HAL_UART_Transmit(&huart1, (uint8_t*) &(cmd->command), sizeof(commands_usart), 100);
	osDelay(UART_DELAY);
	HAL_UART_Transmit(&huart1, (uint8_t*) &(cmd->size), sizeof(uint8_t), 100);
	osDelay(UART_DELAY);
	switch (cmd->command) {
	case ADD_CHANNEL:
		return RET_OK;
	case RMV_CHANNEL:
		return RET_OK;
	case SET_GAIN:
	case SET_FILTER_FREQ:
	case SET_FILTER_TYPE: {
		channel_config_t* args = (channel_config_t*) cmd->data;
		HAL_UART_Transmit(&huart1, (uint8_t*) &(args->number), sizeof(uint8_t), 100); //send channel number
		osDelay(UART_DELAY);
		HAL_UART_Transmit(&huart1, (uint8_t*) args->config, cmd->size, 100); //send channel config
		osDelay(UART_DELAY);
		return RET_OK;
	}
	case NEW_MODULATION:{
		uint16_t* args = (uint16_t*) cmd->data;
		HAL_UART_Transmit(&huart1, (uint8_t*) args, sizeof(uint16_t), 100); //send modulation number
		osDelay(UART_DELAY);
		HAL_UART_Transmit(&huart1, (uint8_t*) (args + 1), sizeof(uint16_t), 100); //send number of prev points
		osDelay(UART_DELAY);
		HAL_UART_Transmit(&huart1, (uint8_t*) (args + 2), sizeof(uint16_t), 100); //send number of signal points
		osDelay(UART_DELAY);
		return RET_OK;
	}
	case ADD_POINTS_MOD:
	case ADD_POINTS_MOD_PRE: {
		uint16_t* args = (uint16_t*) cmd->data;
		HAL_UART_Transmit(&huart1, (uint8_t*) args, sizeof(uint16_t), 100); //send modulation number
		osDelay(UART_DELAY);
		HAL_UART_Transmit(&huart1, (uint8_t*) (args + 1), sizeof(uint16_t), 100); //send number of points
		osDelay(UART_DELAY);
		HAL_UART_Transmit(&huart1, (uint8_t*) (args + 2), args[1] * sizeof(uint16_t), 100); //send points
		osDelay(UART_DELAY);
		return RET_OK;
	}
	case ADD_MODULATION:
	case SET_MODULATION:
		HAL_UART_Transmit(&huart1, (uint8_t*) cmd->data, sizeof(uint16_t), 100); //send modulation number
		osDelay(UART_DELAY);
		return RET_OK;
	case TEST:
		return RET_OK;
	default:
		return RET_ERROR;
	}
	return RET_ERROR;
}

/**
 * @brief		Destructor to remove an existing command packet
 * @param cmd	The command packet to be removed
 * @return		Returns RET_OK when the packet is removed and returns RET_ERROR if the packet structure does not exist
 */
retval_t remove_command_usart(command_usart_t* cmd) {
	if (cmd != NULL) {
		vPortFree(cmd->data);
		vPortFree(cmd);
		return RET_OK;
	} else {
		return RET_ERROR;
	}
}
