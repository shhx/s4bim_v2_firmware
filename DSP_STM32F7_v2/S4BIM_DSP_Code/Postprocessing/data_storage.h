/*
 * Copyright (c) 2016, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * data_storage.h
 *
 *  Created on: 30/6/2016
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@die.upm.es>
 *
 */
/**
 * @file data_storage.h
 */

#ifndef APPLICATION_USER_S4BIM_DSP_CODE_POSTPROCESSING_DATA_STORAGE_H_
#define APPLICATION_USER_S4BIM_DSP_CODE_POSTPROCESSING_DATA_STORAGE_H_

#include "preprocessing.h"
#include "sdram.h"
#include "DSP_conf.h"

typedef struct data_storage{

	float32_t** distances_circular_buffer;
	float32_t** levels_circular_buffer;
	float32_t** snr_circular_buffer;
	uint64_t*	timestamps_circular_buffer;
	uint16_t*	numDistances_circular_buffer;
	uint16_t 	current_index;
	uint16_t	buffer_size;
}data_storage_t;


data_storage_t* new_data_storage(uint16_t buffer_size);
retval_t remove_data_storage(data_storage_t* data_storage);

retval_t store_data(data_storage_t* data_storage, preprocessing_block_t* preprocessing, uint64_t timestamp);

#endif /* APPLICATION_USER_S4BIM_DSP_CODE_POSTPROCESSING_DATA_STORAGE_H_ */
