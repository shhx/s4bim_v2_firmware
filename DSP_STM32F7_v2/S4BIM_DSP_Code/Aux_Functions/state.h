/*
 * Copyright (c) 2016, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * state.h
 *
 *  Created on: 19/5/2016
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@die.upm.es>
 *
 */
/**
 * @file state.h
 */

#ifndef APPLICATION_USER_S4BIM_ADC_CODE_STATE_H_
#define APPLICATION_USER_S4BIM_ADC_CODE_STATE_H_

#include "stm32f7xx.h"
#include "types.h"
#include "cmsis_os.h"


/**
 * @brief	ADC and DCP possible states. This states are shared between both uCs
 */
typedef enum {
	S1_NULL = 65535,	//!< NULL state only used on initialization
	S1_IDLE = 1,		//!< uC state is Idle
	S1_RUNNING = 2,		//!< uC state is running
	S1_CAPTURING = 3,	//!< ADC is capturing a signal
	S1_STOPPED = 4,		//!< uC state is stopped
	S1_SENDING = 5,		//!< ADC is sending a signal
	S1_SLEEP = 6,		//!< uC state is slept
	S1_ERROR = 7,		//!< There has been an error
}state;

/**
 * @brief	Packet containing several states. It has the same size (8) as command_t but in this application only state_0 is used
 */
typedef struct state_{
	state state_0;		//!<Contains the state of the uC
	state state_1;		//!< Not used
	state state_2;		//!< Not used
	state state_3;		//!< Not used
}state_t;

state_t* new_state(void);
retval_t remove_state(state_t* state);



#endif /* APPLICATION_USER_S4BIM_ADC_CODE_STATE_H_ */
