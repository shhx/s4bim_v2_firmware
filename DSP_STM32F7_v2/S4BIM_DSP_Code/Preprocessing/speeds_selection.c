/*
 * Copyright (c) 2016, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * speeds_selection.c
 *
 *  Created on: 31/5/2016
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@die.upm.es>
 *
 */
/**
 * @file speeds_selection.c
 */


#include "speeds_selection.h"



speeds_selection_t* new_speeds_selection(uint16_t max_num_speeds, uint16_t num_guard_low_speeds){

	uint16_t i;
	speeds_selection_t* new_speeds_selection = (speeds_selection_t*) pvPortMalloc(sizeof(speeds_selection_t));

	new_speeds_selection->speeds_positions = (uint16_t*) pvPortMalloc(max_num_speeds*sizeof(uint16_t));
	new_speeds_selection->speeds = (float32_t*) pvPortMalloc(max_num_speeds*sizeof(float32_t));
	new_speeds_selection->speeds_levels = (float32_t*) pvPortMalloc(max_num_speeds*sizeof(float32_t));

	new_speeds_selection->max_num_speeds = max_num_speeds;
	new_speeds_selection->current_num_speeds = 0;
	new_speeds_selection->calibration_factor = DEFAULT_SPEED_CALIBRATION_FACTOR;


	new_speeds_selection->num_guard_low_speeds = num_guard_low_speeds;
	new_speeds_selection->threshold_lvl_calibration_near = DEFAULT_SPEED_CALIBRATION_FACTOR;
	new_speeds_selection->threshold_lvl_calibration_mid = DEFAULT_SPEED_CALIBRATION_FACTOR;
	new_speeds_selection->threshold_lvl_calibration_far = DEFAULT_SPEED_CALIBRATION_FACTOR;
	new_speeds_selection->threshold_lvl_calibration_vfar = DEFAULT_SPEED_CALIBRATION_FACTOR;
	new_speeds_selection->threshold_snr_calibration_near = DEFAULT_SPEED_CALIBRATION_FACTOR;
	new_speeds_selection->threshold_snr_calibration_mid = DEFAULT_SPEED_CALIBRATION_FACTOR;
	new_speeds_selection->threshold_snr_calibration_far = DEFAULT_SPEED_CALIBRATION_FACTOR;
	new_speeds_selection->threshold_snr_calibration_vfar = DEFAULT_SPEED_CALIBRATION_FACTOR;

	for(i=0; i<new_speeds_selection->max_num_speeds; i++){		//First set everything to 0
		new_speeds_selection->speeds_positions[i] = 0;
		new_speeds_selection->speeds[i]	= 0;
		new_speeds_selection->speeds_levels[i] = 0;
	}

	return new_speeds_selection;
}


retval_t remove_speeds_selection(speeds_selection_t* speeds_selection){
	if(speeds_selection != NULL){
		if ((speeds_selection->speeds != NULL) && (speeds_selection->speeds_levels != NULL)&& (speeds_selection->speeds_positions != NULL)){
			vPortFree(speeds_selection->speeds);
			vPortFree(speeds_selection->speeds_levels);
			vPortFree(speeds_selection->speeds_positions);
		}
		else{
			return RET_ERROR;
		}
		vPortFree(speeds_selection);
		return RET_OK;
	}
	else{
		return RET_ERROR;
	}
}

retval_t set_max_num_speeds(speeds_selection_t* speeds_selection, uint16_t max_num_speeds){
	if(speeds_selection != NULL){
		vPortFree(speeds_selection->speeds);
		vPortFree(speeds_selection->speeds_levels);
		vPortFree(speeds_selection->speeds_positions);

		speeds_selection->max_num_speeds = max_num_speeds;

		speeds_selection->speeds = (float32_t*) pvPortMalloc(max_num_speeds*sizeof(float32_t));
		speeds_selection->speeds_levels = (float32_t*) pvPortMalloc(max_num_speeds*sizeof(float32_t));
		speeds_selection->speeds_positions = (uint16_t*) pvPortMalloc(max_num_speeds*sizeof(uint16_t));

		return RET_OK;
	}
	else{
		return RET_ERROR;
	}
}

retval_t do_speeds_calculation(speeds_selection_t* speeds_selection, peaks_selection_t* peaks_selection, modulation_scheme_t* mod_schemes, fft_signal_t* fft_signal){

	if ((speeds_selection != NULL) && (peaks_selection != NULL) && (mod_schemes != NULL) && (fft_signal != NULL)){
		uint16_t i = 0;

		int16_t temp_speed = 0;

		uint16_t no_speed_flag = 0;

		switch(mod_schemes->mod[mod_schemes->current_scheme]){
		case UPRAMP:				//In upramp do nothing
			break;
		case DOWNRAMP:				//In downramp do nothing
			break;
		case CONSTANT:				//If constant there are just speeds

			speeds_selection->current_num_speeds = 0;			//First set everything to 0

			for(i=0; i<speeds_selection->max_num_speeds; i++){
				speeds_selection->speeds_positions[i] = 0;
				speeds_selection->speeds[i]	= 0;
				speeds_selection->speeds_levels[i] = 0;
			}

			for(i=0; i<peaks_selection->current_num_peaks; i++){	//Now select the peaks as speeds. They could be possitive or Negative

				if(speeds_selection->current_num_speeds>= speeds_selection->max_num_speeds){
					break;
				}

				temp_speed = (int16_t) (peaks_selection->peak_positions[i] - (fft_signal->effective_sample_number/2));

				no_speed_flag = 0;

				//Descarto los picos por debajo de los thresholds
				if((temp_speed < DEFAULT_FAR_SPEEDS_SAMPLES) ||(temp_speed > ((-1)*DEFAULT_FAR_SPEEDS_SAMPLES))){		//Very Far peaks	>23m
					if((peaks_selection->peak_levels[i] < (speeds_selection->threshold_lvl_calibration_vfar*DEFAULT_SPEED_SIGNAL_THR)) || (peaks_selection->snr_levels[i] < (speeds_selection->threshold_snr_calibration_vfar*DEFAULT_SPEED_SNR_THR))){
						no_speed_flag = 1;
					}
				}
				else if((temp_speed < DEFAULT_MID_SPEEDS_SAMPLES) ||(temp_speed > ((-1)*DEFAULT_MID_SPEEDS_SAMPLES))){	//Far Peaks			>10m
					if((peaks_selection->peak_levels[i] < (speeds_selection->threshold_lvl_calibration_far*DEFAULT_SPEED_SIGNAL_THR)) || (peaks_selection->snr_levels[i] < (speeds_selection->threshold_snr_calibration_far*DEFAULT_SPEED_SNR_THR))){
						no_speed_flag = 1;
					}
				}
				else if((temp_speed < DEFAULT_NEAR_SPEEDS_SAMPLES) ||(temp_speed > ((-1)*DEFAULT_NEAR_SPEEDS_SAMPLES))){//Mid Peaks 		>5m
					if((peaks_selection->peak_levels[i] < (speeds_selection->threshold_lvl_calibration_mid*DEFAULT_SPEED_SIGNAL_THR)) || (peaks_selection->snr_levels[i] < (speeds_selection->threshold_snr_calibration_mid*DEFAULT_SPEED_SNR_THR))){
						no_speed_flag = 1;
					}
				}
				else{																									//Near Peaks		<5m
					if((peaks_selection->peak_levels[i] < (speeds_selection->threshold_lvl_calibration_near*DEFAULT_SPEED_SIGNAL_THR)) || (peaks_selection->snr_levels[i] < (speeds_selection->threshold_snr_calibration_near*DEFAULT_SPEED_SNR_THR))){
						no_speed_flag = 1;
					}
				}

				//Descarto los picos cercanos a 0
				if((temp_speed > ((-1)*speeds_selection->num_guard_low_speeds)) && (temp_speed < speeds_selection->num_guard_low_speeds)){
					no_speed_flag = 1;
				}


				if(!no_speed_flag){
					speeds_selection->speeds[speeds_selection->current_num_speeds] = (float32_t) temp_speed * (C_SPEED/(1000*4*TIME_ADC_SAMPLE_MS*CENTER_FREQ_MHZ*fft_signal->fft_sample_number)) * speeds_selection->calibration_factor;
					speeds_selection->speeds_positions[speeds_selection->current_num_speeds] = peaks_selection->peak_positions[i];
					speeds_selection->speeds_levels[speeds_selection->current_num_speeds] = peaks_selection->peak_levels[i];
					speeds_selection->current_num_speeds++;
				}
			}


			break;
		default:
			break;
		}
	return RET_OK;
	}
	else{
		return RET_ERROR;
	}
}
