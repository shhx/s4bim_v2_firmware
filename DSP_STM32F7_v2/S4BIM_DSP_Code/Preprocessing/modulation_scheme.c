/*
 * Copyright (c) 2016, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * modulation_scheme.c
 *
 *  Created on: 30/5/2016
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@die.upm.es>
 *
 */
/**
 * @file modulation_scheme.c
 */


#include "modulation_scheme.h"


/**
 * @brief	Constructor to create a new modulation schemes structure
 * @return	Returns the created modulation shcemes structure
 */
modulation_scheme_t* mod_schemes_init(){

	modulation_scheme_t* new_mod_schemes = (modulation_scheme_t*) pvPortMalloc(sizeof(modulation_scheme_t));
	new_mod_schemes->num_schemes = 0;
	new_mod_schemes->current_scheme = 0;
	new_mod_schemes->next_scheme = 0;
	return new_mod_schemes;
}

/**
 * @brief				Destructor to remove an existing modulation schemes struct
 * @param mod_schemes	The  modulation schemes struct to be removed
 * @return				Returns RET_OK when the  modulation schemes struct is removed and returns RET_ERROR if the structure does not exist
 */
retval_t mod_schemes_deInit(modulation_scheme_t* mod_schemes){
	if(mod_schemes != NULL){
		vPortFree(mod_schemes);
		return RET_OK;
	}
	else{
		return RET_ERROR;
	}
}

/**
 * @brief					Add a ramp modulation scheme to a modulation_scheme_t struct
 * @param mod_schemes		Modulation_scheme_t struct where the ramp will be stored
 * @param adc_iq_signal		IQ signal struct containing the ADC sample number and guard samples
 * @param Vmax				Max voltage of the ramp
 * @param Vmin				Min voltage of the ramp
 * @param time				Time of the ramp in us. This includes the premodulation ramp time
 * @param adc_samples		Number of adc samples to capture the ramp
 * @param upramp_downramp	Flag to select the direction of the ramp, upramp or downramp
 * @return
 */
retval_t add_ramp_scheme(modulation_scheme_t* mod_schemes, adc_iq_signal_t* adc_iq_signal, float32_t Vmax, float32_t Vmin, uint16_t time, uint16_t adc_samples, modulation_t modulation){

	uint16_t i;
	uint16_t current_time = 0;
	uint16_t current_value;
	float32_t current_volts;

	uint16_t min_value = VMIN_0V_NUMBER + (uint16_t) (Vmin/V_SAMPLE_DAC);
	uint16_t max_value = VMIN_0V_NUMBER + (uint16_t) (Vmax/V_SAMPLE_DAC);

	float32_t slope = (float32_t) (Vmax-Vmin)/time;

	if(mod_schemes->num_schemes >= MAX_NUM_SCHEMES){
		return	RET_ERROR;						//No more space available for new modulation schemes
	}

	if((Vmax > VMAX) || (Vmin < VMIN)){
		return RET_ERROR;						//Vmax or Vmin excess HW limits
	}

	if((uint16_t) time < (uint16_t) ((adc_samples*TIME_ADC_SAMPLE_US) + PREMOD_RAMP_TIME_US + (adc_iq_signal->guard_samples*TIME_ADC_SAMPLE_US))){	//The ADC must have enough time to capture the ramp
		return RET_ERROR;
	}

	mod_schemes->sig_prev[mod_schemes->num_schemes] = (uint16_t*) pvPortMalloc((PREMOD_CONSTANT_SAMPLES + PREMOD_RAMP_SAMPLES)*sizeof(uint16_t));

	mod_schemes->num_samples_dac[mod_schemes->num_schemes] = (time-PREMOD_RAMP_TIME_US)/TIME_DAC_SAMPLE_US + 1;

	mod_schemes->signal[mod_schemes->num_schemes] = (uint16_t*) pvPortMalloc(mod_schemes->num_samples_dac[mod_schemes->num_schemes]*sizeof(uint16_t));

	mod_schemes->num_samples_adc[mod_schemes->num_schemes] = adc_samples;

	mod_schemes->Vmax[mod_schemes->num_schemes] = Vmax;

	mod_schemes->Vmin[mod_schemes->num_schemes] = Vmin;

	mod_schemes->bandwidth[mod_schemes->num_schemes] = (Vmax-Vmin)/DEFAULT_BANDWIDTH_FACTOR;

	mod_schemes->mod[mod_schemes->num_schemes] = modulation;

	mod_schemes->time[mod_schemes->num_schemes] = time;

	if (modulation == UPRAMP){

		for(i=0; i<PREMOD_CONSTANT_SAMPLES; i++){

			mod_schemes->sig_prev[mod_schemes->num_schemes][i] = min_value;
		}

		for(i=PREMOD_CONSTANT_SAMPLES; i<PREMOD_RAMP_SAMPLES+PREMOD_CONSTANT_SAMPLES; i++){
			current_volts = Vmin + (current_time*slope);
			current_value = VMIN_0V_NUMBER + (uint16_t) (current_volts/V_SAMPLE_DAC);
			mod_schemes->sig_prev[mod_schemes->num_schemes][i] = current_value;
			current_time += TIME_DAC_SAMPLE_US;
		}

		for(i=0; i<mod_schemes->num_samples_dac[mod_schemes->num_schemes] - 1; i++){
			current_volts = Vmin + (current_time*slope);
			current_value = VMIN_0V_NUMBER + (uint16_t) (current_volts/V_SAMPLE_DAC);
			mod_schemes->signal[mod_schemes->num_schemes][i] = current_value;
			current_time += TIME_DAC_SAMPLE_US;
		}

		mod_schemes->signal[mod_schemes->num_schemes][i] = 	VAVG_5V_NUMBER;//The last value is always the avg voltage
	}
	else if(modulation == DOWNRAMP){

		for(i=0; i<PREMOD_CONSTANT_SAMPLES; i++){

			mod_schemes->sig_prev[mod_schemes->num_schemes][i] = max_value;
		}

		for(i=PREMOD_CONSTANT_SAMPLES; i<PREMOD_RAMP_SAMPLES+PREMOD_CONSTANT_SAMPLES; i++){
			current_volts = Vmax - (current_time*slope);
			current_value = VMIN_0V_NUMBER + (uint16_t) (current_volts/V_SAMPLE_DAC);
			mod_schemes->sig_prev[mod_schemes->num_schemes][i] = current_value;
			current_time += TIME_DAC_SAMPLE_US;
		}

		for(i=0; i<mod_schemes->num_samples_dac[mod_schemes->num_schemes] - 1; i++){
			current_volts = Vmax - (current_time*slope);
			current_value = VMIN_0V_NUMBER + (uint16_t) (current_volts/V_SAMPLE_DAC);
			mod_schemes->signal[mod_schemes->num_schemes][i] = current_value;
			current_time += TIME_DAC_SAMPLE_US;
		}

		mod_schemes->signal[mod_schemes->num_schemes][i] = 	VAVG_5V_NUMBER;//The last value is always the avg voltage

	}

	mod_schemes->num_schemes++;

	return RET_OK;
}

retval_t add_const_scheme(modulation_scheme_t* mod_schemes, adc_iq_signal_t* adc_iq_signal, float32_t Vavg, uint16_t time, uint16_t adc_samples){

	uint16_t i;
	uint16_t avg_value = VMIN_0V_NUMBER + (uint16_t) (Vavg/V_SAMPLE_DAC);

	mod_schemes->sig_prev[mod_schemes->num_schemes] = (uint16_t*) pvPortMalloc((PREMOD_CONSTANT_SAMPLES + PREMOD_RAMP_SAMPLES)*sizeof(uint16_t));

	mod_schemes->num_samples_dac[mod_schemes->num_schemes] = (time-PREMOD_RAMP_TIME_US)/TIME_DAC_SAMPLE_US + 1;

	mod_schemes->signal[mod_schemes->num_schemes] = (uint16_t*) pvPortMalloc(mod_schemes->num_samples_dac[mod_schemes->num_schemes]*sizeof(uint16_t));

	mod_schemes->num_samples_adc[mod_schemes->num_schemes] = adc_samples;

	mod_schemes->Vmax[mod_schemes->num_schemes] = Vavg;

	mod_schemes->Vmin[mod_schemes->num_schemes] = Vavg;

	mod_schemes->bandwidth[mod_schemes->num_schemes] = 0;

	mod_schemes->mod[mod_schemes->num_schemes] = CONSTANT;

	mod_schemes->time[mod_schemes->num_schemes] = time;

	if(mod_schemes->num_schemes >= MAX_NUM_SCHEMES){
		return	RET_ERROR;						//No more space available for new modulation schemes
	}

	if((Vavg > VMAX) || (Vavg < VMIN)){
		return RET_ERROR;						//Vmax or Vmin excess HW limits
	}

	if((uint16_t) time < (uint16_t) ((adc_samples*TIME_ADC_SAMPLE_US) + PREMOD_RAMP_TIME_US + (adc_iq_signal->guard_samples*TIME_ADC_SAMPLE_US))){	//The ADC must have enough time to capture the ramp
		return RET_ERROR;
	}

	for(i=0; i<PREMOD_CONSTANT_SAMPLES; i++){

		mod_schemes->sig_prev[mod_schemes->num_schemes][i] = avg_value;
	}

	for(i=PREMOD_CONSTANT_SAMPLES; i<PREMOD_RAMP_SAMPLES+PREMOD_CONSTANT_SAMPLES; i++){
		mod_schemes->sig_prev[mod_schemes->num_schemes][i] = avg_value;
	}

	for(i=0; i<mod_schemes->num_samples_dac[mod_schemes->num_schemes] - 1; i++){
		mod_schemes->signal[mod_schemes->num_schemes][i] = avg_value;
	}

	mod_schemes->signal[mod_schemes->num_schemes][i] = 	VAVG_5V_NUMBER;//The last value is always the avg voltage

	mod_schemes->num_schemes++;

	return RET_OK;
}
