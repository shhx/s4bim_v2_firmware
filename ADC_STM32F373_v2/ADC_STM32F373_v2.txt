Configuration	ADC_STM32F373_v2
STM32CubeMX 	4.21.0
Date	06/17/2017
MCU	STM32F373V8Tx



PERIPHERALS	MODES	FUNCTIONS	PINS
ADC1	IN0	ADC1_IN0	PA0
ADC1	IN1	ADC1_IN1	PA1
ADC1	IN2	ADC1_IN2	PA2
ADC1	IN3	ADC1_IN3	PA3
DAC1	OUT1 Configuration	DAC1_OUT1	PA4
DAC1	OUT2 Configuration	DAC1_OUT2	PA5
I2C1	I2C	I2C1_SCL	PB6
I2C1	I2C	I2C1_SDA	PB7
I2C2	I2C	I2C2_SCL	PF6
I2C2	I2C	I2C2_SDA	PA10
SDADC1	IN0-Differential	SDADC1_AIN0P	PE12
SDADC1	IN0-Differential	SDADC1_AIN0M	PE13
SDADC1	IN2-Differential	SDADC1_AIN2P	PE10
SDADC1	IN2-Differential	SDADC1_AIN2M	PE11
SDADC2	IN6-Differential	SDADC2_AIN6P	PB2
SDADC2	IN6-Differential	SDADC2_AIN6M	PE7
SDADC2	IN8-Differential	SDADC2_AIN8P	PE8
SDADC2	IN8-Differential	SDADC2_AIN8M	PE9
SPI1	Full-Duplex Slave	SPI1_MISO	PB4
SPI1	Full-Duplex Slave	SPI1_MOSI	PB5
SPI1	Full-Duplex Slave	SPI1_SCK	PB3
SPI1	Hardware NSS Input Signal	SPI1_NSS	PA15
SPI2	Full-Duplex Master	SPI2_MISO	PB14
SPI2	Full-Duplex Master	SPI2_MOSI	PB15
SPI2	Full-Duplex Master	SPI2_SCK	PD8
SYS	SysTick	SYS_VS_Systick	VP_SYS_VS_Systick
USB	Device (FS)	USB_DM	PA11
USB	Device (FS)	USB_DP	PA12



Pin Nb	PINs	FUNCTIONs	LABELs
4	PE5	GPIO_Output	
5	PE6	GPIO_Output	
7	PC13	GPIO_Output	
8	PC14-OSC32_IN	GPIO_Output	
9	PC15-OSC32_OUT	GPIO_Output	
13	PF1-OSC_OUT	GPIO_Output	
15	PC0	GPIO_Output	
16	PC1	GPIO_Output	
17	PC2	GPIO_Output	
18	PC3	GPIO_Output	
23	PA0	ADC1_IN0	
24	PA1	ADC1_IN1	
25	PA2	ADC1_IN2	
26	PA3	ADC1_IN3	
29	PA4	DAC1_OUT1	
30	PA5	DAC1_OUT2	
32	PA7	GPIO_Output	
33	PC4	GPIO_Output	
34	PC5	GPIO_Output	
35	PB0	GPIO_Output	
36	PB1	GPIO_Output	
37	PB2	SDADC2_AIN6P	
38	PE7	SDADC2_AIN6M	
39	PE8	SDADC2_AIN8P	
40	PE9	SDADC2_AIN8M	
41	PE10	SDADC1_AIN2P	
42	PE11	SDADC1_AIN2M	
43	PE12	SDADC1_AIN0P	
44	PE13	SDADC1_AIN0M	
53	PB14	SPI2_MISO	
54	PB15	SPI2_MOSI	
55	PD8	SPI2_SCK	
61	PD14	GPIO_Output	
62	PD15	GPIO_Output	
66	PC9	GPIO_Output	
67	PA8	GPIO_Output	
68	PA9	GPIO_Output	
69	PA10	I2C2_SDA	
70	PA11	USB_DM	
71	PA12	USB_DP	
73	PF6	I2C2_SCL	
77	PA15	SPI1_NSS	
80	PC12	GPIO_Output	
81	PD0	GPIO_Output	
82	PD1	GPIO_Output	
83	PD2	GPIO_Output	
84	PD3	GPIO_Output	
89	PB3	SPI1_SCK	
90	PB4	SPI1_MISO	
91	PB5	SPI1_MOSI	
92	PB6	I2C1_SCL	
93	PB7	I2C1_SDA	



SOFTWARE PROJECT

Project Settings : 
Project Name : ADC_STM32F373_v2
Project Folder : C:\Users\LuisAlberto\Dropbox\tfgg\S4BIM_v2\firmware\ADC_STM32F373_v2
Toolchain / IDE : SW4STM32
Firmware Package Name and Version : STM32Cube FW_F3 V1.8.0


Code Generation Settings : 
STM32Cube Firmware Library Package : Copy only the necessary library files
Generate peripheral initialization as a pair of '.c/.h' files per peripheral : Yes
Backup previously generated files when re-generating : No
Delete previously generated files when not re-generated : Yes
Set all free pins as analog (to optimize the power consumption) : No


Toolchains Settings : 
Compiler Optimizations : Balanced Size/Speed






