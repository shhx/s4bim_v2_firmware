/**
 * @file commands_usart.c
 */

#include "commands_usart.h"

extern SemaphoreHandle_t state_mutex_id; 	//!< Extern mutex to change state
extern SemaphoreHandle_t usart_cpl; 	//UART comms semaphore

extern channel_t *channels[4]; //TODO sucio

/**
 * @brief			Constructor to create a new command packet
 * @param command	The initial command of the packet
 * @return			Returns a pointer to the new command packet
 */
command_usart_t* new_command_usart(commands_usart command, void* data) {

	command_usart_t* new_cmd = (command_usart_t*) pvPortMalloc(sizeof(command_usart_t));
	new_cmd->command = command;
	switch (command) {
	case SET_GAIN:
	case SET_ADC_TYPE:
		new_cmd->size = sizeof(uint16_t);
		break;
	case SET_FILTER_FREQ:
		new_cmd->size = sizeof(filter_config_t);
		break;
	case SET_VCO_LIMIT:
		new_cmd->size = sizeof(float);
		break;
	case SET_MODULATION:
	case ADD_MODULATION:
		new_cmd->size = sizeof(uint16_t);
		break;
	case TEST:
		new_cmd->size = sizeof(uint8_t);
		break;
	default:
		new_cmd->size = 0;
		break;
	}

	new_cmd->data = data;
	return new_cmd;
}

retval_t write_command_usart(command_usart_t* cmd) { 	//TODO por ahora no sirve
//
//	HAL_UART_Transmit_IT(&huart3, (uint8_t*) (cmd->command), sizeof(commands_usart));
//	osSemaphoreWait(usart_cpl, osWaitForever);
//	HAL_UART_Transmit_IT(&huart3, (uint8_t*) (cmd->size), sizeof(uint8_t));
//	osSemaphoreWait(usart_cpl, osWaitForever);
//	HAL_UART_Transmit_IT(&huart3, (uint8_t*) cmd->data, cmd->size);
//	osSemaphoreWait(usart_cpl, osWaitForever);
	return RET_OK;
}

/**
 * @brief			Wait for reading a command from the dsp uC. Puts it in a queue.
 * @param queue		Queue to store the received command
 * @return			Returns RET_OK when the command is received
 */
retval_t read_command_from_usart(item* queue) {
	command_usart_t* cmd = new_command_usart(NOPP, NULL);
	HAL_UART_Receive_IT(&huart3, &(cmd->command), sizeof(commands_usart)); //receive command type

//	if (xSemaphoreTake(usart_cpl, 200) != pdTRUE) {
	if (osSemaphoreWait(usart_cpl, 50) == osOK) {
		HAL_UART_Receive_IT(&huart3, &(cmd->size), sizeof(uint8_t)); //receive size of the incoming data
		osSemaphoreWait(usart_cpl, osWaitForever);
		if (cmd->command == NEW_MODULATION) {
			uint16_t number = -1;
			uint16_t signal_size = -1;
			uint16_t sig_prev_size = -1;
			HAL_UART_Receive_IT(&huart3, (uint8_t*) &number, sizeof(uint16_t));
			if (!osSemaphoreWait(usart_cpl, UART_TIMEOUT) == osOK) {
				goto END;
			}
			HAL_UART_Receive_IT(&huart3, (uint8_t*) &signal_size, sizeof(uint16_t));
			if (!osSemaphoreWait(usart_cpl, UART_TIMEOUT) == osOK) {
				goto END;
			}
			HAL_UART_Receive_IT(&huart3, (uint8_t*) &sig_prev_size, sizeof(uint16_t));
			if (!osSemaphoreWait(usart_cpl, UART_TIMEOUT) == osOK) {
				goto END;
			}
			new_modulation(number, signal_size, sig_prev_size);
		} else if (cmd->command == ADD_POINTS_MOD || cmd->command == ADD_POINTS_MOD_PRE) {
			uint16_t number = -1;
			uint16_t num_points = 0;
			uint16_t* points;
			HAL_UART_Receive_IT(&huart3, (uint8_t*) &number, sizeof(uint16_t));
			if (!osSemaphoreWait(usart_cpl, UART_TIMEOUT) == osOK) {
				goto END;
			}
			HAL_UART_Receive_IT(&huart3, (uint8_t*) &num_points, sizeof(uint16_t));
			if (!osSemaphoreWait(usart_cpl, UART_TIMEOUT) == osOK) {
				goto END;
			}
			points = (uint16_t*) pvPortMalloc(num_points * sizeof(uint16_t));
			HAL_UART_Receive_IT(&huart3, (uint8_t*) points, num_points * sizeof(uint16_t));
			if (!osSemaphoreWait(usart_cpl, UART_TIMEOUT) == osOK) {
				free(points);
				goto END;
			}
			if (cmd->command == ADD_POINTS_MOD) {
				add_points_signal(number, points, num_points);
			} else {
				add_points_prev(number, points, num_points);
			}

		} else if (cmd->command == ADD_MODULATION || cmd->command == SET_MODULATION) {
			uint16_t* number = pvPortMalloc(sizeof(uint16_t));
			HAL_UART_Receive_IT(&huart3, (uint8_t*) number, sizeof(uint16_t));
			if (!osSemaphoreWait(usart_cpl, UART_TIMEOUT) == osOK) {
				free(number);
				goto END;
			}
			cmd->data = number;

		} else if (cmd->command == TEST || cmd->command == NOPP) {
			//do nothing
		} else {
			channel_config_t* channel_config = (channel_config_t*) pvPortMalloc(sizeof(channel_config_t));
			HAL_UART_Receive_IT(&huart3, (uint8_t*) &(channel_config->number), sizeof(uint8_t)); //read channel number
			if (!osSemaphoreWait(usart_cpl, UART_TIMEOUT) == osOK) {
				goto END;
			}
			channel_config->config = pvPortMalloc(cmd->size);
			HAL_UART_Receive_IT(&huart3, (uint8_t*) channel_config->config, cmd->size);
			if (!osSemaphoreWait(usart_cpl, UART_TIMEOUT) == osOK) {
				free(channel_config->config);
				goto END;
			}
			cmd->data = channel_config;
		}
		push_queue(queue, cmd); //TODO test queue limit
		return RET_OK;
	}
	END: remove_command_usart(cmd);
	return RET_OK;
}

/**
 * @brief				Dispatch an action when a command is received. Each action has one or more functions associated and are selected by the switch case statement
 * @param command		Command received that should trigger an action
 * @param adc_state		Current adc state structure
 * @param dsp_state		Current dsp state structure
 * @param mod_schemes	Modulation schemes structures containing the different available radar modulations
 * @param adc_iq_signal	Structure containing the signal (IQ samples) captured by the ADC
 * @return				Returns RET_OK if the associated action is succesfully executed and returns RET_ERROR if it is not
 */
retval_t dispatch_command_queue(state_t* adc_state, item* queue) {

	osMutexWait(state_mutex_id, osWaitForever); //wait until state mutex releases

	if (adc_state->state_1 == S1_IDLE || adc_state->state_1 == S1_SENDING) {
		osMutexRelease(state_mutex_id);
		command_usart_t* cmd = pop_queue(queue);
		if (cmd != NULL) {
			HAL_GPIO_TogglePin(GPIOA, GPIO_PIN_7); //debug
			channel_config_t* channel_config = cmd->data; //todo esto no es asi siempre
			switch (cmd->command) {
			case TEST:
				HAL_GPIO_TogglePin(GPIOA, GPIO_PIN_5);
				break;
			case SET_GAIN:
				channel_set_gain(channels[channel_config->number - 1], *(uint16_t*) (channel_config->config));
				vPortFree(channel_config->config);
				break;
			case SET_FILTER_FREQ:
				channel_set_freq(channels[channel_config->number - 1], *(uint32_t*) (channel_config->config));
				vPortFree(channel_config->config);
				break;
			case SET_FILTER_TYPE:
				channel_set_filter(channels[channel_config->number - 1], *((filter_t*) channel_config->config));
				vPortFree(channel_config->config);
				break;
			case SET_ADC_TYPE:
				set_adc_type(channels[channel_config->number - 1], *((ADC_t*) (channel_config->config)));
				vPortFree(channel_config->config);
				break;
			case ADD_MODULATION: {
				uint16_t* number = cmd->data;
				add_modulation(*number);
				break;
			}
			case SET_MODULATION: {
				uint16_t* number = cmd->data;
				set_modulation(*number);
				break;
			}
			case ADD_CHANNEL:
//				create_channel(channels[channel_config->number-1], (uint16_t)*(cmd->data));
				break;
			case RMV_CHANNEL:
				remove_channel(channels[channel_config->number - 1]);
				break;
			default:
				break;
			}
			vPortFree(cmd->data);
			remove_command_usart(cmd);
			return RET_OK;
		}
	}
	osMutexRelease(state_mutex_id);
	return RET_ERROR;
}

/**
 * @brief		Destructor to remove an existing command packet
 * @param cmd	The command packet to be removed
 * @return		Returns RET_OK when the packet is removed and returns RET_ERROR if the packet structure does not exist
 */
retval_t remove_command_usart(command_usart_t* cmd) {
	if (cmd != NULL) {
		vPortFree(cmd);
		return RET_OK;
	} else {
		return RET_ERROR;
	}
}

/**
 * @brief		UART Error Callback function. Halts the uC
 * @param huart	UART Handle struct
 */
void HAL_UART_ErrorCallback(UART_HandleTypeDef *huart) {
	while (1) {
	}
}

/**
 * @brief		UART transfer complete callback.
 * @param huart	UART Handle struct
 */
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart) {
	osSemaphoreRelease(usart_cpl);
}
