/*
 * channel.c
 *
 *  Created on: 17 jun. 2017
 */
#include "channel.h"

channel_t* create_channel(uint8_t number, uint16_t gain, uint32_t freq, filter_t filter_type, ADC_t adc_type,
	uint16_t vco_avg_voltage, uint8_t vco_gain) {

	if (number > 4 || number < 1 || gain > 1024) {
		return NULL;
	}
	if(channels[number-1] != NULL){ //check if the channel exists
		free(channels[number-1]);
	}
	channel_t* new = (channel_t*) pvPortMalloc(sizeof(channel_t));
	new->number = number;
	new->gain = gain;
	pga_set_gain(number, gain);
	new->frequency = freq;
	new->filter_type = filter_type;
	filter_set_freq(number, freq, filter_type);
	new->adc_type = adc_type;
	set_adc_type(new, adc_type);
	vco_set_avg_voltage(number, vco_avg_voltage);
	vco_set_gain(number, vco_gain);
	channels[number-1] = new;
	return new;
}

retval_t channel_set_freq(channel_t* chn, uint32_t freq) {
	
	if (freq < 0 || freq > 50000) {
		return RET_ERROR;
	}
	if(chn != NULL){ //check if the channel exists
		chn->frequency = freq;
		retval_t ret = filter_set_freq(chn->number, freq, chn->filter_type);
		return ret;
	}
	return RET_ERROR;
}

retval_t channel_set_gain(channel_t* chn, uint16_t gain) {
	if (gain < 0 || gain > 1024) {
		return RET_ERROR;
	}
	if(chn != NULL){ //check if the channel exists
		chn->gain = gain;
		retval_t ret = pga_set_gain(chn->number, gain);
		return ret;
	}
	return RET_ERROR;
}

retval_t channel_set_filter(channel_t* chn, filter_t filter_type) {
	if(chn != NULL){ //check if the channel exists
		chn->filter_type = filter_type;
		retval_t ret = filter_set_freq(chn->number, chn->frequency, filter_type);
		return ret;
	}
	return RET_ERROR;
}

retval_t set_adc_type(channel_t* chn, ADC_t adc_type) {
	if(chn != NULL){ //check if the channel exists
		switch (chn->number) {
			case 1:
			if (adc_type == SDADC) {
				HAL_GPIO_WritePin(GPIOC, GPIO_PIN_5, GPIO_PIN_SET);
				HAL_GPIO_WritePin(GPIOB, GPIO_PIN_1, GPIO_PIN_RESET);
			} else {
				HAL_GPIO_WritePin(GPIOB, GPIO_PIN_1, GPIO_PIN_SET);
				HAL_GPIO_WritePin(GPIOC, GPIO_PIN_5, GPIO_PIN_RESET);
			}
			break;
			case 2:
			if (adc_type == SDADC) {
				HAL_GPIO_WritePin(GPIOD, GPIO_PIN_0, GPIO_PIN_SET);
				HAL_GPIO_WritePin(GPIOD, GPIO_PIN_1, GPIO_PIN_RESET);
			} else {
				HAL_GPIO_WritePin(GPIOD, GPIO_PIN_1, GPIO_PIN_SET);
				HAL_GPIO_WritePin(GPIOD, GPIO_PIN_0, GPIO_PIN_RESET);
			}
			break;
			case 3:
			if (adc_type == SDADC) {
				HAL_GPIO_WritePin(GPIOE, GPIO_PIN_5, GPIO_PIN_SET);
				HAL_GPIO_WritePin(GPIOC, GPIO_PIN_13, GPIO_PIN_RESET);
			} else {
				HAL_GPIO_WritePin(GPIOC, GPIO_PIN_13, GPIO_PIN_SET);
				HAL_GPIO_WritePin(GPIOE, GPIO_PIN_5, GPIO_PIN_RESET);
			}
			break;
			case 4:
			if (adc_type == SDADC) {
				HAL_GPIO_WritePin(GPIOC, GPIO_PIN_3, GPIO_PIN_SET);
				HAL_GPIO_WritePin(GPIOC, GPIO_PIN_2, GPIO_PIN_RESET);
			} else {
				HAL_GPIO_WritePin(GPIOC, GPIO_PIN_2, GPIO_PIN_SET);
				HAL_GPIO_WritePin(GPIOC, GPIO_PIN_3, GPIO_PIN_RESET);
			}
			break;
			default:
			return RET_ERROR;
			break;
		}
		return RET_OK;
	}
	return RET_ERROR;

}

retval_t remove_channel(channel_t* chn) {
	if (chn != NULL) {
		vPortFree(chn);
		return RET_OK;
	} else {
		return RET_ERROR;
	}
}
