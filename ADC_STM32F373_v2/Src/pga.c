/*
 * pga.c
 *
 *  Created on: 25 may. 2017
 */

#include "pga.h"

const uint8_t gains[] = {1, 2, 4, 5, 8, 10, 16, 32};

/*
 * [0, 1, 2, 3, 4,  5,  6,  7]
 * {1, 2, 4, 5, 8, 10, 16, 32}
 * [0, 1, 2, 4, 5, 8, 10, 16, 20, 25, 32, 40, 50, 64, 80, 100, 128, 160, 256, 320, 512, 1024]
 */
retval_t pga_set_gain(uint8_t channel, uint8_t gain) {
	uint8_t reg_gain = REG_GAIN;
//	uint8_t reg_channel = REG_CHANNEL;
	uint8_t ind1 = 0;
	uint8_t ind2 = 0;
	for (int i = 0; i < 8; ++i) {// calculate the nearest gain for each PGA
		for (int j = 0; j < 8; ++j) {
			uint16_t temp = gains[i]*gains[j];
			if(abs(temp - gain) < abs(gains[ind1]*gains[ind2] - gain)){
				ind1 = i;
				ind2 = j;
			}
		}
	}
	switch (channel) {
	case 1:
		HAL_GPIO_WritePin(GPIOD, GPIO_PIN_14, GPIO_PIN_RESET); //PD14
		HAL_SPI_Transmit(&hspi2, &reg_gain, 1, 100);
		HAL_SPI_Transmit(&hspi2, &ind1, 1, 100);
		HAL_GPIO_WritePin(GPIOD, GPIO_PIN_14, GPIO_PIN_SET);
		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_0, GPIO_PIN_RESET); //PB0
		HAL_SPI_Transmit(&hspi2, &reg_gain, 1, 100);
		HAL_SPI_Transmit(&hspi2, &ind2, 1, 100);
		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_0, GPIO_PIN_SET);
		break;
	case 2:
		HAL_GPIO_WritePin(GPIOD, GPIO_PIN_3, GPIO_PIN_RESET); //PD3
		HAL_SPI_Transmit(&hspi2, &reg_gain, 1, 100);
		HAL_SPI_Transmit(&hspi2, &ind1, 1, 100);
		HAL_GPIO_WritePin(GPIOD, GPIO_PIN_3, GPIO_PIN_SET);
		HAL_GPIO_WritePin(GPIOD, GPIO_PIN_2, GPIO_PIN_RESET); //PD2
		HAL_SPI_Transmit(&hspi2, &reg_gain, 1, 100);
		HAL_SPI_Transmit(&hspi2, &ind2, 1, 100);
		HAL_GPIO_WritePin(GPIOD, GPIO_PIN_2, GPIO_PIN_SET);
		break;
	case 3:
		HAL_GPIO_WritePin(GPIOC, GPIO_PIN_15, GPIO_PIN_RESET); //PC15
		HAL_SPI_Transmit(&hspi2, &reg_gain, 1, 100);
		HAL_SPI_Transmit(&hspi2, &ind1, 1, 100);
		HAL_GPIO_WritePin(GPIOC, GPIO_PIN_15, GPIO_PIN_SET);
		HAL_GPIO_WritePin(GPIOC, GPIO_PIN_14, GPIO_PIN_RESET); //PC14
		HAL_SPI_Transmit(&hspi2, &reg_gain, 1, 100);
		HAL_SPI_Transmit(&hspi2, &ind2, 1, 100);
		HAL_GPIO_WritePin(GPIOC, GPIO_PIN_14, GPIO_PIN_SET);
		break;
	case 4:
		HAL_GPIO_WritePin(GPIOC, GPIO_PIN_1, GPIO_PIN_RESET); //PC1
		HAL_SPI_Transmit(&hspi2, &reg_gain, 1, 100);
		HAL_SPI_Transmit(&hspi2, &ind1, 1, 100);
		HAL_GPIO_WritePin(GPIOC, GPIO_PIN_1, GPIO_PIN_SET);

		HAL_GPIO_WritePin(GPIOF, GPIO_PIN_1, GPIO_PIN_RESET); //PF1
		HAL_SPI_Transmit(&hspi2, &reg_gain, 1, 100);
		HAL_SPI_Transmit(&hspi2, &ind2, 1, 100);
		HAL_GPIO_WritePin(GPIOF, GPIO_PIN_1, GPIO_PIN_SET);
		break;
	default:
		return RET_ERROR;
	}
	return RET_OK;
}

