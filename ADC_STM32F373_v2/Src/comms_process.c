/*
 * comms_process.c
 *
 *  Created on: 25 may. 2017
 */

#include "stm32f3xx.h"

#include "gpio.h"

#include "cmsis_os.h"
#include "commands_usart.h"
#include "state.h"
#include "list.h"

extern state_t* adc_state; //TODO sucio

osSemaphoreId (usart_cpl); 	//UART comms semaphore
osSemaphoreDef(usart_cpl_semaphore);

void comms_process() {

	item *queue; 	// Points to the first item in the queue.
	if(initList(&queue) != RET_OK){
		return;
	}

	usart_cpl = osSemaphoreCreate(osSemaphore(usart_cpl_semaphore), 1);
	osSemaphoreWait(usart_cpl, osWaitForever);

//	adc_state->state_1 = S1_IDLE;
//	adc_state->state_0 = S1_RUNNING;

	while (1) {
		read_command_from_usart(queue);		//Blocks until a command arrives or timeouts
		dispatch_command_queue(adc_state, queue);
	}
}
