/*
 * Copyright (c) 2016, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * commands.c
 *
 *  Created on: 19/5/2016
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@die.upm.es>
 *
 */
/**
 * @file commands.c
 */

#include "commands.h"

/**
 * Flag to mark an spi transmision complete. Used by HAL_SPI_TxRxCpltCallback interrupt
 */
//uint16_t spi_cpl = 0;
extern SemaphoreHandle_t spi_cpl;				//!< Extern flag to mark an spi transfer complete. Used by HAL_SPI_TxRxCpltCallback interrupt
extern SemaphoreHandle_t state_mutex_id; 		//!< Extern mutex to change state

//Action functions declaration//
retval_t next_action(state_t* adc_state, state_t* dsp_state, modulation_set_t* modulations, adc_iq_signal_t* adc_iq_signal);

/**
 * @brief			Constructor to create a new command packet
 * @param command	The initial command of the packet
 * @param param_0	The first 2 byte param
 * @param param_1	The second 2 byte param
 * @param param_2	The third 2 byte param
 * @return			Returns a pointer to the new command packet
 */
command_t* new_command(commands command, uint16_t param_0, uint16_t param_1, uint16_t param_2) {

	command_t* new_cmd = (command_t*) pvPortMalloc(sizeof(command_t));
	new_cmd->command = command;
	new_cmd->params[0] = param_0;
	new_cmd->params[1] = param_1;
	new_cmd->params[2] = param_2;
	return new_cmd;
}

/**
 * @brief		Destructor to remove an existing command packet
 * @param cmd	The command packet to be removed
 * @return		Returns RET_OK when the packet is removed and returns RET_ERROR if the packet structure does not exist
 */
retval_t remove_command(command_t* cmd) {
	if (cmd != NULL) {
		vPortFree(cmd);
		return RET_OK;
	} else {
		return RET_ERROR;
	}
}

/**
 * @brief			Wait for reading a command from the dsp uC. Blocking call. The adc state is also sent simultaneously to the dsp (SPI Full Duplex).
 * 					This function enables data ready pin (low level) before receiving
 * @param command	Packet structure to store the received command
 * @param state		ADC state to be sent to the dsp
 * @return			Returns RET_OK when the command is received
 */
retval_t read_command_from_spi(command_t* command, state_t* state) {

	HAL_SPI_TransmitReceive_DMA(&hspi3, (uint8_t*) state, (uint8_t*) command, sizeof(command_t) / 2);  //16 bit transmissions

	osMutexWait(state_mutex_id, osWaitForever);  //wait until state mutex releases
	state->state_1 = S1_IDLE;
	osMutexRelease(state_mutex_id);

	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_8, RESET);
	osSemaphoreWait(spi_cpl, osWaitForever);

	osMutexWait(state_mutex_id, osWaitForever);  //wait until state mutex releases
	state->state_1 = S1_RUNNING;
	osMutexRelease(state_mutex_id);

	return RET_OK;
}

/**
 * @brief			Wait for sending a command to the dsp uC. Blocking Call. The dsp state is also received simultaneously from the dsp (SPI Full Duplex)
 *  				This function enables data ready pin (low level) before transmitting
 * @param command	Packet structure containing the command to be sent
 * @param state		DSP state structure to store the received state
 * @return			Returns RET_OK	when the command is sent
 */
retval_t write_command_to_spi(command_t* command, state_t* state) {

	HAL_SPI_TransmitReceive_DMA(&hspi3, (uint8_t*) command, (uint8_t*) state, sizeof(command_t) / 2);  //16 bit transmissions

	osMutexWait(state_mutex_id, osWaitForever);  //wait until state mutex releases
	state->state_1 = S1_IDLE;
	osMutexRelease(state_mutex_id);

	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_8, RESET);
	osSemaphoreWait(spi_cpl, osWaitForever);


	osMutexWait(state_mutex_id, osWaitForever);  //wait until state mutex releases
	state->state_1 = S1_RUNNING;
	osMutexRelease(state_mutex_id);

	return RET_OK;
}

/**
 * @brief				Dispatch an action when a command is received. Each action has one or more functions associated and are selected by the switch case statement
 * @param command		Command received that should trigger an action
 * @param adc_state		Current adc state structure
 * @param dsp_state		Current dsp state structure
 * @param mod_schemes	Modulation schemes structures containing the different available radar modulations
 * @param adc_iq_signal	Structure containing the signal (IQ samples) captured by the ADC
 * @return				Returns RET_OK if the associated action is succesfully executed and returns RET_ERROR if it is not
 */
retval_t dispatch_command(command_t* command, state_t* adc_state, state_t* dsp_state, modulation_set_t* set,
		adc_iq_signal_t* adc_iq_signal) {

	switch (command->command) {

	case NOP:
		break;
	case CURRENT_NEXT:
		if (next_action(adc_state, dsp_state, set, adc_iq_signal) == RET_OK) {
			return RET_OK;
		} else {
			return RET_ERROR;
		}
		break;
	case SET_CURRENT_AND_NEXT:
		//todo
//		if(set->modulations[command->params[0]] != NULL && set->modulations[command->params[0]]->valid){
//			set->curret_modulation = (uint16_t) command->params[0];
//		}
		if (next_action(adc_state, dsp_state, set, adc_iq_signal) == RET_OK) {
			return RET_OK;
		} else {
			return RET_ERROR;
		}
		break;
	case STOP:
		break;
	case SLEEP:
		break;
	case ACK:
		break;
	case ACK_NEXT:
		break;
	default:
		break;

	}

	return RET_OK;
}

/**
 * @brief				This action is executed when a NEXT command is received. It first sends the ACK_NEXT command to the dsp.
 * 						Then if both adc and dsp are in state S1_RUNNING, the adc starts generating the signal with the DAC and capturing the
 * 						IQ samples with the ADC. After complete capturing, the IQ signal is sent to the dsp. Then waits for the ACK command
 * @param adc_state		Current adc state structure
 * @param dsp_state		Current dsp state structure
 * @param mod_schemes	Modulation schemes structures containing the different available radar modulations. Only the current modulation will be generated
 * @param adc_iq_signal	Structure containing the signal (IQ samples) captured by the ADC
 * @return				Returns RET_OK if the signal is generated, captured and sent successfully. If not it returns RET_ERROR
 */
retval_t next_action(state_t* adc_state, state_t* dsp_state, modulation_set_t* modulations, adc_iq_signal_t* adc_iq_signal) {

	command_t* cmd = new_command(ACK_NEXT, adc_iq_signal->adc_sample_number, adc_iq_signal->guard_samples, 0);	//Create a new aux command

	write_command_to_spi(cmd, dsp_state);	//Send ACK_NEXT command with the number of bytes that are going to be sent

	if ((dsp_state->state_0 == S1_RUNNING) && (adc_state->state_0 == S1_RUNNING)) {

		adc_state->state_0 = S1_CAPTURING;

		generate_and_capture(adc_iq_signal, modulations);	//Generate the signal to the VCO and capture IQ signals

		send_iq_signal_spi(adc_iq_signal, adc_state);	//Send captured signal to the DSP

		read_command_from_spi(cmd, adc_state);	//Read the ACK command from the DSP
		if (cmd->command != ACK) {
			remove_command(cmd);
			return RET_ERROR;
		}
		adc_state->state_0 = S1_RUNNING;
	} else {

		remove_command(cmd);
		return RET_ERROR;
	}
	remove_command(cmd);
	return RET_OK;
}

/**
 * @brief		SPI Error Callback function. Halts the uC
 * @param hspi	Spi Handle struct
 */
void HAL_SPI_ErrorCallback(SPI_HandleTypeDef *hspi) {
	while (1){
//		HAL_GPIO_TogglePin(GPIOA, GPIO_PIN_7);
//		osDelay(100);
	}
}

/**
 * @brief		Spi transfer complete callback. Disable the data ready pin (high level) and activates the spi_cpl flag
 * @param hspi	Spi Handle struct
 */
void HAL_SPI_TxRxCpltCallback(SPI_HandleTypeDef *hspi) {
	osSemaphoreRelease(spi_cpl);
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_8, SET);
}
