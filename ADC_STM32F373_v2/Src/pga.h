/*
 * pga.h
 *
 *  Created on: 25 may. 2017
 *      Author: CapitanTrueno
 */

#ifndef APPLICATION_USER_S4BIM_ADC_CODE_PGA_H_
#define APPLICATION_USER_S4BIM_ADC_CODE_PGA_H_

#include "types.h"
#include "spi.h"
#include <stdlib.h>

#define REG_GAIN 	0b01000000
#define REG_CHANNEL 0b01000001

retval_t pga_set_gain(uint8_t channel, uint8_t gain);

#endif /* APPLICATION_USER_S4BIM_ADC_CODE_PGA_H_ */
