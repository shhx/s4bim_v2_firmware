/*
 * Copyright (c) 2016, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * main_process.c
 *
 *  Created on: 18/5/2016
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@die.upm.es>
 *
 */
/**
 * @mainpage	This document contains the code information of the STM32F373 uC used for generating and capturing
 * 				the radar signals needed in the project S4BIM.
 */

/**
 * @file main_process.c
 */

#include "stm32f3xx.h"
#include "dma.h"
#include "sdadc.h"
#include "spi.h"
#include "tim.h"
#include "gpio.h"
#include "dac.h"

#include "cmsis_os.h"
#include "modulation.h"
#include "adc_iq_signals.h"
#include "commands.h"
#include "state.h"
#include "channel.h"

#define DEFAULT_VMIN				0			//!<Default min voltage of the ramp
#define	DEFAULT_VMAX				10			//!<Default max voltage of the ramp
#define	DEFAULT_VAVG				5			//!<Default avg constant voltage
#define	DEFAULT_RAMP_TIME			7000		//!<Default ramp time in us
#define	DEFAULT_ADC_SAMPLES			256			//!<Default ADC samples
#define	DEFAULT_RAMP				DOWNRAMP	//!<Default ramp kind
#define DEFAULT_NUM_GUARD_SAMPLES	20			//!<Default ADC guard samples

osMutexDef(state_mutex);    // Declare mutex
osMutexId (state_mutex_id); // Mutex ID
osSemaphoreDef(adc_cpl_semaphore);
osSemaphoreId (adc_cpl);	//Semaphore to indicate ADC conversion completed
osSemaphoreDef(spi_cpl_semaphore);
osSemaphoreId (spi_cpl); 	//SPI comms semaphore
osSemaphoreDef(dac_cpl_semaphore);
osSemaphoreId (dac_cpl);	//Semaphore to indicate ADC conversion completed

state_t* adc_state; //TODO suuuuuuuuuucio

/**
 * @brief	This is the main loop of the uC program. First the gpio pins are initialized and the ADCs are calibrated
 * 			The data structs adc_iq_signal, mod_schemes, command, adc_state and dsp_state are initialized and a new
 * 			ramp scheme is added to the mod_schemes struct. The timers are started and then starts the main while loop.
 * 			It just receives a command and then call the dispatch_command functions that will perform the corresponding
 * 			action to the received command
 */
void main_process() {
	retval_t ret;
	dac_cpl = osSemaphoreCreate(osSemaphore(dac_cpl_semaphore), 1);
//	osSemaphoreWait(dac_cpl, osWaitForever); //generate_and_capture already takes first
	spi_cpl = osSemaphoreCreate(osSemaphore(spi_cpl_semaphore), 1);
	osSemaphoreWait(spi_cpl, osWaitForever);
	adc_cpl = osSemaphoreCreate(osSemaphore(adc_cpl_semaphore), 1);
	osSemaphoreWait(adc_cpl, osWaitForever);
	state_mutex_id = osMutexCreate(osMutex(state_mutex));

	//HAL_GPIO_WritePin(GPIOA, GPIO_PIN_5, SET);

	//SPI Flow control To DSP initially disabled (high level)
	//HAL_GPIO_WritePin(GPIOA, GPIO_PIN_8, SET);

	//Create the initial iq_signal_struct with the default ADC sample size
	adc_iq_signal_t* adc_iq_signal = new_adc_iq_signal(DEFAULT_ADC_SAMPLES,	DEFAULT_NUM_GUARD_SAMPLES);

	modulation_set_t* modulations = mod_set_init();
	add_initial_ramp();
	add_initial_ramp2();

	//Channels
	channels[0] = create_channel(1, 10, 20000, ALT_FILTER, SDADC, 4500, 3); //radar en este
	channels[1] = create_channel(2, 1, 20000, ALT_FILTER, SDADC, 4500, 3);
	channels[2] = create_channel(3, 10, 20000, ALT_FILTER, SDADC, 4500, 3); //radar en este
	channels[3] = create_channel(4, 3, 10000, ALT_FILTER, SDADC, 4500, 3);
	//channel_set_gain(channels[1], 5);

	//Create a command structure
	command_t* cmd = new_command(NOP, 0, 0, 0);

	//Create adc and dsp states structures, and its associated mutex
	adc_state = new_state();
	state_t* dsp_state = new_state();

	//Initialize Timers and Calibrate ADCs
	//HAL_TIM_Base_Start(&htim2);		//START TIMER2
	HAL_TIM_Base_Start(&htim6);		//START DAC TIMER6
	HAL_SDADC_CalibrationStart(&hsdadc2, SDADC_CALIBRATION_SEQ_3);
	HAL_SDADC_PollForCalibEvent(&hsdadc2, 500);
	HAL_SDADC_CalibrationStart(&hsdadc1, SDADC_CALIBRATION_SEQ_3);
	HAL_SDADC_PollForCalibEvent(&hsdadc1, 500);

	adc_state->state_1 = S1_IDLE;
	adc_state->state_0 = S1_RUNNING;

	while (1) {
		read_command_from_spi(cmd, adc_state);	//Waits until a command arrives
		dispatch_command(cmd, adc_state, dsp_state, modulations, adc_iq_signal);
	}
}

/**
 * @brief		This function delays the thread the specified us. Not used
 * @param delay	Number of uS to delay
 */
void delay_us(uint32_t delay) {

	uint32_t t1 = 0;
	uint32_t t2 = 0;

	t1 = __HAL_TIM_GetCounter(&htim2);
	t2 = __HAL_TIM_GetCounter(&htim2);
	while ((t2 - t1) < (delay * 2)) {
		t2 = __HAL_TIM_GetCounter(&htim2);
	}
	__HAL_TIM_SetCounter(&htim2,0);

}

