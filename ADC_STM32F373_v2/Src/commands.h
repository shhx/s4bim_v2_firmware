/*
 * Copyright (c) 2016, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgment:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * commands.h
 *
 *  Created on: 19/5/2016
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@die.upm.es>
 *
 */
/**
 * @file commands.h
 */

#ifndef APPLICATION_USER_S4BIM_ADC_CODE_COMMANDS_H_
#define APPLICATION_USER_S4BIM_ADC_CODE_COMMANDS_H_

#include "types.h"
#include "state.h"
#include "adc_iq_signals.h"
#include "spi.h"
#include "gpio.h"
#include "cmsis_os.h"

/**
 * @brief	Commands available to be sent or received by the ADC uC
 */
typedef enum {
	NOP = 65535,             //!< Do nothing command
	CURRENT_NEXT = 1,        //!< Command to generate the current signal in mod_schemes, capture with adc and send IQ samples using SPI
	SET_CURRENT_AND_NEXT = 2,//!< Command set the current signal in mod_schemes, generate it, capture with adc and send IQ samples using SPI
	STOP = 3,                //!< Command to stop an action
	SLEEP = 4,               //!< Command to sleep an uC
	ACK_NEXT = 5,            //!< Ack command sent after a Next command is received. This command must contain as params the number of adc samples and the number of guard samples
	ACK = 6,                 //!< General Ack command
	NACK = 7,                //!< General Nack command
}commands;


/**
 * @brief	Command packet struct. It contains a commands and 6 bytes of params that could be sent with the command. It has the same size (8) as state_t
 */
typedef struct command{
	commands command;	//!< Current command stored in the packet
	uint16_t params[3];	//!< Arguments that can be sent or received in a command packet
}command_t;

command_t* new_command(commands command, uint16_t param_0, uint16_t param_1, uint16_t param_2);
retval_t remove_command(command_t* cmd);

retval_t read_command_from_spi(command_t* command, state_t* state);
retval_t write_command_to_spi(command_t* command, state_t* state);

retval_t dispatch_command(command_t* command, state_t* adc_state, state_t* dsp_state, modulation_set_t* set, adc_iq_signal_t* adc_iq_signal);

#endif /* APPLICATION_USER_S4BIM_ADC_CODE_COMMANDS_H_ */




