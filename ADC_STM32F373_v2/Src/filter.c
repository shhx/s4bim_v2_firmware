/*
 * filter.c
 *
 *  Created on: 17 jun. 2017
 */

#include "filter.h"


retval_t filter_set_freq(uint8_t channel, uint32_t freq, filter_t filter_type) {

	switch (filter_type) {
	case SCF_FILTER:{
		uint16_t period = 60e6/100/freq-1;
		__HAL_TIM_SET_AUTORELOAD(&htim19, period);
		HAL_TIM_OC_Start(&htim19, TIM_CHANNEL_1);		//START filter timer19
		HAL_TIM_OC_Start(&htim19, TIM_CHANNEL_2);
		break;
	}
	case ALT_FILTER:
		HAL_TIM_OC_Stop(&htim19, TIM_CHANNEL_1);
		HAL_TIM_OC_Stop(&htim19, TIM_CHANNEL_2);
		set_freq_alternative(channel, freq);
		break;
	}
	return RET_OK;
}
/*
 * chevishev filter, resistances calculated using "Filter Lab", C11 = 0.015uF C12 = 0.0022uF
 */
retval_t set_freq_alternative(uint8_t channel, uint32_t freq) {
	uint16_t resistance[2];
	if (freq <= 1000) {
		resistance[0] = 31730;
		resistance[1] = 34170;
	} else if (freq <= 1500) {
		resistance[0] = 21500;
		resistance[1] = 22600;
	} else if (freq <= 2000) {
		resistance[0] = 16200;
		resistance[1] = 16900;
	} else if (freq <= 3000) {
		resistance[0] = 10700;
		resistance[1] = 11300;
	} else if (freq <= 4000) {
		resistance[0] = 8060;
		resistance[1] = 8450;
	} else if (freq <= 5000) {
		resistance[0] = 6340;
		resistance[1] = 6810;
	} else if (freq <= 6000) {
		resistance[0] = 5230;
		resistance[1] = 5760;
	} else if (freq <= 7000) {
		resistance[0] = 4530;
		resistance[1] = 4870;
	} else if (freq <= 8000) {
		resistance[0] = 3920;
		resistance[1] = 4320;
	} else if (freq <= 9000) {
		resistance[0] = 3480;
		resistance[1] = 3830;
	} else if (freq <= 10000) {
		resistance[0] = 3170;
		resistance[1] = 3420;
	} else if (freq <= 20000) {
		resistance[0] = 1590;
		resistance[1] = 1710;
	} else if (freq <= 30000) {
		resistance[0] = 1060;
		resistance[1] = 1140;
	} else if (freq <= 40000) {
		resistance[0] = 793;
		resistance[1] = 854;
	} else if (freq <= 50000) {
		resistance[0] = 634;
		resistance[1] = 683;
	} else if (freq <= 60000) {
		resistance[0] = 529;
		resistance[1] = 569;
	} else if (freq <= 70000) {
		resistance[0] = 453;
		resistance[1] = 488;
	} else if (freq <= 80000) {
		resistance[0] = 397;
		resistance[1] = 427;
	} else if (freq <= 90000) {
		resistance[0] = 353;
		resistance[1] = 380;
	} else if (freq <= 100000) {
		resistance[0] = 317;
		resistance[1] = 342;
	} else {
		return RET_ERROR;
	}
	spi_write_filter_res(channel, resistance[0], resistance[1]);
	return RET_OK;
}

/*
 * res1 -> resA
 * res2 -> resB
 */
retval_t spi_write_filter_res(uint8_t channel, uint16_t res1, uint16_t res2) {
	uint8_t resA = (res1 - 250) / 195.31f;
	uint8_t resB = (res2 - 250) / 195.31f; //todo no truncar
	uint8_t wiperA = WIPERA;
	uint8_t wiperB = WIPERB;
	switch (channel) {
	case 1:
		HAL_GPIO_WritePin(GPIOC, GPIO_PIN_4, GPIO_PIN_RESET); //PC4
		HAL_SPI_Transmit(&hspi2, &wiperA, 1, 100);
		HAL_SPI_Transmit(&hspi2, &resA, 1, 100);
		HAL_GPIO_WritePin(GPIOC, GPIO_PIN_4, GPIO_PIN_SET);
		HAL_GPIO_WritePin(GPIOC, GPIO_PIN_4, GPIO_PIN_RESET); //PC4
		HAL_SPI_Transmit(&hspi2, &wiperB, 1, 100);
		HAL_SPI_Transmit(&hspi2, &resB, 1, 100);
		HAL_GPIO_WritePin(GPIOC, GPIO_PIN_4, GPIO_PIN_SET);
		break;
	case 2:
		HAL_GPIO_WritePin(GPIOC, GPIO_PIN_12, GPIO_PIN_RESET); //PC12
		HAL_SPI_Transmit(&hspi2, &wiperA, 1, 100);
		HAL_SPI_Transmit(&hspi2, &resA, 1, 100);
		HAL_SPI_Transmit(&hspi2, &wiperB, 1, 100);
		HAL_SPI_Transmit(&hspi2, &resB, 1, 100);
		HAL_GPIO_WritePin(GPIOC, GPIO_PIN_12, GPIO_PIN_SET);
		break;
	case 3:
		HAL_GPIO_WritePin(GPIOE, GPIO_PIN_6, GPIO_PIN_RESET); //PE6
		HAL_SPI_Transmit(&hspi2, &wiperA, 1, 100);
		HAL_SPI_Transmit(&hspi2, &resA, 1, 100);
		HAL_SPI_Transmit(&hspi2, &wiperB, 1, 100);
		HAL_SPI_Transmit(&hspi2, &resB, 1, 100);
		HAL_GPIO_WritePin(GPIOE, GPIO_PIN_6, GPIO_PIN_SET);
		break;
	case 4:
		HAL_GPIO_WritePin(GPIOC, GPIO_PIN_0, GPIO_PIN_RESET); //PC0
		HAL_SPI_Transmit(&hspi2, &wiperA, 1, 100);
		HAL_SPI_Transmit(&hspi2, &resA, 1, 100);
		HAL_GPIO_WritePin(GPIOC, GPIO_PIN_0, GPIO_PIN_SET); //PC0
		HAL_GPIO_WritePin(GPIOC, GPIO_PIN_0, GPIO_PIN_RESET); //PC0
		HAL_SPI_Transmit(&hspi2, &wiperB, 1, 100);
		HAL_SPI_Transmit(&hspi2, &resB, 1, 100);
		HAL_GPIO_WritePin(GPIOC, GPIO_PIN_0, GPIO_PIN_SET);
		break;
	}
	return RET_OK;
}
