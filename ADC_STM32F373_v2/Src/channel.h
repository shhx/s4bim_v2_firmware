/*
 * channel.h
 *
 *  Created on: 17 jun. 2017
 */

#ifndef CHANNEL_H_
#define CHANNEL_H_

#include "types.h"
#include "pga.h"
#include "filter.h"
#include "vco.h"

typedef enum {
	SAR,
	SDADC,
} ADC_t;

typedef struct channel {
	uint8_t		number;
	uint16_t 	gain;
	uint32_t 	frequency;
	filter_t 	filter_type;
	ADC_t		adc_type;
} channel_t;


channel_t *channels[4];

channel_t* create_channel(uint8_t number, uint16_t gain, uint32_t freq, filter_t filter_type, ADC_t adc_type,
		uint16_t vco_avg_voltage, uint8_t vco_gain);
retval_t channel_set_freq(channel_t* chn, uint32_t freq);
retval_t channel_set_gain(channel_t* chn, uint16_t gain);
retval_t channel_set_filter(channel_t* chn, filter_t filter_type);
retval_t set_adc_type(channel_t* chn, ADC_t adc_type);
retval_t channel_reset_adc(channel_t* chn, ADC_t adc_type);
retval_t remove_channel(channel_t* chn);

#endif /* CHANNEL_H_ */
