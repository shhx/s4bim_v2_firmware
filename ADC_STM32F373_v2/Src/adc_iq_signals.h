/*
 * Copyright (c) 2016, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * adc_iq_signals.h
 *
 *  Created on: 18/5/2016
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@die.upm.es>
 *
 */
/**
 * @file adc_iq_signals.h
 */

#ifndef APPLICATION_USER_S4BIM_ADC_CODE_ADC_IQ_SIGNALS_H_
#define APPLICATION_USER_S4BIM_ADC_CODE_ADC_IQ_SIGNALS_H_

#include "dac.h"
#include "dma.h"
#include "sdadc.h"
#include "tim.h"
#include "state.h"
#include "spi.h"
#include "modulation.h"

extern SemaphoreHandle_t adc_cpl;
extern SemaphoreHandle_t dac_cpl;
extern SemaphoreHandle_t spi_cpl;

/**
 * @brief	Contains the pointers to the I and Q signals captured by the ADC. The pointers to these signals are consecutive (first I and then Q), and the first
 * 			samples of each signal are the guard_samples
 */
typedef struct adc_iq_signal{
	int16_t* i_signal;			//!< I_signal pointer. The size of this array is adc_sample_number+guard_samples. The next sample is the first Q guard sample
	int16_t* q_signal;			//!< Q_signal pointer. The size of this array is adc_sample_number+guard_samples.
	uint16_t adc_sample_number;	//!< Number of samples to be captured by the ADC
	uint16_t guard_samples;		//!< Number of guard samples to be captured with the ADC
}adc_iq_signal_t;


adc_iq_signal_t* new_adc_iq_signal(uint16_t adc_sample_number, uint16_t guard_samples);
retval_t remove_adc_iq_signal(adc_iq_signal_t* adc_iq_signal);

retval_t generate_and_capture(adc_iq_signal_t* adc_iq_signal, modulation_set_t* set);

retval_t send_iq_signal_spi(adc_iq_signal_t* adc_iq_signal, state_t* adc_state);


#endif /* APPLICATION_USER_S4BIM_ADC_CODE_ADC_IQ_SIGNALS_H_ */
