/*
 * Copyright (c) 2016, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * modulation_scheme.c
 *
 *  Created on: 18/5/2017
 *
 */
/**
 * @file modulation_scheme.c
 */

#include "modulation.h"

const uint16_t ramp1[288] = { 100, 114, 127, 141, 154, 168, 181, 195, 209, 222, 236, 249, 263, 276, 290, 304, 317, 331, 344, 358, 371, 385, 399, 412,
		426, 439, 453, 466, 480, 494, 507, 521, 534, 548, 561, 575, 589, 602, 616, 629, 643, 656, 670, 684, 697, 711, 724, 738, 751, 765, 779, 792,
		806, 819, 833, 846, 860, 874, 887, 901, 914, 928, 941, 955, 969, 982, 996, 1009, 1023, 1036, 1050, 1064, 1077, 1091, 1104, 1118, 1131, 1145,
		1159, 1172, 1186, 1199, 1213, 1226, 1240, 1254, 1267, 1281, 1294, 1308, 1321, 1335, 1349, 1362, 1376, 1389, 1403, 1416, 1430, 1444, 1457,
		1471, 1484, 1498, 1511, 1525, 1539, 1552, 1566, 1579, 1593, 1606, 1620, 1634, 1647, 1661, 1674, 1688, 1701, 1715, 1729, 1742, 1756, 1769,
		1783, 1796, 1810, 1824, 1837, 1851, 1864, 1878, 1891, 1905, 1919, 1932, 1946, 1959, 1973, 1986, 2000, 2014, 2027, 2041, 2054, 2068, 2081,
		2095, 2109, 2122, 2136, 2149, 2163, 2176, 2190, 2204, 2217, 2231, 2244, 2258, 2271, 2285, 2299, 2312, 2326, 2339, 2353, 2366, 2380, 2394,
		2407, 2421, 2434, 2448, 2461, 2475, 2489, 2502, 2516, 2529, 2543, 2556, 2570, 2584, 2597, 2611, 2624, 2638, 2651, 2665, 2679, 2692, 2706,
		2719, 2733, 2746, 2760, 2774, 2787, 2801, 2814, 2828, 2841, 2855, 2869, 2882, 2896, 2909, 2923, 2936, 2950, 2964, 2977, 2991, 3004, 3018,
		3031, 3045, 3059, 3072, 3086, 3099, 3113, 3126, 3140, 3154, 3167, 3181, 3194, 3208, 3221, 3235, 3249, 3262, 3276, 3289, 3303, 3316, 3330,
		3344, 3357, 3371, 3384, 3398, 3411, 3425, 3439, 3452, 3466, 3479, 3493, 3506, 3520, 3534, 3547, 3561, 3574, 3588, 3601, 3615, 3629, 3642,
		3656, 3669, 3683, 3696, 3710, 3724, 3737, 3751, 3764, 3778, 3791, 3805, 3819, 3832, 3846, 3859, 3873, 3886, 3900, 3914, 3927, 3941, 3954,
		3968, 3981, 2016 };

const uint16_t zeros1[126] = { 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350,
		350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350,
		350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350,
		350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350,
		350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350 };

/**
 * @brief	Constructor to create a new modulation structure
 * @return	Returns the created modulation structure
 */
modulation_outline_t* new_modulation(uint16_t number, uint16_t sig_prev_size, uint16_t signal_size) {
	modulation_outline_t* temp_mod = set->modulations[number];
	if (temp_mod != NULL) { //modulation already exist, let's clear it
		set->curret_modulation = 0;
		vPortFree(temp_mod->sig_prev);
		vPortFree(temp_mod->signal);
		vPortFree(temp_mod);
	}
	modulation_outline_t* modulation = (modulation_outline_t*) pvPortMalloc(sizeof(modulation_outline_t));
	sig_prev_points_count = 0;
	signal_points_count = 0;
	modulation->number = number;
	modulation->sig_prev_size = sig_prev_size;
	modulation->signal_size = signal_size;
	modulation->signal = (uint16_t*) pvPortMalloc(signal_size * sizeof(uint16_t));
	modulation->sig_prev = (uint16_t*) pvPortMalloc(sig_prev_size * sizeof(uint16_t));
	// adc samples = signal time / adc sample time
	modulation->num_samples_adc = modulation->signal_size * TIME_DAC_SAMPLE_US / TIME_ADC_SAMPLE_US + DEFAULT_NUM_GUARD_SAMPLES;
	modulation->num_samples_dac = modulation->sig_prev_size + modulation->signal_size;
	set->modulations[number] = modulation;
	modulation->valid = 0;
	return modulation;
}

retval_t add_points_prev(uint16_t number, uint16_t* points, uint16_t num_points) {
	modulation_outline_t* modulation = set->modulations[number];
	if (modulation == NULL) {
		return RET_ERROR;
	}
	if ((sig_prev_points_count + num_points) > modulation->sig_prev_size) { // trying to put more points than it should
		return RET_ERROR;
	}

	for (int i = 0; i < num_points; ++i) {
		modulation->signal[sig_prev_points_count + i] = points[i];
	}
	sig_prev_points_count += num_points;
	vPortFree(points);
	return RET_OK;
}

retval_t add_points_signal(uint16_t number, uint16_t* points, uint16_t num_points) {
	modulation_outline_t* modulation = set->modulations[number];
	if (modulation == NULL) {
		return RET_ERROR;
	}
	if ((signal_points_count + num_points) > modulation->signal_size) { // trying to put more points than the maxium
		return RET_ERROR;
	}

	for (int i = 0; i < num_points; ++i) {
		modulation->signal[signal_points_count + i] = points[i];
	}
	signal_points_count += num_points;
	vPortFree(points);
	return RET_OK;
}

retval_t check_modulation_ok(uint16_t number) {
	modulation_outline_t* modulation = set->modulations[number];
	// if number of points added are equals to the maximum the modulation it's ok
	if (modulation != NULL && sig_prev_points_count == modulation->sig_prev_size && signal_points_count == modulation->signal_size) {
		return RET_OK;
	}
	return RET_ERROR;
}

retval_t add_modulation(uint16_t number) {
	if (check_modulation_ok(number) == RET_OK) {
		set->modulations[number]->valid = 1;
		return RET_OK;
	}
	return RET_ERROR;
}

retval_t set_modulation(uint16_t number) {
	if (check_modulation_ok(number) == RET_OK) {
		set->curret_modulation = number;
		return RET_OK;
	}
	return RET_ERROR;
}

retval_t rmv_modulation(uint16_t number) {
	if (set->modulations[number] == NULL) {
		return RET_ERROR;
	}
	vPortFree(set->modulations[number]);
	return RET_OK;
}

retval_t add_initial_ramp(void) {

	new_modulation(0, 126, 288);
	uint16_t* points = (uint16_t*) pvPortMalloc(126 * sizeof(uint16_t));
	memcpy(points, zeros1, 126 * sizeof(uint16_t));
	add_points_prev(0, points, 126);
	points = (uint16_t*) pvPortMalloc(288 * sizeof(uint16_t));
	memcpy(points, ramp1, 288 * sizeof(uint16_t));
	add_points_signal(0, points, 288);
	add_modulation(0);
	return RET_OK;
}

retval_t add_initial_ramp2(void) {

	new_modulation(1, 126, 288);
	uint16_t* points = (uint16_t*) pvPortMalloc(126 * sizeof(uint16_t));
	memcpy(points, zeros1, 126 * sizeof(uint16_t));
	add_points_prev(1, points, 126);
	points = (uint16_t*) pvPortMalloc(288 * sizeof(uint16_t));
	memcpy(points, ramp1, 288 * sizeof(uint16_t));
	add_points_signal(1, points, 288);
	add_modulation(1);

	return RET_OK;
}

retval_t add_initial_ramp3(void) {
	uint16_t ramp[288] = { 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350,
			350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350,
			350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350,
			350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350,
			350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350,
			350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350,
			350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350,
			350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350,
			350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350,
			350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350,
			350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350 };

	new_modulation(2, 126, 288);
	uint16_t* points = (uint16_t*) pvPortMalloc(126 * sizeof(uint16_t));
	memcpy(points, zeros1, 126 * sizeof(uint16_t));
	add_points_prev(2, points, 126);
	points = (uint16_t*) pvPortMalloc(288 * sizeof(uint16_t));
	memcpy(points, ramp, 288 * sizeof(uint16_t));
	add_points_signal(2, points, 288);
	add_modulation(2);

	return RET_OK;
}

modulation_set_t* mod_set_init(void) {
	set = (modulation_set_t*) pvPortMalloc(sizeof(modulation_set_t));
	return set;
}
