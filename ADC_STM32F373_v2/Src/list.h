/*
 * list.h
 *
 *  Created on: 7 jun. 2017
 */

#ifndef APPLICATION_S4BIM_ADC_CODE_LIST_H_
#define APPLICATION_S4BIM_ADC_CODE_LIST_H_

#include "types.h"
#include "cmsis_os.h"
#include "commands_usart.h"

#define MAX_LIST_ITEMS 20


uint8_t items_count;

retval_t initList(item** head);
retval_t push_queue(item* head, command_usart_t* cmd);
command_usart_t* pop_queue(item* head);

#endif /* APPLICATION_S4BIM_ADC_CODE_LIST_H_ */
