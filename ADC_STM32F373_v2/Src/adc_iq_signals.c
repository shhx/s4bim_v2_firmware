/*
 * Copyright (c) 2016, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * adc_iq_signals.c
 *
 *  Created on: 18/5/2016
 *
 */
/**
 * @file adc_iq_signals.c
 */

#include "adc_iq_signals.h"

//uint16_t adc_completed = 0;	//!< Flag to mark an adc transfer complete. Used by  HAL_SDADC_ConvCpltCallback
uint16_t dac_completed = 1;  //!< Flag to mark a dac transfer complete. Used by  HAL_DAC_ConvCpltCallbackCh1

/**
 * @brief					Constructor to create a new adc_iq signal with a number of samples and guard samples
 * @param adc_sample_number	Number of samples to capture with the ADC
 * @param guard_samples		Number of guard samples to capture first with the ADC
 * @return					Returns the new adc_iq_signal structure
 */
adc_iq_signal_t* new_adc_iq_signal(uint16_t adc_sample_number, uint16_t guard_samples) {

	adc_iq_signal_t* new_adc_iq = (adc_iq_signal_t*) pvPortMalloc(sizeof(adc_iq_signal_t));
	int16_t* tmp_ptr = (int16_t*) pvPortMalloc(((adc_sample_number + guard_samples) * 2) * sizeof(int16_t));

	new_adc_iq->adc_sample_number = adc_sample_number;
	new_adc_iq->guard_samples = guard_samples;
	new_adc_iq->i_signal = tmp_ptr;
	new_adc_iq->q_signal = tmp_ptr + new_adc_iq->adc_sample_number + new_adc_iq->guard_samples;

	return new_adc_iq;
}

/**
 * @brief				Destructor to remove an adc_iq_signal structure
 * @param adc_iq_signal	ADC IQ signal to remove
 * @return				Returns RET_OK when the ADC IQ signal is removed and returns RET_ERROR if the structure does not exist
 */
retval_t remove_adc_iq_signal(adc_iq_signal_t* adc_iq_signal) {
	if (adc_iq_signal != NULL) {
		if (adc_iq_signal->i_signal != NULL) {
			vPortFree(adc_iq_signal->i_signal);
		} else {
			return RET_ERROR;
		}
		vPortFree(adc_iq_signal);
		return RET_OK;
	} else {
		return RET_ERROR;
	}

}

/**
 * @brief				This function generates the current signal of the mod_schemes struct and captures the radar returned signal
 * 						with the ADC. First the premodulation signal is generated, and nothing is captured at this time. Then the real
 * 						modulation signal is generated and the adc captures the sample_number+guard_samples configured. This function blocks
 * 						till everything is done
 * @param adc_iq_signal	ADC IQ signal where the captured signal will be stored
 * @param mod_schemes	Modulation schemes containing the different schemes to be generated. Only the current_scheme will be generated
 * @return				Returns RET_OK when everything is ok
 */
retval_t generate_and_capture(adc_iq_signal_t* adc_iq_signal, modulation_set_t* set) {

	osSemaphoreWait(dac_cpl, osWaitForever);
	modulation_outline_t* mod = set->modulations[set->curret_modulation];
	__HAL_TIM_SetCounter(&htim6,0);
	HAL_DAC_Start_DMA(&hdac1, DAC_CHANNEL_1, (uint32_t *) mod->sig_prev, mod->sig_prev_size, DAC_ALIGN_12B_R);	//Genera la se�al previa a la rampa

	osSemaphoreWait(dac_cpl, 10);			//Espera a que se haya completado
//	HAL_DAC_Stop_DMA(&hdac1, DAC_CHANNEL_1);			//Para el DAC
//	HAL_GPIO_TogglePin(GPIOA, GPIO_PIN_7);

	__HAL_TIM_SetCounter(&htim6,0);  //Por algun motivo tengo que poner el counter a 0 para que funcione bien
	HAL_DAC_Start_DMA(&hdac1, DAC_CHANNEL_1, (uint32_t *) mod->signal, mod->signal_size, DAC_ALIGN_12B_R);  //Se genera la rampa. Duraci�n 5,4ms aprox
	HAL_SDADC_Start_DMA(&hsdadc2, (uint32_t *) adc_iq_signal->i_signal, adc_iq_signal->adc_sample_number + adc_iq_signal->guard_samples); //Comienza a capturar muestras con el SDADC2
	HAL_SDADC_Start_DMA(&hsdadc1, (uint32_t *) adc_iq_signal->q_signal, adc_iq_signal->adc_sample_number + adc_iq_signal->guard_samples); //Comienza a capturar muestras con el SDADC1

	osSemaphoreWait(adc_cpl, osWaitForever);		//Espero a que se complete la lectura del ADC
//	  adc_completed = 0										//Reset del flag de lectura de ADC completa

	return RET_OK;

}

/**
 * @brief				This function sends a previously captured IQ signal through SPI.
 * @param adc_iq_signal	IQ Signal to be sent
 * @param adc_state		Current adc state. It is modified in the function to the SENDING state
 * @return				Returns RET_OK when everything is done
 */
retval_t send_iq_signal_spi(adc_iq_signal_t* adc_iq_signal, state_t* adc_state) {

	uint16_t* null_rcv = (uint16_t*) pvPortMalloc((adc_iq_signal->adc_sample_number + adc_iq_signal->guard_samples) * 2 * sizeof(uint16_t));

	HAL_SPI_TransmitReceive_DMA(&hspi3, (uint8_t*) adc_iq_signal->i_signal, (uint8_t*) null_rcv,
			(adc_iq_signal->adc_sample_number + adc_iq_signal->guard_samples) * 2);

	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_8, RESET);

	adc_state->state_1 = S1_SENDING;
	osSemaphoreWait(spi_cpl, osWaitForever);
	vPortFree(null_rcv);

	return RET_OK;

}

/**
 * @brief		DAC conversion is done. Puts dac_completed flag high
 * @param hdac	DAC Handle struct
 */
void HAL_DAC_ConvCpltCallbackCh1(DAC_HandleTypeDef* hdac) {
//	dac_completed = 1;
//	HAL_DAC_Stop_DMA(&hdac1, DAC_CHANNEL_1);			//Para el DAC
	osSemaphoreRelease(dac_cpl);
}


/**
 * @brief			ADC conversion is done. Puts the adc_completed flag high when both channels are done
 * @param hsdadc	ADC Handle struct
 */
void HAL_SDADC_ConvCpltCallback(SDADC_HandleTypeDef* hsdadc) {
	if (hsdadc->Instance == SDADC1) {
		HAL_SDADC_Stop_DMA(&hsdadc1);			//Para el ADC
		osSemaphoreRelease(adc_cpl);		//Indica que se ha completado la lectura del ADC. Se usa en el SDADC1 ya que empieza despu�s que el SDADC2
	}
	if (hsdadc->Instance == SDADC2) {
		HAL_SDADC_Stop_DMA(&hsdadc2);
		osSemaphoreRelease(adc_cpl);
	}

}

/**
 * @brief 		DAC Error Callback function. Halts the uC
 * @param hdac	DAC Handle struct
 */
void HAL_DAC_ErrorCallbackCh1(DAC_HandleTypeDef *hdac) {
	while (1) {
//		HAL_GPIO_TogglePin(GPIOA, GPIO_PIN_7);
		osDelay(100);
	}
}

void HAL_DACEx_ConvCpltCallbackCh2(DAC_HandleTypeDef* hdac) {
	HAL_DAC_Stop_DMA(&hdac1, DAC_CHANNEL_2);			//Para el DAC
	osSemaphoreRelease(dac_cpl);
//	dac_completed = 1;

}

void HAL_DACEx_ErrorCallbackCh2(DAC_HandleTypeDef *hdac) {
	while (1) {
//		HAL_GPIO_TogglePin(GPIOA, GPIO_PIN_7);
//		osDelay(100);
	}
}

/**
 * 					ADC Error Callback function. Halts the uC
 * @param hsdadc	ADC Handle struct
 */
void HAL_SDADC_ErrorCallback(SDADC_HandleTypeDef* hsdadc) {
	while (1) {
		osDelay(100);
	}
}
