/*
 * vco.h
 *
 *  Created on: 22 jun. 2017
 *      Author: LuisAlberto
 */

#ifndef APPLICATION_S4BIM_ADC_CODE_VCO_H_
#define APPLICATION_S4BIM_ADC_CODE_VCO_H_


#include "types.h"
#include "math.h"
#include "i2c.h"

#define ADDRESS_A 0b0101010
#define ADDRESS_G 0b0101000

retval_t vco_set_avg_voltage(uint8_t channel, uint16_t voltage);
retval_t vco_set_gain(uint8_t channel, uint8_t gain);

#endif /* APPLICATION_S4BIM_ADC_CODE_VCO_H_ */
