/*
 * Copyright (c) 2016, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * types.h
 *
 *  Created on: 18/5/2016
 *
 */
/**
 * @file 	types.h
 * @brief 	General types used in the project
 */

#ifndef APPLICATION_USER_S4BIM_ADC_CODE_TYPES_H_
#define APPLICATION_USER_S4BIM_ADC_CODE_TYPES_H_

#include "cmsis_os.h"
/**
 * @brief Return values available
 */
typedef enum {
	RET_OK,   //!< Return success
	RET_ERROR,   //!< Return error
} retval_t;

/**
 * @brief	Commands available to be sent or received by the ADC uC
 */
typedef enum {
	SET_VCO_LIMIT,	 	 //!< Command set the DAC output range voltage
	SET_MODULATION,
	ADD_CHANNEL,
	RMV_CHANNEL,
	SET_GAIN,
	SET_FILTER_FREQ,
	SET_FILTER_TYPE,
	SET_ADC_TYPE,
	ADD_POINTS_MOD,
	ADD_POINTS_MOD_PRE,
	ADD_MODULATION,
	NEW_MODULATION,
	TEST = 254,
	NOPP = 255,
} commands_usart;

/**
 * @brief	Command packet struct.
 */
typedef struct command_usart {
	commands_usart command;	//!< Current command stored in the packet
	uint8_t size;			//!< size of the data that can be sent or received in a command packet
	void* data;
} command_usart_t;

typedef struct {
	uint16_t argc;
	char** argv;
	retval_t (* command_func)(uint16_t argc, char** argv);
}
command_data_t;

typedef struct item_struct {
	command_usart_t *command;
	struct item_struct *next;
} item;

#endif /* APPLICATION_USER_S4BIM_ADC_CODE_TYPES_H_ */
