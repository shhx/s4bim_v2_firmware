/*
 * Copyright (c) 2016, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * modulation_scheme.h
 *
 *  Created on: 18/5/2016
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@die.upm.es>
 *
 */
/**
 * @file modulation_scheme.h
 */

#ifndef APPLICATION_USER_S4BIM_ADC_CODE_MODULATION_H_
#define APPLICATION_USER_S4BIM_ADC_CODE_MODULATION_H_

#include "stm32f3xx.h"
#include "cmsis_os.h"
#include "arm_math.h"
#include "types.h"

#define VMAX	10					//!< Max Voltage level in V
#define	VMIN	0					//!< Min Voltage level in V

#define VMAX_10V_NUMBER	3735		//!< Binary number corresponding to 10 V max value
#define VAVG_5V_NUMBER	2150		//!< Binary number corresponding to 5 V avg value
#define	VMIN_0V_NUMBER	350			//!< Binary number corresponding to 0 V min value

#define	V_SAMPLE_DAC	0.003		//!< Resolution of the DAC: 3mV

#define TIME_ADC_SAMPLE_US	20		//!< ADC sample time 50KHz in us: 20
#define TIME_ADC_SAMPLE_MS	0.02f	//!< ADC sample time 50KHz in ms: 0.02

#define TIME_DAC_SAMPLE_US	20		//!< DAC sample time 50KHz in us: 20
#define TIME_DAC_SAMPLE_MS	0.02f	//!< DAC sample time 50KHz in ms: 0.02

#define PREMOD_CONSTANT_TIME_MS	1.26f		//!< Constant signal premodulation time in ms
#define PREMOD_CONSTANT_TIME_US	1260		//!< Constant signal premodulation time in us
#define PREMOD_CONSTANT_SAMPLES	PREMOD_CONSTANT_TIME_US/TIME_DAC_SAMPLE_US	//!< Constant signal premodulation sample number

#define PREMOD_RAMP_TIME_MS	1.26f			//!< Ramp signal premodulation time in ms
#define PREMOD_RAMP_TIME_US	1260			//!< Ramp signal premodulation time in us
#define PREMOD_RAMP_SAMPLES	PREMOD_RAMP_TIME_US/TIME_DAC_SAMPLE_US	//!< Ramp signal premodulation sample number

#define PREMOD_SAMPLES	126			//!< Total number of samples used in the premodulation signal

#define UPRAMP		1				//!< Upramp flag
#define DOWNRAMP	2				//!< Downramp flag

#define MAX_NUM_MODULATIONS		5
#define DEFAULT_NUM_GUARD_SAMPLES 20

uint16_t sig_prev_points_count;
uint16_t signal_points_count;

/**
 * @brief Modulation struct.
 */
typedef struct modulation_outline {
	uint8_t number;
	uint16_t sig_prev_size;
	uint16_t signal_size;
	uint16_t* sig_prev;				//!< Pointer to the premodulation signal samples of each scheme
	uint16_t* signal;					//!< Pointer to the modulation signal samples of each scheme
	uint16_t num_samples_dac;					//!< Number of dac samples
	uint16_t num_samples_adc;					//!< Number of adc samples
	uint8_t valid;
} modulation_outline_t;


typedef struct modulation_set {
	uint16_t curret_modulation; //number of the current modulation
	modulation_outline_t *modulations[MAX_NUM_MODULATIONS];
} modulation_set_t;

modulation_set_t* set;


modulation_outline_t* new_modulation(uint16_t number, uint16_t sig_prev_size, uint16_t signal_size);
retval_t add_points_prev(uint16_t number, uint16_t* points, uint16_t num_points);
retval_t add_points_signal(uint16_t number, uint16_t* points, uint16_t num_points);
retval_t check_modulation_ok(uint16_t number);
retval_t add_modulation(uint16_t number);
retval_t set_modulation(uint16_t number);
retval_t rmv_modulation(uint16_t number);
modulation_set_t* mod_set_init(void);
retval_t add_initial_ramp(void);
retval_t add_initial_ramp2(void);
retval_t add_initial_ramp3(void);
#endif /* APPLICATION_USER_S4BIM_ADC_CODE_MODULATION_H_ */
