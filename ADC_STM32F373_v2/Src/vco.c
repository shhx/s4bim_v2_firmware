/*
 * vco.c
 *
 *  Created on: 22 jun. 2017
 */

#include "vco.h"

/*
 * voltage in milivolts
 */
retval_t vco_set_avg_voltage(uint8_t channel, uint16_t voltage) {
	uint8_t data = roundf(voltage / 1000.0f * 127 / 12) + 3;
	uint8_t mem = 0x0;
	//HAL_I2C_IsDeviceReady(&hi2c1, ADDRESS_A, 5, 50);
	HAL_I2C_Mem_Write(&hi2c1, ADDRESS_A << 1, mem, 1, &data, 1, 50);
	return RET_OK;
}

retval_t vco_set_gain(uint8_t channel, uint8_t gain) {
	uint8_t data = 127 - 4990 / (gain - 1) * 127 / 10000;
	data = 127;
	uint8_t mem = 0x0;
//	while (HAL_I2C_IsDeviceReady(&hi2c1, ADDRESS_A, 5, 50) != HAL_OK) {
//		;
//	}
	HAL_I2C_Mem_Write(&hi2c1, ADDRESS_G << 1, mem, 1, &data, 1, 50);
	return RET_OK;
}
