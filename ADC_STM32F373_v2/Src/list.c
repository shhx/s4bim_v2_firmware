/*
 * list.c
 *
 *  Created on: 7 jun. 2017
 */

#include "list.h"

retval_t initList(item** head) {
	items_count = 0;
	*head = (item*) pvPortMalloc(sizeof(item));
	if ((*head) == NULL) {
		return RET_ERROR;
	}
	(*head)->command = new_command_usart(NOPP, NULL);
	(*head)->next = NULL;
	return RET_OK;
}

retval_t push_queue(item* head, command_usart_t* cmd) {
	item* newItem;
	item* aux = head;
	if (items_count < MAX_LIST_ITEMS) {// limit the number of items in the list
		while (aux->next != NULL) {
			aux = aux->next;
		}
		newItem = (item *) pvPortMalloc(sizeof(item));
		newItem->command = cmd;
		newItem->next = NULL;
		aux->next = newItem;
		items_count++;
		return RET_OK;
	}
	return RET_ERROR;
}

command_usart_t* pop_queue(item* head) {
	item* aux = head->next;
	item* previous = head;
	command_usart_t* temp;

	if (aux != NULL) { //list empty
		while (aux->next != NULL) {
			previous = aux;
			aux = aux->next;
		}

		temp = aux->command;
		vPortFree(aux);
		previous->next = NULL;
		items_count--;
		return temp;
	}
	return NULL;
}
