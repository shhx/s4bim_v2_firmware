/*
 * filter.h
 *
 *  Created on: 22 jun. 2017
 *      Author: LuisAlberto
 */

#ifndef APPLICATION_S4BIM_ADC_CODE_FILTER_H_
#define APPLICATION_S4BIM_ADC_CODE_FILTER_H_

#include "types.h"
#include "gpio.h"
#include "spi.h"
#include "tim.h"

#define WIPERA		0x0
#define WIPERB 		0x1

typedef enum {
	SCF_FILTER,
	ALT_FILTER,
} filter_t;

retval_t filter_set_freq(uint8_t number, uint32_t freq, filter_t filter_type);
retval_t set_freq_alternative(uint8_t channel, uint32_t freq);
retval_t spi_write_filter_res(uint8_t channel, uint16_t res1, uint16_t res2);


#endif /* APPLICATION_S4BIM_ADC_CODE_FILTER_H_ */
