/**
 * @file commands.h
 */

#ifndef APPLICATION_USER_S4BIM_ADC_CODE_COMMANDS_USART_H_
#define APPLICATION_USER_S4BIM_ADC_CODE_COMMANDS_USART_H_

#include "types.h"
#include "state.h"
#include "modulation.h"
#include "usart.h"
#include "gpio.h"
#include "list.h"
#include "channel.h"

#define UART_TIMEOUT 200

typedef struct channel_config {
	uint8_t		number;
	void* 		config;
} channel_config_t;


typedef struct filter_config {
	uint8_t 	type;					//!< Filter used
	float 		filter_value;			//!< Filter frequency
} filter_config_t;


command_usart_t* new_command_usart(commands_usart command, void* data);
retval_t remove_command_usart(command_usart_t* cmd);

retval_t read_command_from_usart(item* queue);
retval_t write_command_to_usart(command_usart_t* command);

retval_t dispatch_command_queue(state_t* adc_state, item* queue);

#endif /* APPLICATION_USER_S4BIM_ADC_CODE_COMMANDS_USART_H_ */
