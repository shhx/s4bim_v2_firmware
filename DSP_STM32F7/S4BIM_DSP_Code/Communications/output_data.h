/*
 * Copyright (c) 2016, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * output_data.h
 *
 *  Created on: 1/6/2016
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@die.upm.es>
 *
 */
/**
 * @file output_data.h
 */

#ifndef APPLICATION_USER_S4BIM_DSP_CODE_COMMUNICATIONS_OUTPUT_DATA_H_
#define APPLICATION_USER_S4BIM_DSP_CODE_COMMUNICATIONS_OUTPUT_DATA_H_


#include "gpio.h"
#include "preprocessing.h"
#include "postprocessing.h"
#include "time.h"
#include "DSP_conf.h"
#include "usart.h"
#include "main_control.h"

#define SPI_DATA_PACKET_SIZE	144
#define MAX_UART_PACKET_SIZE	256

////////////////SPI DATA PREAMBLE/////////////////////
#define PRE1 0xAA
#define PRE2 0x9F
#define	PRE3 0x55
#define PRE4 0xF9
////////////////////////////////////////////////

typedef enum{
	DISTANCES = 0,
	FREQ = 1,
	TIME = 2,
	POST_PROC_DIST_SIGNAL = 3,
}output_data_mode_t;

typedef enum{
	NONE = 0,
	UART = 1,
	SPI = 2,
	SPI_UART = 3,
	WIFI = 4,
}output_data_interface_t;

typedef enum{
	WIFI_AP_CONNECTED = 0,
	WIFI_AP_DISCONNECTED = 1,
}wifi_ap_state_t;

typedef enum{
	WIFI_OUTPUT_STARTED = 0,
	WIFI_OUTPUT_STOPPED = 1,
}wifi_output_state_t;

typedef struct output_data{
	retval_t (*uart_output_func)(struct output_data* output_data, preprocessing_block_t* preprocessing_block, control_t* main_control, postprocessing_block_t* postprocessing_block);
	retval_t (*spi_output_func)(struct output_data* output_data, preprocessing_block_t* preprocessing_block, control_t* main_control, postprocessing_block_t* postprocessing_block);
	output_data_mode_t output_data_mode;
	output_data_interface_t output_data_interface;
	uint8_t* output_data_dist_packet;
	uint8_t* output_data_freq_packet;
	uint8_t* output_data_time_packet;
	wifi_ap_state_t wifi_ap_state;
	wifi_output_state_t wifi_output_state;
}output_data_t;


output_data_t* new_output_data(output_data_mode_t output_data_mode, output_data_interface_t output_data_interface);
retval_t remove_output_data(output_data_t* output_data);

retval_t change_outputData_interface(output_data_t* output_data, output_data_interface_t new_output_data_interface);

retval_t outputData(output_data_t* output_data, preprocessing_block_t* preprocessing_block, control_t* main_control, postprocessing_block_t* postprocessing_block);

osSemaphoreId geth_comm_semaphore;                         // Semaphore ID

#endif /* APPLICATION_USER_S4BIM_DSP_CODE_COMMUNICATIONS_OUTPUT_DATA_H_ */



