/*
 * Copyright (c) 2016, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * wifi_commands.h
 *
 *  Created on: 22/7/2016
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@die.upm.es>
 *
 */
/**
 * @file wifi_commands.h
 */

#ifndef APPLICATION_USER_S4BIM_DSP_CODE_COMMUNICATIONS_WIFI_COMMANDS_H_
#define APPLICATION_USER_S4BIM_DSP_CODE_COMMUNICATIONS_WIFI_COMMANDS_H_


#include "DSP_conf.h"
#include "output_data.h"
#include "config_commands.h"

/**
 * COMMANDS STRING DEFINITIONS
 */
#define WIFI_AP_CONNECTED_CMD				"AP_CONN"
#define WIFI_AP_DISCONNECTED_CMD			"AP_DISCONN"
#define WIFI_OUT_START_CMD					"WIFI_START"
#define WIFI_OUT_STOP_CMD					"WIFI_STOP"

#define WIFI_SET_TIME						"SET_TIME"
#define WIFI_CALIBRATION					"CALIBRATION"
#define WIFI_CALIBRATION_SONAR				"CALIBRATION_SON"
#define WIFI_POSITION						"SET_POSITION"
#define WIFI_ORIENTATION					"SET_ORIENTATION"


/*****************************************************/


retval_t select_wifi_command(command_data_t* command_data);
retval_t execute_wifi_command(command_data_t* command_data);


#endif /* APPLICATION_USER_S4BIM_DSP_CODE_COMMUNICATIONS_WIFI_COMMANDS_H_ */
