/*
 * Copyright (c) 2016, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * adc_commands.c
 *
 *  Created on: 19/5/2016
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@die.upm.es>
 *
 */
/**
 * @file adc_comm.c
 */

#include "adc_comm.h"

#define DEFAULT_ADC_SEM_WAIT	50

osThreadId currentThread;


command_t* new_command(commands command, uint16_t param_0, uint16_t param_1, uint16_t param_2){

	command_t* new_cmd = (command_t*) pvPortMalloc(sizeof(command_t));
	new_cmd->command = command;
	new_cmd->params[0] = param_0;
	new_cmd->params[1] = param_1;
	new_cmd->params[2] = param_2;
	return new_cmd;
}

retval_t remove_command(command_t* cmd){
	vPortFree(cmd);
	return RET_OK;
}


retval_t read_command_from_spi(command_t* command, state_t* state){


	while(HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_10) != GPIO_PIN_RESET){
		if(osSemaphoreWait (adc_comm_semaphore, DEFAULT_ADC_SEM_WAIT) == osErrorOS){
			break;
		}
	}


	HAL_SPI_TransmitReceive_DMA(&hspi1, (uint8_t*) state, (uint8_t*) command, sizeof(command_t)/2);


	osSemaphoreWait (adc_comm_semaphore, DEFAULT_ADC_SEM_WAIT);		//Wait for the semaphore


	while(HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_10) != GPIO_PIN_SET){	//wait till the pin returns low
		if(osSemaphoreWait (adc_comm_semaphore, DEFAULT_ADC_SEM_WAIT) == osErrorOS){
			break;
		}
	}

	return RET_OK;
}

retval_t write_command_to_spi(command_t* command, state_t* state){

	while(HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_10) != GPIO_PIN_RESET){
		if(osSemaphoreWait (adc_comm_semaphore, DEFAULT_ADC_SEM_WAIT) == osErrorOS){
			break;
		}
	}

	HAL_SPI_TransmitReceive_DMA(&hspi1, (uint8_t*) command, (uint8_t*) state, sizeof(command_t)/2);

	osSemaphoreWait (adc_comm_semaphore, DEFAULT_ADC_SEM_WAIT);		//Wait for the semaphore

	while(HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_10) != GPIO_PIN_SET){	//wait till the pin returns low
		if(osSemaphoreWait (adc_comm_semaphore, DEFAULT_ADC_SEM_WAIT) == osErrorOS){
			break;
		}
	}

	return RET_OK;
}

retval_t receive_iq_signal(adc_iq_signal_t* adc_iq_signal){

	if (adc_iq_signal != NULL){

		uint16_t* null_transmit = (uint16_t*) pvPortMalloc((adc_iq_signal->adc_sample_number+adc_iq_signal->guard_samples)*2*sizeof(uint16_t));

		while(HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_10) != GPIO_PIN_RESET){
			if(osSemaphoreWait (adc_comm_semaphore, DEFAULT_ADC_SEM_WAIT) == osErrorOS){
				break;
			}
		}

		HAL_SPI_TransmitReceive_DMA(&hspi1, (uint8_t*) null_transmit, (uint8_t*) adc_iq_signal->i_signal, (adc_iq_signal->adc_sample_number+adc_iq_signal->guard_samples) *2);

		osSemaphoreWait (adc_comm_semaphore, DEFAULT_ADC_SEM_WAIT);		//Wait for the semaphore

		while(HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_10) != GPIO_PIN_SET){	//wait till the pin returns low
			if(osSemaphoreWait (adc_comm_semaphore, DEFAULT_ADC_SEM_WAIT) == osErrorOS){
				break;
			}
		}

		vPortFree(null_transmit);

		return RET_OK;
	}
	else{
		return RET_ERROR;
	}
}

void adc_comm_spi_callback(void){
	osSemaphoreRelease (adc_comm_semaphore);
}

void adc_comm_pin_callback(void){
	osSemaphoreRelease (adc_comm_semaphore);
}
