/*
 * Copyright (c) 2016, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * config_commands.c
 *
 *  Created on: 7/6/2016
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@die.upm.es>
 *
 */
/**
 * @file config_commands.c
 */


#include "config_commands.h"

extern uint8_t wifi_proc_first;

extern osMutexId  preprocessing_mutex_id;	// Mutex ID
extern osMutexId  uart6_mutex_id;

extern preprocessing_block_t* preprocessing_block;				//It is neccesary to use the mutex each time the preprocessing block config is modified by commands

extern osMutexId  postprocessing_mutex_id;	// Mutex ID

extern postprocessing_block_t* postprocessing_block;

extern output_data_t* output_data;

retval_t null_func(uint16_t argc, char** argv);

retval_t change_adc_samples_func(uint16_t argc, char** argv);
retval_t change_adc_guard_samples_func(uint16_t argc, char** argv);

retval_t change_window_func(uint16_t argc, char** argv);
retval_t change_tukey_alpha_func(uint16_t argc, char** argv);

retval_t change_fft_samples_func(uint16_t argc, char** argv);
retval_t change_fft_effective_samples_func(uint16_t argc, char** argv);

retval_t change_oscfar_window_func(uint16_t argc, char** argv);
retval_t change_oscfar_korder_func(uint16_t argc, char** argv);
retval_t change_oscfar_alpha_func(uint16_t argc, char** argv);

retval_t change_postoscfar_window_func(uint16_t argc, char** argv);
retval_t change_postoscfar_korder_func(uint16_t argc, char** argv);
retval_t change_postoscfar_alpha_func(uint16_t argc, char** argv);

retval_t change_far_freq_sample_snr_func(uint16_t argc, char** argv);
retval_t change_far_freq_value_snr_func(uint16_t argc, char** argv);

retval_t change_max_num_peaks_func(uint16_t argc, char** argv);

retval_t change_max_num_speeds_func(uint16_t argc, char** argv);
retval_t change_speeds_cal_func(uint16_t argc, char** argv);
retval_t change_speeds_guard_func(uint16_t argc, char** argv);
retval_t change_speeds_near_lvl_func(uint16_t argc, char** argv);
retval_t change_speeds_mid_lvl_func(uint16_t argc, char** argv);
retval_t change_speeds_far_lvl_func(uint16_t argc, char** argv);
retval_t change_speeds_vfar_lvl_func(uint16_t argc, char** argv);
retval_t change_speeds_near_snr_func(uint16_t argc, char** argv);
retval_t change_speeds_mid_snr_func(uint16_t argc, char** argv);
retval_t change_speeds_far_snr_func(uint16_t argc, char** argv);
retval_t change_speeds_vfar_snr_func(uint16_t argc, char** argv);
retval_t enable_speed_filter(uint16_t argc, char** argv);
retval_t disable_speed_filter(uint16_t argc, char** argv);

retval_t change_max_num_distances_func(uint16_t argc, char** argv);
retval_t change_distances_cal_func(uint16_t argc, char** argv);
retval_t change_downramp_cal_func(uint16_t argc, char** argv);
retval_t change_distances_guard_func(uint16_t argc, char** argv);
retval_t change_distances_near_lvl_func(uint16_t argc, char** argv);
retval_t change_distances_mid_lvl_func(uint16_t argc, char** argv);
retval_t change_distances_far_lvl_func(uint16_t argc, char** argv);
retval_t change_distances_vfar_lvl_func(uint16_t argc, char** argv);
retval_t change_distances_near_snr_func(uint16_t argc, char** argv);
retval_t change_distances_mid_snr_func(uint16_t argc, char** argv);
retval_t change_distances_far_snr_func(uint16_t argc, char** argv);
retval_t change_distances_vfar_snr_func(uint16_t argc, char** argv);

retval_t change_speed_calc_rate_func(uint16_t argc, char** argv);
retval_t change_updown_rate_func(uint16_t argc, char** argv);
retval_t change_delay_func(uint16_t argc, char** argv);

retval_t change_output_mode_func(uint16_t argc, char** argv);
retval_t change_output_interface_func(uint16_t argc, char** argv);

retval_t change_dist_cluster_size_func(uint16_t argc, char** argv);
retval_t change_dist_max_dist_func(uint16_t argc, char** argv);
retval_t change_dist_round_step_func(uint16_t argc, char** argv);
retval_t change_post_proc_samples_func(uint16_t argc, char** argv);

retval_t change_post_proc_threeshold_func(uint16_t argc, char** argv);
retval_t change_final_dist_min_func(uint16_t argc, char** argv);
retval_t change_final_dist_res_func(uint16_t argc, char** argv);


retval_t set_wifi_ssid_pass(uint16_t argc, char** argv);

command_data_t* new_command_data(){

	command_data_t* new_command_data = (command_data_t*) pvPortMalloc(sizeof(command_data_t));
	new_command_data->argc = 0;
	new_command_data->command_func = null_func;

	return new_command_data;
}
retval_t remove_command_data(command_data_t* command_data){

	if(command_data != NULL){
		vPortFree(command_data);
		return RET_OK;
	}
	else{
		return RET_ERROR;
	}

}

retval_t select_command(command_data_t* command_data){
	if(command_data != NULL){
		if(command_data->argc != 0){
			/////////////////////////////////////////////////////////////////////
			if(strcmp(command_data->argv[0],CHANGE_ADC_SAMPLES) == 0){
				command_data->command_func = change_adc_samples_func;
			}
			else if(strcmp(command_data->argv[0],CHANGE_ADC_GUARD_SAMPLES) == 0){
				command_data->command_func = change_adc_guard_samples_func;
			}
			/////////////////////////////////////////////////////////////////////
			else if(strcmp(command_data->argv[0], CHANGE_WINDOW_STR) == 0){
				command_data->command_func = change_window_func;
			}
			else if(strcmp(command_data->argv[0],CHANGE_TUKEY_ALPHA) == 0){
				command_data->command_func = change_tukey_alpha_func;
			}
			/////////////////////////////////////////////////////////////////////
			else if(strcmp(command_data->argv[0],CHANGE_FFT_SAMPLES) == 0){
				command_data->command_func = change_fft_samples_func;
			}
			else if(strcmp(command_data->argv[0],CHANGE_FFT_EFFECTIVE_SAMPLES) == 0){
				command_data->command_func = change_fft_effective_samples_func;
			}
			/////////////////////////////////////////////////////////////////////
			else if(strcmp(command_data->argv[0],CHANGE_OSCFAR_WINDOW) == 0){
				command_data->command_func = change_oscfar_window_func;
			}
			else if(strcmp(command_data->argv[0],CHANGE_OSCFAR_KORDER) == 0){
				command_data->command_func = change_oscfar_korder_func;
			}
			else if(strcmp(command_data->argv[0],CHANGE_OSCFAR_ALPHA) == 0){
				command_data->command_func = change_oscfar_alpha_func;
			}
			/////////////////////////////////////////////////////////////////////
			else if(strcmp(command_data->argv[0],CHANGE_POSTOSCFAR_WINDOW) == 0){
				command_data->command_func = change_postoscfar_window_func;
			}
			else if(strcmp(command_data->argv[0],CHANGE_POSTOSCFAR_KORDER) == 0){
				command_data->command_func = change_postoscfar_korder_func;
			}
			else if(strcmp(command_data->argv[0],CHANGE_POSTOSCFAR_ALPHA) == 0){
				command_data->command_func = change_postoscfar_alpha_func;
			}
			/////////////////////////////////////////////////////////////////////
			else if(strcmp(command_data->argv[0],CHANGE_FAR_FREQ_SAMPLE_SNR) == 0){
				command_data->command_func = change_far_freq_sample_snr_func;
			}
			else if(strcmp(command_data->argv[0],CHANGE_FAR_FREQ_VALUE_SNR) == 0){
				command_data->command_func = change_far_freq_value_snr_func;
			}
			/////////////////////////////////////////////////////////////////////
			else if(strcmp(command_data->argv[0],CHANGE_MAX_NUM_PEAKS) == 0){
				command_data->command_func = change_max_num_peaks_func;
			}
			/////////////////////////////////////////////////////////////////////
			else if(strcmp(command_data->argv[0],CHANGE_MAX_NUM_SPEEDS) == 0){
				command_data->command_func = change_max_num_speeds_func;
			}
			else if(strcmp(command_data->argv[0],CHANGE_SPEEDS_CALIBRATION) == 0){
				command_data->command_func = change_speeds_cal_func;
			}
			else if(strcmp(command_data->argv[0],CHANGE_SPEEDS_GUARD) == 0){
				command_data->command_func = change_speeds_guard_func;
			}
			else if(strcmp(command_data->argv[0],CHANGE_SPEEDS_NEAR_LVL_THR) == 0){
				command_data->command_func = change_speeds_near_lvl_func;
			}
			else if(strcmp(command_data->argv[0],CHANGE_SPEEDS_MID_LVL_THR) == 0){
				command_data->command_func = change_speeds_mid_lvl_func;
			}
			else if(strcmp(command_data->argv[0],CHANGE_SPEEDS_FAR_LVL_THR) == 0){
				command_data->command_func = change_speeds_far_lvl_func;
			}
			else if(strcmp(command_data->argv[0],CHANGE_SPEEDS_VFAR_LVL_THR) == 0){
				command_data->command_func = change_speeds_vfar_lvl_func;
			}
			else if(strcmp(command_data->argv[0],CHANGE_SPEEDS_NEAR_SNR_THR) == 0){
				command_data->command_func = change_speeds_near_snr_func;
			}
			else if(strcmp(command_data->argv[0],CHANGE_SPEEDS_MID_SNR_THR) == 0){
				command_data->command_func = change_speeds_mid_snr_func;
			}
			else if(strcmp(command_data->argv[0],CHANGE_SPEEDS_FAR_SNR_THR) == 0){
				command_data->command_func = change_speeds_far_snr_func;
			}
			else if(strcmp(command_data->argv[0],CHANGE_SPEEDS_VFAR_SNR_THR) == 0){
				command_data->command_func = change_speeds_vfar_snr_func;
			}
			/////////////////////////////////////////////////////////////////////
			else if(strcmp(command_data->argv[0],CHANGE_MAX_NUM_DISTANCES) == 0){
				command_data->command_func = change_max_num_distances_func;
			}
			else if(strcmp(command_data->argv[0],CHANGE_DISTANCE_CALIBRATION) == 0){
				command_data->command_func = change_distances_cal_func;
			}
			else if(strcmp(command_data->argv[0],CHANGE_DOWNRAMP_CALIBRATION) == 0){
				command_data->command_func = change_downramp_cal_func;
			}
			else if(strcmp(command_data->argv[0],CHANGE_DISTANCE_GUARD) == 0){
				command_data->command_func = change_distances_guard_func;
			}
			else if(strcmp(command_data->argv[0],CHANGE_DISTANCE_NEAR_LVL_THR) == 0){
				command_data->command_func = change_distances_near_lvl_func;
			}
			else if(strcmp(command_data->argv[0],CHANGE_DISTANCE_MID_LVL_THR) == 0){
				command_data->command_func = change_distances_mid_lvl_func;
			}
			else if(strcmp(command_data->argv[0],CHANGE_DISTANCE_FAR_LVL_THR) == 0){
				command_data->command_func = change_distances_far_lvl_func;
			}
			else if(strcmp(command_data->argv[0],CHANGE_DISTANCE_VFAR_LVL_THR) == 0){
				command_data->command_func = change_distances_vfar_lvl_func;
			}
			else if(strcmp(command_data->argv[0],CHANGE_DISTANCE_NEAR_SNR_THR) == 0){
				command_data->command_func = change_distances_near_snr_func;
			}
			else if(strcmp(command_data->argv[0],CHANGE_DISTANCE_MID_SNR_THR) == 0){
				command_data->command_func = change_distances_mid_snr_func;
			}
			else if(strcmp(command_data->argv[0],CHANGE_DISTANCE_FAR_SNR_THR) == 0){
				command_data->command_func = change_distances_far_snr_func;
			}
			else if(strcmp(command_data->argv[0],CHANGE_DISTANCE_VFAR_SNR_THR) == 0){
				command_data->command_func = change_distances_vfar_snr_func;
			}
			else if(strcmp(command_data->argv[0],ENABLE_SPEED_FILTER) == 0){
				command_data->command_func = enable_speed_filter;
			}
			else if(strcmp(command_data->argv[0],DISABLE_SPEED_FILTER) == 0){
				command_data->command_func = disable_speed_filter;
			}
			/////////////////////////////////////////////////////////////////////
			else if(strcmp(command_data->argv[0],CHANGE_SPEED_CALCULATION_RATE) == 0){
				command_data->command_func = change_speed_calc_rate_func;
			}
			else if(strcmp(command_data->argv[0],CHANGE_UPDOWN_RATE) == 0){
				command_data->command_func = change_updown_rate_func;
			}
			else if(strcmp(command_data->argv[0],CHANGE_DELAY) == 0){
				command_data->command_func = change_delay_func;
			}
			/////////////////////////////////////////////////////////////////////
			else if(strcmp(command_data->argv[0],CHANGE_OUTPUT_MODE) == 0){
				command_data->command_func = change_output_mode_func;
			}
			else if(strcmp(command_data->argv[0],CHANGE_OUTPUT_INTERFACE) == 0){
				command_data->command_func = change_output_interface_func;
			}
			/////////////////////////////////////////////////////////////////////
			else if(strcmp(command_data->argv[0],CHANGE_DIST_PROC_CLUSTER_SIZE) == 0){
				command_data->command_func = change_dist_cluster_size_func;
			}
			else if(strcmp(command_data->argv[0],CHANGE_DIST_PROC_MAX_DIST) == 0){
				command_data->command_func = change_dist_max_dist_func;
			}
			else if(strcmp(command_data->argv[0],CHANGE_DIST_ROUND_STEP) == 0){
				command_data->command_func = change_dist_round_step_func;
			}
			else if(strcmp(command_data->argv[0],CHANGE_POST_PROC_SAMPLES) == 0){
				command_data->command_func = change_post_proc_samples_func;
			}
			/////////////////////////////////////////////////////////////////////
			else if(strcmp(command_data->argv[0],CHANGE_POST_PROC_THREESHOLD) == 0){
				command_data->command_func = change_post_proc_threeshold_func;
			}
			else if(strcmp(command_data->argv[0],CHANGE_FINAL_MIN_DIST) == 0){
				command_data->command_func = change_final_dist_min_func;
			}
			else if(strcmp(command_data->argv[0],CHANGE_FINAL_DIST_RES) == 0){
				command_data->command_func = change_final_dist_res_func;
			}
			else if(strcmp(command_data->argv[0],SET_WIFI_SSID_PASS) == 0){
				command_data->command_func = set_wifi_ssid_pass;
			}
			else{
				command_data->command_func = null_func;
				return RET_ERROR;
			}
			return RET_OK;
		}
		else{
			return RET_OK;
		}

		return RET_OK;
	}
	else{
		return RET_ERROR;
	}
}

retval_t execute_command(command_data_t* command_data){
	if(command_data != NULL){
		uint16_t i = 0;

		command_data->command_func(command_data->argc, command_data->argv);

		command_data->command_func = null_func;

		for (i=0; i<command_data->argc; i++){			//When a command is executed, the memory occupied by the args must be freed
			if(command_data->argv[i] != NULL){
				vPortFree(command_data->argv[i]);
				command_data->argv[i] = NULL;
			}
		}
		if(command_data->argv != NULL){
			vPortFree(command_data->argv);
			command_data->argv = NULL;
		}

		command_data->argc = 0;

		return RET_OK;

	}
	else{
		return RET_ERROR;
	}
}


retval_t null_func(uint16_t argc, char** argv){
	return RET_OK;
}




retval_t change_adc_samples_func(uint16_t argc, char** argv){
	if(preprocessing_block != NULL){
		if(argc==2){
			uint16_t new_adc_sample_number = (uint16_t) atoi(argv[1]);
			osMutexWait (preprocessing_mutex_id, osWaitForever);
			prep_change_adc_sample_number(preprocessing_block, new_adc_sample_number);
			osMutexRelease (preprocessing_mutex_id);
			return RET_OK;
		}
		else{
			return RET_ERROR;
		}
	}
	else{
		return RET_ERROR;
	}
}

retval_t change_adc_guard_samples_func(uint16_t argc, char** argv){
	if(preprocessing_block != NULL){
		if(argc==2){
			uint16_t new_guard_sample_number = (uint16_t) atoi(argv[1]);
			osMutexWait (preprocessing_mutex_id, osWaitForever);
			change_adc_guard_sample(preprocessing_block->adc_iq_signal, new_guard_sample_number);
			osMutexRelease (preprocessing_mutex_id);
			return RET_OK;
		}
		else{
			return RET_ERROR;
		}
	}
	else{
		return RET_ERROR;
	}
}

retval_t change_window_func(uint16_t argc, char** argv){

	if(preprocessing_block != NULL){

		if(strcmp(argv[1], WINDOW_HANNING_STR) == 0){
			osMutexWait (preprocessing_mutex_id, osWaitForever);
			change_window_type(preprocessing_block->window, Hanning);
			osMutexRelease (preprocessing_mutex_id);

		}
		else if (strcmp(argv[1], WINDOW_HAMMING_STR) == 0){
			osMutexWait (preprocessing_mutex_id, osWaitForever);
			change_window_type(preprocessing_block->window, Hamming);
			osMutexRelease (preprocessing_mutex_id);

		}
		else if(strcmp(argv[1], WINDOW_TUKEY_STR) == 0){
			osMutexWait (preprocessing_mutex_id, osWaitForever);
			change_window_type(preprocessing_block->window, Tukey);
			osMutexRelease (preprocessing_mutex_id);

		}
		else if(strcmp(argv[1], NOWINDOW_STR) == 0){
			osMutexWait (preprocessing_mutex_id, osWaitForever);
			change_window_type(preprocessing_block->window, No_Window);
			osMutexRelease (preprocessing_mutex_id);
		}
		else{
			return RET_ERROR;
		}
		return RET_OK;
	}
	else{
		return RET_ERROR;
	}
	return RET_OK;
}

retval_t change_tukey_alpha_func(uint16_t argc, char** argv){
	if(preprocessing_block != NULL){
		if(argc==2){
			float32_t new_alpha = (float32_t) atof(argv[1]);
			osMutexWait (preprocessing_mutex_id, osWaitForever);
			preprocessing_block->window->tukey_alpha = new_alpha;
			osMutexRelease (preprocessing_mutex_id);
			return RET_OK;
		}
		else{
			return RET_ERROR;
		}
	}
	else{
		return RET_ERROR;
	}
}

retval_t change_fft_samples_func(uint16_t argc, char** argv){
	if(preprocessing_block != NULL){
		if(argc==2){
			uint16_t new_fft_sample_number = (uint16_t) atoi(argv[1]);
			osMutexWait (preprocessing_mutex_id, osWaitForever);
			change_freq_sample_number(preprocessing_block, new_fft_sample_number);
			osMutexRelease (preprocessing_mutex_id);
			return RET_OK;
		}
		else{
			return RET_ERROR;
		}
	}
	else{
		return RET_ERROR;
	}
}
retval_t change_fft_effective_samples_func(uint16_t argc, char** argv){
	if(preprocessing_block != NULL){
		if(argc==2){
			uint16_t new_valid_sample_number = (uint16_t) atoi(argv[1]);
			osMutexWait (preprocessing_mutex_id, osWaitForever);
			change_freq_valid_samples(preprocessing_block, new_valid_sample_number);
			osMutexRelease (preprocessing_mutex_id);
			return RET_OK;
		}
		else{
			return RET_ERROR;
		}
	}
	else{
		return RET_ERROR;
	}
}

retval_t change_oscfar_window_func(uint16_t argc, char** argv){
	if(preprocessing_block != NULL){
		if(argc==2){
			uint16_t new_window_size = (uint16_t) atoi(argv[1]);
			osMutexWait (preprocessing_mutex_id, osWaitForever);
			preprocessing_block->os_cfar->window_size = new_window_size;
			osMutexRelease (preprocessing_mutex_id);
			return RET_OK;
		}
		else{
			return RET_ERROR;
		}
	}
	else{
		return RET_ERROR;
	}
}
retval_t change_oscfar_korder_func(uint16_t argc, char** argv){
	if(preprocessing_block != NULL){
		if(argc==2){
			uint16_t new_k_order = (uint16_t) atoi(argv[1]);
			osMutexWait (preprocessing_mutex_id, osWaitForever);
			preprocessing_block->os_cfar->k_order = new_k_order;
			osMutexRelease (preprocessing_mutex_id);
			return RET_OK;
		}
		else{
			return RET_ERROR;
		}
	}
	else{
		return RET_ERROR;
	}
}
retval_t change_oscfar_alpha_func(uint16_t argc, char** argv){
	if(preprocessing_block != NULL){
		if(argc==2){
			float32_t new_alpha = (float32_t) atof(argv[1]);
			osMutexWait (preprocessing_mutex_id, osWaitForever);
			preprocessing_block->os_cfar->alpha = new_alpha;
			osMutexRelease (preprocessing_mutex_id);
			return RET_OK;
		}
		else{
			return RET_ERROR;
		}
	}
	else{
		return RET_ERROR;
	}
}

retval_t change_postoscfar_window_func(uint16_t argc, char** argv){
	if(postprocessing_block != NULL){
		if(argc==2){
			uint16_t new_window_size = (uint16_t) atoi(argv[1]);
			osMutexWait (postprocessing_mutex_id, osWaitForever);
			postprocessing_block->post_os_cfar->window_size = new_window_size;
			osMutexRelease (postprocessing_mutex_id);
			return RET_OK;
		}
		else{
			return RET_ERROR;
		}
	}
	else{
		return RET_ERROR;
	}
}
retval_t change_postoscfar_korder_func(uint16_t argc, char** argv){
	if(postprocessing_block != NULL){
		if(argc==2){
			uint16_t new_k_order = (uint16_t) atoi(argv[1]);
			osMutexWait (postprocessing_mutex_id, osWaitForever);
			postprocessing_block->post_os_cfar->k_order = new_k_order;
			osMutexRelease (postprocessing_mutex_id);
			return RET_OK;
		}
		else{
			return RET_ERROR;
		}
	}
	else{
		return RET_ERROR;
	}
}
retval_t change_postoscfar_alpha_func(uint16_t argc, char** argv){
	if(postprocessing_block != NULL){
		if(argc==2){
			float32_t new_alpha = (float32_t) atof(argv[1]);
			osMutexWait (postprocessing_mutex_id, osWaitForever);
			postprocessing_block->post_os_cfar->alpha = new_alpha;
			osMutexRelease (postprocessing_mutex_id);
			return RET_OK;
		}
		else{
			return RET_ERROR;
		}
	}
	else{
		return RET_ERROR;
	}
}

retval_t change_far_freq_sample_snr_func(uint16_t argc, char** argv){
	if(preprocessing_block != NULL){
		if(argc==2){
			uint16_t new_far_sample = (uint16_t) atoi(argv[1]);
			osMutexWait (preprocessing_mutex_id, osWaitForever);
			preprocessing_block->snr->far_freq_sample = new_far_sample;
			osMutexRelease (preprocessing_mutex_id);
			return RET_OK;
		}
		else{
			return RET_ERROR;
		}
	}
	else{
		return RET_ERROR;
	}
}
retval_t change_far_freq_value_snr_func(uint16_t argc, char** argv){
	if(preprocessing_block != NULL){
		if(argc==2){
			float32_t new_far_value = (float32_t) atof(argv[1]);
			osMutexWait (preprocessing_mutex_id, osWaitForever);
			preprocessing_block->snr->far_freq_constant_value = new_far_value;
			osMutexRelease (preprocessing_mutex_id);
			return RET_OK;
		}
		else{
			return RET_ERROR;
		}
	}
	else{
		return RET_ERROR;
	}
}

retval_t change_max_num_peaks_func(uint16_t argc, char** argv){
	if(preprocessing_block != NULL){
		if(argc==2){
			uint16_t new_max_num_peaks = (uint16_t) atoi(argv[1]);
			osMutexWait (preprocessing_mutex_id, osWaitForever);
			set_max_num_peaks(preprocessing_block->peaks_selection, new_max_num_peaks);
			osMutexRelease (preprocessing_mutex_id);
			return RET_OK;
		}
		else{
			return RET_ERROR;
		}
	}
	else{
		return RET_ERROR;
	}
}

retval_t change_max_num_speeds_func(uint16_t argc, char** argv){
	if(preprocessing_block != NULL){
		if(argc==2){
			uint16_t new_max_num_speeds = (uint16_t) atoi(argv[1]);
			osMutexWait (preprocessing_mutex_id, osWaitForever);
			set_max_num_speeds(preprocessing_block->speed_selection, new_max_num_speeds);
			osMutexRelease (preprocessing_mutex_id);
			return RET_OK;
		}
		else{
			return RET_ERROR;
		}
	}
	else{
		return RET_ERROR;
	}
}
retval_t change_speeds_cal_func(uint16_t argc, char** argv){
	if(preprocessing_block != NULL){
		if(argc==2){
			float32_t new_calibration = (float32_t) atof(argv[1]);
			osMutexWait (preprocessing_mutex_id, osWaitForever);
			preprocessing_block->speed_selection->calibration_factor = new_calibration;
			osMutexRelease (preprocessing_mutex_id);
			return RET_OK;
		}
		else{
			return RET_ERROR;
		}
	}
	else{
		return RET_ERROR;
	}
}
retval_t change_speeds_guard_func(uint16_t argc, char** argv){
	if(preprocessing_block != NULL){
		if(argc==2){
			uint16_t new_speeds_guard = (uint16_t) atoi(argv[1]);
			osMutexWait (preprocessing_mutex_id, osWaitForever);
			preprocessing_block->speed_selection->num_guard_low_speeds = new_speeds_guard;
			osMutexRelease (preprocessing_mutex_id);
			return RET_OK;
		}
		else{
			return RET_ERROR;
		}
	}
	else{
		return RET_ERROR;
	}
}
retval_t change_speeds_near_lvl_func(uint16_t argc, char** argv){
	if(preprocessing_block != NULL){
		if(argc==2){
			float32_t new_lvl = (float32_t) atof(argv[1]);
			osMutexWait (preprocessing_mutex_id, osWaitForever);
			preprocessing_block->speed_selection->threshold_lvl_calibration_near = new_lvl;
			osMutexRelease (preprocessing_mutex_id);
			return RET_OK;
		}
		else{
			return RET_ERROR;
		}
	}
	else{
		return RET_ERROR;
	}
}
retval_t change_speeds_mid_lvl_func(uint16_t argc, char** argv){
	if(preprocessing_block != NULL){
		if(argc==2){
			float32_t new_lvl = (float32_t) atof(argv[1]);
			osMutexWait (preprocessing_mutex_id, osWaitForever);
			preprocessing_block->speed_selection->threshold_lvl_calibration_mid = new_lvl;
			osMutexRelease (preprocessing_mutex_id);
			return RET_OK;
		}
		else{
			return RET_ERROR;
		}
	}
	else{
		return RET_ERROR;
	}
}
retval_t change_speeds_far_lvl_func(uint16_t argc, char** argv){
	if(preprocessing_block != NULL){
		if(argc==2){
			float32_t new_lvl = (float32_t) atof(argv[1]);
			osMutexWait (preprocessing_mutex_id, osWaitForever);
			preprocessing_block->speed_selection->threshold_lvl_calibration_far = new_lvl;
			osMutexRelease (preprocessing_mutex_id);
			return RET_OK;
		}
		else{
			return RET_ERROR;
		}
	}
	else{
		return RET_ERROR;
	}
}
retval_t change_speeds_vfar_lvl_func(uint16_t argc, char** argv){
	if(preprocessing_block != NULL){
		if(argc==2){
			float32_t new_lvl = (float32_t) atof(argv[1]);
			osMutexWait (preprocessing_mutex_id, osWaitForever);
			preprocessing_block->speed_selection->threshold_lvl_calibration_vfar = new_lvl;
			osMutexRelease (preprocessing_mutex_id);
			return RET_OK;
		}
		else{
			return RET_ERROR;
		}
	}
	else{
		return RET_ERROR;
	}
}
retval_t change_speeds_near_snr_func(uint16_t argc, char** argv){
	if(preprocessing_block != NULL){
		if(argc==2){
			float32_t new_snr = (float32_t) atof(argv[1]);
			osMutexWait (preprocessing_mutex_id, osWaitForever);
			preprocessing_block->speed_selection->threshold_snr_calibration_near = new_snr;
			osMutexRelease (preprocessing_mutex_id);
			return RET_OK;
		}
		else{
			return RET_ERROR;
		}
	}
	else{
		return RET_ERROR;
	}
}
retval_t change_speeds_mid_snr_func(uint16_t argc, char** argv){
	if(preprocessing_block != NULL){
		if(argc==2){
			float32_t new_snr = (float32_t) atof(argv[1]);
			osMutexWait (preprocessing_mutex_id, osWaitForever);
			preprocessing_block->speed_selection->threshold_snr_calibration_mid = new_snr;
			osMutexRelease (preprocessing_mutex_id);
			return RET_OK;
		}
		else{
			return RET_ERROR;
		}
	}
	else{
		return RET_ERROR;
	}
}
retval_t change_speeds_far_snr_func(uint16_t argc, char** argv){
	if(preprocessing_block != NULL){
		if(argc==2){
			float32_t new_snr = (float32_t) atof(argv[1]);
			osMutexWait (preprocessing_mutex_id, osWaitForever);
			preprocessing_block->speed_selection->threshold_snr_calibration_far = new_snr;
			osMutexRelease (preprocessing_mutex_id);
			return RET_OK;
		}
		else{
			return RET_ERROR;
		}
	}
	else{
		return RET_ERROR;
	}
}
retval_t change_speeds_vfar_snr_func(uint16_t argc, char** argv){
	if(preprocessing_block != NULL){
		if(argc==2){
			float32_t new_snr = (float32_t) atof(argv[1]);
			osMutexWait (preprocessing_mutex_id, osWaitForever);
			preprocessing_block->speed_selection->threshold_snr_calibration_vfar = new_snr;
			osMutexRelease (preprocessing_mutex_id);
			return RET_OK;
		}
		else{
			return RET_ERROR;
		}
	}
	else{
		return RET_ERROR;
	}
}

retval_t change_max_num_distances_func(uint16_t argc, char** argv){
	if(preprocessing_block != NULL){
		if(argc==2){
			uint16_t new_max_num_distances = (uint16_t) atoi(argv[1]);
			osMutexWait (preprocessing_mutex_id, osWaitForever);
			set_max_num_distances(preprocessing_block->distances_selection, new_max_num_distances);
			osMutexRelease (preprocessing_mutex_id);
			return RET_OK;
		}
		else{
			return RET_ERROR;
		}
	}
	else{
		return RET_ERROR;
	}
}
retval_t change_distances_cal_func(uint16_t argc, char** argv){
	if(preprocessing_block != NULL){
		if(argc==2){
			float32_t new_calibration = (float32_t) atof(argv[1]);
			osMutexWait (preprocessing_mutex_id, osWaitForever);
			preprocessing_block->distances_selection->calibration_factor = new_calibration;
			osMutexRelease (preprocessing_mutex_id);
			return RET_OK;
		}
		else{
			return RET_ERROR;
		}
	}
	else{
		return RET_ERROR;
	}
}
retval_t change_downramp_cal_func(uint16_t argc, char** argv){
	if(preprocessing_block != NULL){
		if(argc==2){
			float32_t new_calibration = (float32_t) atof(argv[1]);
			osMutexWait (preprocessing_mutex_id, osWaitForever);
			preprocessing_block->distances_selection->downramp_calibration_factor = new_calibration;
			osMutexRelease (preprocessing_mutex_id);
			return RET_OK;
		}
		else{
			return RET_ERROR;
		}
	}
	else{
		return RET_ERROR;
	}
}
retval_t change_distances_guard_func(uint16_t argc, char** argv){
	if(preprocessing_block != NULL){
		if(argc==2){
			uint16_t new_speeds_guard = (uint16_t) atoi(argv[1]);
			osMutexWait (preprocessing_mutex_id, osWaitForever);
			preprocessing_block->distances_selection->num_guard_low_distances = new_speeds_guard;
			osMutexRelease (preprocessing_mutex_id);
			return RET_OK;
		}
		else{
			return RET_ERROR;
		}
	}
	else{
		return RET_ERROR;
	}
}
retval_t change_distances_near_lvl_func(uint16_t argc, char** argv){
	if(preprocessing_block != NULL){
		if(argc==2){
			float32_t new_lvl = (float32_t) atof(argv[1]);
			osMutexWait (preprocessing_mutex_id, osWaitForever);
			preprocessing_block->distances_selection->threshold_lvl_calibration_near = new_lvl;
			osMutexRelease (preprocessing_mutex_id);
			return RET_OK;
		}
		else{
			return RET_ERROR;
		}
	}
	else{
		return RET_ERROR;
	}
}
retval_t change_distances_mid_lvl_func(uint16_t argc, char** argv){
	if(preprocessing_block != NULL){
		if(argc==2){
			float32_t new_lvl = (float32_t) atof(argv[1]);
			osMutexWait (preprocessing_mutex_id, osWaitForever);
			preprocessing_block->distances_selection->threshold_lvl_calibration_mid = new_lvl;
			osMutexRelease (preprocessing_mutex_id);
			return RET_OK;
		}
		else{
			return RET_ERROR;
		}
	}
	else{
		return RET_ERROR;
	}
}
retval_t change_distances_far_lvl_func(uint16_t argc, char** argv){
	if(preprocessing_block != NULL){
		if(argc==2){
			float32_t new_lvl = (float32_t) atof(argv[1]);
			osMutexWait (preprocessing_mutex_id, osWaitForever);
			preprocessing_block->distances_selection->threshold_lvl_calibration_far = new_lvl;
			osMutexRelease (preprocessing_mutex_id);
			return RET_OK;
		}
		else{
			return RET_ERROR;
		}
	}
	else{
		return RET_ERROR;
	}
}
retval_t change_distances_vfar_lvl_func(uint16_t argc, char** argv){
	if(preprocessing_block != NULL){
		if(argc==2){
			float32_t new_lvl = (float32_t) atof(argv[1]);
			osMutexWait (preprocessing_mutex_id, osWaitForever);
			preprocessing_block->distances_selection->threshold_lvl_calibration_vfar = new_lvl;
			osMutexRelease (preprocessing_mutex_id);
			return RET_OK;
		}
		else{
			return RET_ERROR;
		}
	}
	else{
		return RET_ERROR;
	}
}
retval_t change_distances_near_snr_func(uint16_t argc, char** argv){
	if(preprocessing_block != NULL){
		if(argc==2){
			float32_t new_snr = (float32_t) atof(argv[1]);
			osMutexWait (preprocessing_mutex_id, osWaitForever);
			preprocessing_block->distances_selection->threshold_snr_calibration_near = new_snr;
			osMutexRelease (preprocessing_mutex_id);
			return RET_OK;
		}
		else{
			return RET_ERROR;
		}
	}
	else{
		return RET_ERROR;
	}
}
retval_t change_distances_mid_snr_func(uint16_t argc, char** argv){
	if(preprocessing_block != NULL){
		if(argc==2){
			float32_t new_snr = (float32_t) atof(argv[1]);
			osMutexWait (preprocessing_mutex_id, osWaitForever);
			preprocessing_block->distances_selection->threshold_snr_calibration_mid = new_snr;
			osMutexRelease (preprocessing_mutex_id);
			return RET_OK;
		}
		else{
			return RET_ERROR;
		}
	}
	else{
		return RET_ERROR;
	}
}
retval_t change_distances_far_snr_func(uint16_t argc, char** argv){
	if(preprocessing_block != NULL){
		if(argc==2){
			float32_t new_snr = (float32_t) atof(argv[1]);
			osMutexWait (preprocessing_mutex_id, osWaitForever);
			preprocessing_block->distances_selection->threshold_snr_calibration_far = new_snr;
			osMutexRelease (preprocessing_mutex_id);
			return RET_OK;
		}
		else{
			return RET_ERROR;
		}
	}
	else{
		return RET_ERROR;
	}
}
retval_t change_distances_vfar_snr_func(uint16_t argc, char** argv){
	if(preprocessing_block != NULL){
		if(argc==2){
			float32_t new_snr = (float32_t) atof(argv[1]);
			osMutexWait (preprocessing_mutex_id, osWaitForever);
			preprocessing_block->distances_selection->threshold_snr_calibration_vfar = new_snr;
			osMutexRelease (preprocessing_mutex_id);
			return RET_OK;
		}
		else{
			return RET_ERROR;
		}
	}
	else{
		return RET_ERROR;
	}
}
retval_t enable_speed_filter(uint16_t argc, char** argv){
	if(preprocessing_block != NULL){
		osMutexWait (preprocessing_mutex_id, osWaitForever);
		preprocessing_block->distances_selection->discard_speed = DISCARD_SPEED;
		osMutexRelease (preprocessing_mutex_id);
		return RET_OK;
	}
	else{
		return RET_ERROR;
	}
}
retval_t disable_speed_filter(uint16_t argc, char** argv){
	if(preprocessing_block != NULL){
		osMutexWait (preprocessing_mutex_id, osWaitForever);
		preprocessing_block->distances_selection->discard_speed = NO_DISCARD_SPEED;
		osMutexRelease (preprocessing_mutex_id);
		return RET_OK;
	}
	else{
		return RET_ERROR;
	}
}


retval_t change_speed_calc_rate_func(uint16_t argc, char** argv){
	if(preprocessing_block != NULL){
		if(argc==2){
			uint16_t new_speed_calc_rate = (uint16_t) atoi(argv[1]);
			osMutexWait (preprocessing_mutex_id, osWaitForever);
			preprocessing_block->speed_calc_rate = new_speed_calc_rate;
			osMutexRelease (preprocessing_mutex_id);
			return RET_OK;
		}
		else{
			return RET_ERROR;
		}
	}
	else{
		return RET_ERROR;
	}
}
retval_t change_updown_rate_func(uint16_t argc, char** argv){
	if(preprocessing_block != NULL){
		if(argc==2){
			uint16_t new_updown_rate = (uint16_t) atoi(argv[1]);
			osMutexWait (preprocessing_mutex_id, osWaitForever);
			preprocessing_block->updown_calc_rate = new_updown_rate;
			osMutexRelease (preprocessing_mutex_id);
			return RET_OK;
		}
		else{
			return RET_ERROR;
		}
	}
	else{
		return RET_ERROR;
	}
}

retval_t change_delay_func(uint16_t argc, char** argv){
	if(preprocessing_block != NULL){
		if(argc==2){
			uint32_t new_delay = (uint32_t) atoi(argv[1]);
			osMutexWait (preprocessing_mutex_id, osWaitForever);
			preprocessing_block->delay = new_delay;
			osMutexRelease (preprocessing_mutex_id);
			return RET_OK;
		}
		else{
			return RET_ERROR;
		}
	}
	else{
		return RET_ERROR;
	}
}

retval_t change_output_mode_func(uint16_t argc, char** argv){

	if(preprocessing_block != NULL){

		if(strcmp(argv[1], OUTPUT_MODE_DISTANCES) == 0){
			osMutexWait (preprocessing_mutex_id, osWaitForever);
			output_data->output_data_mode = DISTANCES;
			osMutexRelease (preprocessing_mutex_id);

		}
		else if (strcmp(argv[1], OUTPUT_MODE_FREQ) == 0){
			osMutexWait (preprocessing_mutex_id, osWaitForever);
			output_data->output_data_mode = FREQ;
			osMutexRelease (preprocessing_mutex_id);

		}
		else if(strcmp(argv[1], OUTPUT_MODE_TIME) == 0){
			osMutexWait (preprocessing_mutex_id, osWaitForever);
			output_data->output_data_mode = TIME;
			osMutexRelease (preprocessing_mutex_id);
		}
		else if(strcmp(argv[1], OUTPUT_MODE_DIST_SIGNAL) == 0){
			osMutexWait (preprocessing_mutex_id, osWaitForever);
			output_data->output_data_mode = POST_PROC_DIST_SIGNAL;
			osMutexRelease (preprocessing_mutex_id);
		}
		else{
			return RET_ERROR;
		}
		return RET_OK;
	}
	else{
		return RET_ERROR;
	}
	return RET_OK;
}
retval_t change_output_interface_func(uint16_t argc, char** argv){

	if(preprocessing_block != NULL){

		if(strcmp(argv[1], OUTPUT_INTERFACE_NONE) == 0){
			osMutexWait (preprocessing_mutex_id, osWaitForever);
			change_outputData_interface(output_data, NONE);
			osMutexRelease (preprocessing_mutex_id);
		}
		else if (strcmp(argv[1], OUTPUT_INTERFACE_UART) == 0){
			osMutexWait (preprocessing_mutex_id, osWaitForever);
			change_outputData_interface(output_data, UART);
			osMutexRelease (preprocessing_mutex_id);
		}
		else if(strcmp(argv[1], OUTPUT_INTERFACE_SPI) == 0){
			osMutexWait (preprocessing_mutex_id, osWaitForever);
			change_outputData_interface(output_data, SPI);
			osMutexRelease (preprocessing_mutex_id);
		}
		else if (strcmp(argv[1], OUTPUT_INTERFACE_SPI_UART) == 0){
			osMutexWait (preprocessing_mutex_id, osWaitForever);
			change_outputData_interface(output_data, SPI_UART);
			osMutexRelease (preprocessing_mutex_id);
		}
		else{
			return RET_ERROR;
		}
		return RET_OK;
	}
	else{
		return RET_ERROR;
	}
	return RET_OK;
}


retval_t change_dist_cluster_size_func(uint16_t argc, char** argv){

	if(postprocessing_block != NULL){
		if(argc==2){
			uint16_t new_cluster_size = (uint16_t) atoi(argv[1]);
			osMutexWait (postprocessing_mutex_id, osWaitForever);
			postprocessing_block->distances_processing->cluster_size = new_cluster_size;
			osMutexRelease (postprocessing_mutex_id);
			return RET_OK;
		}
		else{
			return RET_ERROR;
		}
	}
	else{
		return RET_ERROR;
	}
}
retval_t change_dist_max_dist_func(uint16_t argc, char** argv){
	if(postprocessing_block != NULL){
		if(argc==2){
			float32_t new_max_dist = (float32_t) atof(argv[1]);
			osMutexWait (postprocessing_mutex_id, osWaitForever);
			postprocessing_block->distances_processing->max_distance = new_max_dist;

			postprocessing_block->distances_processing->num_distance_signal_samples = (uint16_t) new_max_dist/postprocessing_block->distances_processing->rounding_step;

			sdramFree(postprocessing_block->distances_processing->distances_signal);
			sdramFree(postprocessing_block->distances_processing->clustered_distances_signal);
			sdramFree(postprocessing_block->distances_processing->average_level_signal);
			sdramFree(postprocessing_block->distances_processing->average_snr_signal);

			postprocessing_block->distances_processing->distances_signal = sdramMalloc(postprocessing_block->distances_processing->num_distance_signal_samples * sizeof(uint16_t));
			postprocessing_block->distances_processing->clustered_distances_signal = sdramMalloc(postprocessing_block->distances_processing->num_distance_signal_samples * sizeof(uint16_t));
			postprocessing_block->distances_processing->average_level_signal = sdramMalloc(postprocessing_block->distances_processing->num_distance_signal_samples * sizeof(float32_t));
			postprocessing_block->distances_processing->average_snr_signal = sdramMalloc(postprocessing_block->distances_processing->num_distance_signal_samples * sizeof(float32_t));

			osMutexRelease (postprocessing_mutex_id);
			return RET_OK;
		}
		else{
			return RET_ERROR;
		}
	}
	else{
		return RET_ERROR;
	}

}
retval_t change_dist_round_step_func(uint16_t argc, char** argv){

	if(postprocessing_block != NULL){
		if(argc==2){
			float32_t new_round_step = (float32_t) atof(argv[1]);
			osMutexWait (postprocessing_mutex_id, osWaitForever);
			postprocessing_block->distances_processing->rounding_step = new_round_step;

			postprocessing_block->distances_processing->num_distance_signal_samples = (uint16_t) postprocessing_block->distances_processing->max_distance/new_round_step;

			sdramFree(postprocessing_block->distances_processing->distances_signal);
			sdramFree(postprocessing_block->distances_processing->clustered_distances_signal);
			sdramFree(postprocessing_block->distances_processing->average_level_signal);
			sdramFree(postprocessing_block->distances_processing->average_snr_signal);

			postprocessing_block->distances_processing->distances_signal = sdramMalloc(postprocessing_block->distances_processing->num_distance_signal_samples * sizeof(uint16_t));
			postprocessing_block->distances_processing->clustered_distances_signal = sdramMalloc(postprocessing_block->distances_processing->num_distance_signal_samples * sizeof(uint16_t));
			postprocessing_block->distances_processing->average_level_signal = sdramMalloc(postprocessing_block->distances_processing->num_distance_signal_samples * sizeof(float32_t));
			postprocessing_block->distances_processing->average_snr_signal = sdramMalloc(postprocessing_block->distances_processing->num_distance_signal_samples * sizeof(float32_t));

			osMutexRelease (postprocessing_mutex_id);
			return RET_OK;
		}
		else{
			return RET_ERROR;
		}
	}
	else{
		return RET_ERROR;
	}
}
retval_t change_post_proc_samples_func(uint16_t argc, char** argv){

	if(postprocessing_block != NULL){
		if(argc==2){
			uint16_t new_proc_samples = (uint16_t) atoi(argv[1]);
			uint16_t i;
			osMutexWait (postprocessing_mutex_id, osWaitForever);


			for(i=0; i<postprocessing_block->distances_processing->num_distances_samples; i++){

				if(postprocessing_block->distances_processing->rounded_distances_samples[i] != NULL){

					sdramFree(postprocessing_block->distances_processing->rounded_distances_samples[i]);
					postprocessing_block->distances_processing->rounded_distances_samples[i] = NULL;

				}

			}

			postprocessing_block->distances_processing->num_distances_samples = new_proc_samples;

			sdramFree(postprocessing_block->distances_processing->rounded_distances_samples);
			sdramFree(postprocessing_block->distances_processing->numDistances);

			postprocessing_block->distances_processing->rounded_distances_samples = sdramMalloc(new_proc_samples * sizeof(float32_t*));
			postprocessing_block->distances_processing->numDistances = sdramMalloc(new_proc_samples * sizeof(uint16_t));

			osMutexRelease (postprocessing_mutex_id);
			return RET_OK;
		}
		else{
			return RET_ERROR;
		}
	}
	else{
		return RET_ERROR;
	}

}


retval_t change_post_proc_threeshold_func(uint16_t argc, char** argv){

	if(postprocessing_block != NULL){
		if(argc==2){
			float32_t new_threeshold = (float32_t) atof(argv[1]);
			osMutexWait (postprocessing_mutex_id, osWaitForever);
			postprocessing_block->post_distances_selection->threeshold_value = new_threeshold;
			osMutexRelease (postprocessing_mutex_id);
			return RET_OK;
		}
		else{
			return RET_ERROR;
		}
	}
	else{
		return RET_ERROR;
	}

}


retval_t change_final_dist_min_func(uint16_t argc, char** argv){
	if(postprocessing_block != NULL){
		if(argc==2){
			float32_t new_min_dist = (float32_t) atof(argv[1]);
			osMutexWait (postprocessing_mutex_id, osWaitForever);
			postprocessing_block->final_distances->min_distance = new_min_dist;
			osMutexRelease (postprocessing_mutex_id);
			return RET_OK;
		}
		else{
			return RET_ERROR;
		}
	}
	else{
		return RET_ERROR;
	}
}

retval_t change_final_dist_res_func(uint16_t argc, char** argv){
	if(postprocessing_block != NULL){
		if(argc==2){
			float32_t new_dist_res = (float32_t) atof(argv[1]);
			osMutexWait (postprocessing_mutex_id, osWaitForever);
			postprocessing_block->final_distances->distance_res = new_dist_res;
			osMutexRelease (postprocessing_mutex_id);
			return RET_OK;
		}
		else{
			return RET_ERROR;
		}
	}
	else{
		return RET_ERROR;
	}
}

retval_t set_wifi_ssid_pass(uint16_t argc, char** argv){
	if(postprocessing_block != NULL){
		if(argc==3){

			char* outstr = pvPortMalloc(256*sizeof(char));
			osMutexWait (uart6_mutex_id, osWaitForever);
			sprintf(outstr,"quit\r");
			HAL_UART_Transmit(&huart6,(uint8_t*) outstr, strlen(outstr), 50);
			osDelay(10);
			sprintf(outstr,"quit\r");
			HAL_UART_Transmit(&huart6,(uint8_t*) outstr, strlen(outstr), 50);
			osDelay(10);
			sprintf(outstr,"file.remove(\"script2.lua\");\r\n");
			HAL_UART_Transmit(&huart6,(uint8_t*) outstr, strlen(outstr), 50);
			osDelay(10);
			sprintf(outstr,"file.open(\"script2.lua\",\"w+\");\r\n");
			HAL_UART_Transmit(&huart6,(uint8_t*) outstr, strlen(outstr), 50);
			osDelay(10);
			sprintf(outstr,"file.writeline('wifi.sta.eventMonStart()');\r\n");
			HAL_UART_Transmit(&huart6,(uint8_t*) outstr, strlen(outstr), 50);
			osDelay(10);
			sprintf(outstr,"file.writeline('wifi.sta.config(\"%s\",\"%s\", 1)');\r\n", argv[1], argv[2]);
			HAL_UART_Transmit(&huart6,(uint8_t*) outstr, strlen(outstr), 50);
			osDelay(10);
			sprintf(outstr,"file.close();\r\n");
			HAL_UART_Transmit(&huart6,(uint8_t*) outstr, strlen(outstr), 50);
			osDelay(10);
			sprintf(outstr,"dofile(\"script2.lua\");\r\n");
			HAL_UART_Transmit(&huart6,(uint8_t*) outstr, strlen(outstr), 50);

			output_data->wifi_ap_state = WIFI_AP_DISCONNECTED;
			wifi_proc_first = 1;


			osMutexRelease (uart6_mutex_id);
			vPortFree(outstr);
			return RET_OK;
		}
		else{
			return RET_ERROR;
		}
	}
	else{
		return RET_ERROR;
	}
}
