/*
 * Copyright (c) 2016, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * fft_signal.h
 *
 *  Created on: 30/5/2016
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@die.upm.es>
 *
 */
/**
 * @file fft_signal.h
 */

#ifndef APPLICATION_USER_S4BIM_DSP_CODE_PREPROCESSING_FFT_SIGNAL_H_
#define APPLICATION_USER_S4BIM_DSP_CODE_PREPROCESSING_FFT_SIGNAL_H_

#include "interpolation.h"
#include "arm_math.h"
#include "arm_const_structs.h"

#define DEFAULT_IFFT_FLAG 0
#define DEFAULT_DOBITREVERSE 1

typedef enum{
	Standard_fft,
	No_fft,
}fft_type_t;

typedef struct fft_signal{
	retval_t (*fft_func_ptr)(struct fft_signal* fft_signal, interpolation_t* interpolation);
	float32_t* fft_input_output_signal;			//After doing the FFT this will contain the complex frequency domain. Previously it is the time domain signal
	float32_t* fft_mag_output_ordered_signal;
	float32_t* fft_mag_output_signal;
	uint16_t ifft_flag;
	uint16_t fft_sample_number;
	uint16_t effective_sample_number;
	fft_type_t fft_type;
}fft_signal_t;


fft_signal_t* new_fft_signal(uint16_t fft_sample_number, uint16_t effective_sample_number, fft_type_t fft_type);
retval_t remove_fft_signal(fft_signal_t* fft_signal);

retval_t change_fft_type(fft_signal_t* fft_signal, fft_type_t new_fft_type);

retval_t change_fft_sample_number(fft_signal_t* fft_signal, uint16_t fft_samples);
retval_t change_fft_effective_sample_number(fft_signal_t* fft_signal, uint16_t fft_effective_samples);

#endif /* APPLICATION_USER_S4BIM_DSP_CODE_PREPROCESSING_FFT_SIGNAL_H_ */
