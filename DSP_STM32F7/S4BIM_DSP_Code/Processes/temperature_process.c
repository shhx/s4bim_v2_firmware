/*
 * Copyright (c) 2016, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * temperature_process.c
 *
 *  Created on: 8/7/2016
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@die.upm.es>
 *
 */
/**
 * @file temperature_process.c
 */

#include "DSP_conf.h"
#include "arm_math.h"
#include "adc.h"

#define SAMPLE_NUMBER 16
#define ADC_SEMAPHORE_TIMEOUT 250

osMutexDef (temperature_mutex);   // Declare mutex
osMutexId  temperature_mutex_id;	// Mutex ID
float32_t temperature = 0;

osSemaphoreId temperature_semaphore;                         // Semaphore ID

void temperature_thread(void const * argument){

	uint16_t i;
	temperature_mutex_id = osMutexCreate(osMutex(temperature_mutex));

	osSemaphoreDef(temperature_semaphore);
	temperature_semaphore = osSemaphoreCreate(osSemaphore(temperature_semaphore), 1);
	osSemaphoreWait (temperature_semaphore, osWaitForever);		//Wait for the semaphore		//Initially the semaphore token is captured


	while(1){

		float32_t local_temp = 0;
		uint32_t read_sample = 0;
		osDelay(250);

		for(i=0; i<SAMPLE_NUMBER; i++){
			HAL_ADC_Start_IT(&hadc1);
			osSemaphoreWait (temperature_semaphore, ADC_SEMAPHORE_TIMEOUT);
			read_sample += HAL_ADC_GetValue(&hadc1);
		}


		local_temp = (float32_t) (read_sample/SAMPLE_NUMBER);
		local_temp = (local_temp*0.000720f)/0.01f;
		osMutexWait (temperature_mutex_id, osWaitForever);
		temperature = local_temp;
		osMutexRelease (temperature_mutex_id);
	}


}


void temperature_callback(void){
	osSemaphoreRelease (temperature_semaphore);
}
