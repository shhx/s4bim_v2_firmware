/*
 * Copyright (c) 2016, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * post_oscfar.c
 *
 *  Created on: 5/7/2016
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@die.upm.es>
 *
 */
/**
 * @file post_oscfar.c
 */


#include "post_oscfar.h"

retval_t no_post_os_cfar(struct post_os_cfar* post_os_cfar, distances_processing_t* distances_processing);
retval_t do_post_os_cfar(struct post_os_cfar* post_os_cfar, distances_processing_t* distances_processing);


extern int cmpfunc_float32 (const void * a, const void * b);
extern int cmpfunc_uint16 (const void * a, const void * b);


post_os_cfar_t* new_post_os_cfar(uint16_t fft_sample_number, uint16_t effective_sample_number, post_os_cfar_type_t new_post_os_cfar_type){

	post_os_cfar_t* new_post_os_cfar = (post_os_cfar_t*) sdramMalloc(sizeof(post_os_cfar_t));
	new_post_os_cfar->effective_sample_number = effective_sample_number;

	new_post_os_cfar->signal_threshold_output = (float32_t*) sdramMalloc(new_post_os_cfar->effective_sample_number*sizeof(float32_t));

	new_post_os_cfar->window_size = DEFAULT_POST_WINDOW_SIZE;
	new_post_os_cfar->k_order = DEFAULT_POST_K_ORDER;
	new_post_os_cfar->alpha = DEFAULT_POST_OSCFAR_ALPHA;

	new_post_os_cfar->post_os_cfar_type = new_post_os_cfar_type;

	switch(new_post_os_cfar_type){
	case Standard_post_os_cfar:
		new_post_os_cfar->post_os_cfar_func_ptr = do_post_os_cfar;
		break;
	case No_post_os_cfar:
		new_post_os_cfar->post_os_cfar_func_ptr = no_post_os_cfar;
		break;
	default:
		new_post_os_cfar->post_os_cfar_func_ptr = no_post_os_cfar;
		break;
	}

	return new_post_os_cfar;
}
retval_t remove_post_os_cfar(post_os_cfar_t* post_os_cfar){
	if(post_os_cfar != NULL){
		sdramFree(post_os_cfar->signal_threshold_output);
		sdramFree(post_os_cfar);
		return RET_OK;
	}
	else{
		return RET_ERROR;
	}
}

retval_t change_post_os_cfar_type(post_os_cfar_t* post_os_cfar, post_os_cfar_type_t new_post_os_cfar_type){
	if(post_os_cfar != NULL){
		post_os_cfar->post_os_cfar_type = new_post_os_cfar_type;

		switch(new_post_os_cfar_type){
		case Standard_post_os_cfar:
			post_os_cfar->post_os_cfar_func_ptr = do_post_os_cfar;
			break;
		case No_post_os_cfar:
			post_os_cfar->post_os_cfar_func_ptr = no_post_os_cfar;
			break;
		default:
			post_os_cfar->post_os_cfar_func_ptr = no_post_os_cfar;
			break;
		}

		return RET_OK;
	}
	else{
		return RET_ERROR;
	}
}


retval_t no_post_os_cfar(struct post_os_cfar* post_os_cfar, distances_processing_t* distances_processing){
	if((post_os_cfar != NULL) && (distances_processing != NULL)){

		int32_t i = 0;
		for(i=0; i<post_os_cfar->effective_sample_number; i++){
			post_os_cfar->signal_threshold_output[i] = 0;
		}
		return RET_OK;
	}
	else{
		return RET_ERROR;
	}
}

retval_t __attribute__((section(".itcmtext"))) do_post_os_cfar(struct post_os_cfar* post_os_cfar, distances_processing_t* distances_processing){
	if((post_os_cfar != NULL) && (distances_processing != NULL)){

		int32_t i = 0;
		int32_t j = 0;
		float32_t* window = (float32_t*) sdramMalloc(post_os_cfar->window_size*sizeof(float32_t));

		for(i=0; i<post_os_cfar->effective_sample_number; i++){

			if(i<post_os_cfar->window_size/2){									//FIRST ELEMENTS
				for(j=0;j<post_os_cfar->window_size;j++){
					if(j<post_os_cfar->window_size/2){
						if((i-(post_os_cfar->window_size/2)+j)<0){
							window[j] = 0;
						}
						else{
						window[j] = (float32_t) distances_processing->clustered_distances_signal[i-(post_os_cfar->window_size/2)+j];
						}
					}
					else{
						window[j] = (float32_t) distances_processing->clustered_distances_signal[i-((post_os_cfar->window_size/2)-1)+j];
					}
				}
			}

			else if((post_os_cfar->effective_sample_number-i)<post_os_cfar->window_size/2){									//LAST ELEMENTS
				for(j=0;j<post_os_cfar->window_size;j++){
					if(j<post_os_cfar->window_size/2){
						window[j] = (float32_t) distances_processing->clustered_distances_signal[i-(post_os_cfar->window_size/2)+j];
					}
					else{
						if((i-(post_os_cfar->window_size/2)+j)<post_os_cfar->effective_sample_number){
							window[j] = (float32_t) distances_processing->clustered_distances_signal[i-(post_os_cfar->window_size/2)+j];
						}
						else{
							window[j] = 0;
						}
					}
				}
			}


			else{										//ALL OTHER ELEMENTS
				for(j=0;j<post_os_cfar->window_size;j++){
					if(j<post_os_cfar->window_size/2){
						window[j] = (float32_t) distances_processing->clustered_distances_signal[i-(post_os_cfar->window_size/2)+j];
					}
					else{
						window[j] = (float32_t) distances_processing->clustered_distances_signal[i-((post_os_cfar->window_size/2)-1)+j];
					}
				}
			}

			qsort(window, post_os_cfar->window_size, sizeof(float32_t), cmpfunc_float32);
			post_os_cfar->signal_threshold_output[i] = window[post_os_cfar->k_order-1] * post_os_cfar->alpha;
		}

		sdramFree(window);
		return RET_OK;
	}
	else{
		return RET_ERROR;
	}
}


retval_t change_post_cfar_effective_sample_number(post_os_cfar_t* post_os_cfar, uint16_t effective_sample_number){

	if(post_os_cfar != NULL){

		sdramFree(post_os_cfar->signal_threshold_output);

		post_os_cfar->effective_sample_number = effective_sample_number;

		post_os_cfar->signal_threshold_output = (float32_t*) sdramMalloc(post_os_cfar->effective_sample_number*sizeof(float32_t));

		return RET_OK;
	}
	else{
		return RET_ERROR;
	}
}
