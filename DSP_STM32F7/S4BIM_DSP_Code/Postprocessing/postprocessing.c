/*
 * Copyright (c) 2016, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * postprocessing.c
 *
 *  Created on: 5/7/2016
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@die.upm.es>
 *
 */
/**
 * @file postprocessing.c
 */

#include "postprocessing.h"

postprocessing_block_t* new_postprocessing_block(void){

	postprocessing_block_t* new_postprocessing_block = (postprocessing_block_t*) sdramMalloc(sizeof(postprocessing_block_t));
	new_postprocessing_block->distances_processing = new_distances_processing(DEFAULT_NUM_DISTANCES_SAMPLES, DEFAULT_ROUNDING_STEP, DEFAULT_MAX_DISTANCE, DEFAULT_CLUSTER_SIZE);
	new_postprocessing_block->post_os_cfar = new_post_os_cfar(new_postprocessing_block->distances_processing->num_distance_signal_samples, new_postprocessing_block->distances_processing->num_distance_signal_samples, DEFAULT_OSCFAR_TYPE);
	new_postprocessing_block->post_distances_selection = new_post_distances_selection(DEFAULT_MAX_NUM_POST_DISTANCES);
	new_postprocessing_block->final_distances = new_final_distances(DEFAULT_MAX_FINAL_DISTANCES);
	return new_postprocessing_block;
}
retval_t remove_postprocessing_block(postprocessing_block_t* postprocessing_block){
	remove_distances_processing(postprocessing_block->distances_processing);
	remove_post_distances_selection(postprocessing_block->post_distances_selection);
	remove_post_os_cfar(postprocessing_block->post_os_cfar);
	sdramFree(postprocessing_block);
	return RET_OK;
}
