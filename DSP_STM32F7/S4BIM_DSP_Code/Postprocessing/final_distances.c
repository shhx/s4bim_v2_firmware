/*
 * Copyright (c) 2016, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * final_distances.c
 *
 *  Created on: 14/7/2016
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@die.upm.es>
 *
 */
/**
 * @file final_distances.c
 */

#include "final_distances.h"

final_distances_t* new_final_distances(uint16_t max_num_distances){

	final_distances_t* new_final_distances = (final_distances_t*) sdramMalloc(sizeof(final_distances_t));

	new_final_distances->final_distances = (float32_t*) sdramMalloc(max_num_distances*sizeof(float32_t));
	new_final_distances->final_levels = (float32_t*) sdramMalloc(max_num_distances*sizeof(float32_t));

	new_final_distances->min_distance = MIN_DISTANCE;
	new_final_distances->distance_res = DISTANCE_RES;

	new_final_distances->current_num_distances = 0;
	new_final_distances->max_num_distances = max_num_distances;

	return new_final_distances;
}
retval_t remove_final_distances(final_distances_t* final_distances){
	if(final_distances != NULL){
		sdramFree(final_distances->final_distances);
		sdramFree(final_distances->final_levels);
		sdramFree(final_distances);
		final_distances = NULL;
		return RET_OK;
	}
	else{
		return RET_ERROR;
	}
}

retval_t set_max_num_final_distances(final_distances_t* final_distances, uint16_t new_max_num_final_distances){

	if(final_distances != NULL){
		sdramFree(final_distances->final_distances);
		sdramFree(final_distances->final_levels);

		final_distances->max_num_distances = new_max_num_final_distances;
		final_distances->current_num_distances = 0;

		final_distances->final_distances = (float32_t*) sdramMalloc(new_max_num_final_distances*sizeof(float32_t));
		final_distances->final_levels = (float32_t*) sdramMalloc(new_max_num_final_distances*sizeof(float32_t));

		return RET_OK;
	}
	else{
		return RET_ERROR;
	}

}

retval_t do_final_distances(final_distances_t* final_distances, post_distances_selection_t* post_distances_selection){

	if((post_distances_selection != NULL) && (final_distances != NULL)){

		uint16_t i;
		uint16_t j;
		uint16_t local_indexes[6];
		uint16_t num_local_indexes;

		uint16_t num_dists = 0;

		uint16_t prev_distances_num = post_distances_selection->current_num_distances;
		float32_t* prev_distances = post_distances_selection->distances;
		float32_t diff = 0;



		for(i=0; i<prev_distances_num; i++){
			if(prev_distances[i] > final_distances->min_distance){
				num_local_indexes = 0;
				local_indexes[num_local_indexes] = i;
				num_local_indexes++;

				for(j=1; j<5; j++){

					if((i+j) < prev_distances_num){

						diff = prev_distances[i+j] - prev_distances[i];
						if(diff < final_distances->distance_res){
							local_indexes[num_local_indexes] = i+j;
							num_local_indexes++;
						}
						else{
							break;
						}
					}
					else{

						break;
					}
				}

				if(num_local_indexes > 1){

					float32_t avg_dist = 0;
					float32_t avg_lvl = 0;

					for(j=0; j<num_local_indexes; j++){
						avg_dist += prev_distances[local_indexes[j]];
						avg_lvl += post_distances_selection->levels[local_indexes[j]];
					}

					final_distances->final_distances[num_dists] = avg_dist/num_local_indexes;
					final_distances->final_levels[num_dists] = avg_lvl/num_local_indexes;
					num_dists++;

					i += num_local_indexes;
				}

				else{

					final_distances->final_distances[num_dists] = prev_distances[i];
					final_distances->final_levels[num_dists] = post_distances_selection->levels[i];
					num_dists++;

				}

				if(num_dists >= final_distances->max_num_distances){
					break;
				}

			}

		}

		final_distances->current_num_distances = num_dists;

		return RET_OK;
	}
	else{
		return RET_ERROR;
	}

}
