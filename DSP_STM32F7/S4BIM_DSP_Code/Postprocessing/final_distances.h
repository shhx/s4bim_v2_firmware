/*
 * Copyright (c) 2016, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * final_distances.h
 *
 *  Created on: 14/7/2016
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@die.upm.es>
 *
 */
/**
 * @file final_distances.h
 */

#ifndef APPLICATION_USER_S4BIM_DSP_CODE_POSTPROCESSING_FINAL_DISTANCES_H_
#define APPLICATION_USER_S4BIM_DSP_CODE_POSTPROCESSING_FINAL_DISTANCES_H_

#include "DSP_conf.h"
#include "arm_math.h"
#include "post_distances_selection.h"

#define MIN_DISTANCE	1.5f
#define DISTANCE_RES	2.5f

typedef struct final_distances{
	float32_t* final_distances;
	float32_t* final_levels;
	uint16_t current_num_distances;
	uint16_t max_num_distances;
	float32_t min_distance;
	float32_t distance_res;
}final_distances_t;


final_distances_t* new_final_distances(uint16_t max_num_distances);
retval_t remove_final_distances(final_distances_t* final_distances);

retval_t set_max_num_final_distances(final_distances_t* final_distances, uint16_t new_max_num_final_distances);

retval_t do_final_distances(final_distances_t* final_distances, post_distances_selection_t* post_distances_selection);


#endif /* APPLICATION_USER_S4BIM_DSP_CODE_POSTPROCESSING_FINAL_DISTANCES_H_ */
