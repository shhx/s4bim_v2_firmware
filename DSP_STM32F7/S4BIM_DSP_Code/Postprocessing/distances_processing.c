/*
 * Copyright (c) 2016, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * distances_processing.c
 *
 *  Created on: 5/7/2016
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@die.upm.es>
 *
 */
/**
 * @file distances_processing.c
 */

#include "distances_processing.h"

extern osMutexId  data_storage_mutex_id;

distances_processing_t* new_distances_processing(uint16_t num_distances_samples, float32_t rounding_step, float32_t max_distance, uint16_t cluster_size){

	distances_processing_t* new_distances_processing = (distances_processing_t*) sdramMalloc(sizeof(distances_processing_t));
	uint16_t i;

	new_distances_processing->max_distance = max_distance;
	new_distances_processing->rounding_step = rounding_step;
	new_distances_processing->num_distances_samples = num_distances_samples;
	new_distances_processing->cluster_size = cluster_size;
	new_distances_processing->prev_index = 0;
	new_distances_processing->current_index = 0;
	new_distances_processing->data_ready_flag = NO_DATA_READY;
	new_distances_processing->output_data_ready_flag = NO_DATA_READY;

	new_distances_processing->rounded_distances_samples = sdramMalloc(num_distances_samples * sizeof(float32_t*));

	new_distances_processing->numDistances = sdramMalloc(num_distances_samples * sizeof(uint16_t));

	new_distances_processing->num_distance_signal_samples = (uint16_t) max_distance/rounding_step;

	new_distances_processing->distances_signal = sdramMalloc(new_distances_processing->num_distance_signal_samples * sizeof(uint16_t));
	new_distances_processing->clustered_distances_signal = sdramMalloc(new_distances_processing->num_distance_signal_samples * sizeof(uint16_t));
	new_distances_processing->average_level_signal = sdramMalloc(new_distances_processing->num_distance_signal_samples * sizeof(float32_t));
	new_distances_processing->average_snr_signal = sdramMalloc(new_distances_processing->num_distance_signal_samples * sizeof(float32_t));
	new_distances_processing->noise_lvl = 0;

	for(i=0; i<num_distances_samples; i++){
		new_distances_processing->rounded_distances_samples[i] = NULL;
	}

	return new_distances_processing;

}

retval_t remove_distances_processing(distances_processing_t* distances_processing){

	uint16_t i;

	for (i=0 ; i<distances_processing->num_distances_samples; i++){

		if(distances_processing->rounded_distances_samples[i] != NULL){
			sdramFree(distances_processing->rounded_distances_samples[i]);
			distances_processing->rounded_distances_samples[i] = NULL;
		}

	}
	sdramFree(distances_processing->rounded_distances_samples);
	distances_processing->rounded_distances_samples = NULL;

	sdramFree(distances_processing->numDistances);
	distances_processing->numDistances = NULL;

	sdramFree(distances_processing->distances_signal);
	distances_processing->distances_signal = NULL;

	sdramFree(distances_processing->clustered_distances_signal);
	distances_processing->clustered_distances_signal = NULL;

	sdramFree(distances_processing->average_level_signal);
	distances_processing->average_level_signal = NULL;

	sdramFree(distances_processing->average_snr_signal);
	distances_processing->average_snr_signal = NULL;

	sdramFree(distances_processing);
	distances_processing = NULL;

	return RET_OK;
}

retval_t data_ready_check(distances_processing_t* distances_processing, data_storage_t* data_storage){

	if(data_storage->current_index < data_storage->buffer_size){

		if((data_storage->current_index > distances_processing->prev_index)){
			if(((data_storage->current_index+1)%distances_processing->num_distances_samples) == 0){

				if((data_storage->current_index == data_storage->buffer_size-1) && (distances_processing->prev_index == 0)){
					return RET_OK;
				}

				distances_processing->data_ready_flag = DATA_READY;

				distances_processing->current_index = data_storage->current_index;

				return RET_OK;
			}
			else{

				return RET_OK;
			}
		}
	}
	return RET_OK;
}

float32_t level_test[60];
float32_t snr_test[60];
float32_t dist_test[60];

retval_t round_samples(distances_processing_t* distances_processing, data_storage_t* data_storage){

	if(distances_processing->data_ready_flag == DATA_READY){
		int16_t i, j;
		uint16_t num_dist;

		uint16_t prev_index = distances_processing->prev_index;

		//Init to 0 the distances signals
		for(i=0; i<distances_processing->num_distance_signal_samples; i++){
			distances_processing->distances_signal[i] = 0;
			distances_processing->clustered_distances_signal[i] = 0;
			distances_processing->average_level_signal[i] = 0;
			distances_processing->average_snr_signal[i] = 0;
		}
		distances_processing->noise_lvl = 0;

		for(i=0; i<distances_processing->num_distances_samples; i++){
			osMutexWait (data_storage_mutex_id, osWaitForever);
			num_dist = data_storage->numDistances_circular_buffer[prev_index+i];
			osMutexRelease (data_storage_mutex_id);

			if(distances_processing->rounded_distances_samples[i] == NULL){
				distances_processing->rounded_distances_samples[i] = (float32_t*) sdramMalloc(num_dist*sizeof(float32_t));
			}
			else{
				if(distances_processing->numDistances[i] != num_dist){
					sdramFree(distances_processing->rounded_distances_samples[i]);
					distances_processing->rounded_distances_samples[i] = (float32_t*) sdramMalloc(num_dist*sizeof(float32_t));
				}
			}


			for(j=0; j<num_dist; j++){
				osMutexWait (data_storage_mutex_id, osWaitForever);
				distances_processing->rounded_distances_samples[i][j] = roundf(data_storage->distances_circular_buffer[prev_index+i][j]/distances_processing->rounding_step)*distances_processing->rounding_step;

				//NOW FILL DISTANCES SIGNAL
				uint16_t distance_signal_index = (uint16_t) ((distances_processing->rounded_distances_samples[i][j]+0.1f)/distances_processing->rounding_step);

				if(distance_signal_index < distances_processing->num_distance_signal_samples){
					distances_processing->distances_signal[distance_signal_index]++;

					distances_processing->average_level_signal[distance_signal_index] += data_storage->levels_circular_buffer[prev_index+i][j];
					distances_processing->average_snr_signal[distance_signal_index] += data_storage->snr_circular_buffer[prev_index+i][j];
				}

				osMutexRelease (data_storage_mutex_id);
			}


			distances_processing->numDistances[i] = num_dist;

		}

		//NOW CREATE THE CLUSTERED SIGNAL

		uint16_t average_noise_count = 0;

		for(i=0; i<distances_processing->num_distance_signal_samples; i++){
			uint16_t point_value = 0;

			for(j=1; j<=distances_processing->cluster_size; j++){
				if((i+j)< distances_processing->num_distance_signal_samples){
				point_value += distances_processing->distances_signal[i+j];
				}
				if((i-j) >= 0){
				point_value += distances_processing->distances_signal[i-j];
				}
			}
			point_value += distances_processing->distances_signal[i];
			distances_processing->clustered_distances_signal[i] = point_value;

			if(distances_processing->distances_signal[i] != 0){
				distances_processing->average_level_signal[i] = (float32_t) distances_processing->average_level_signal[i]/distances_processing->distances_signal[i];
				distances_processing->average_snr_signal[i] = (float32_t) distances_processing->average_snr_signal[i]/distances_processing->distances_signal[i];
			}
			else if (distances_processing->clustered_distances_signal[i] != 0){
				if(((i+distances_processing->cluster_size) < distances_processing->num_distance_signal_samples) && ((i-distances_processing->cluster_size) >= 0)){
					if( distances_processing->distances_signal[i+1] != 0){

						distances_processing->average_level_signal[i] = (float32_t) distances_processing->average_level_signal[i+1]/distances_processing->distances_signal[i+1];
						distances_processing->average_snr_signal[i] = (float32_t) distances_processing->average_snr_signal[i+1]/distances_processing->distances_signal[i+1];

					}
					else if(distances_processing->distances_signal[i-1] != 0){

						distances_processing->average_level_signal[i] = (float32_t) distances_processing->average_level_signal[i-1]/distances_processing->distances_signal[i-1];
						distances_processing->average_snr_signal[i] = (float32_t) distances_processing->average_snr_signal[i-1]/distances_processing->distances_signal[i-1];

					}
				}

			}

			if(distances_processing->average_snr_signal[i] != 0){
				distances_processing->noise_lvl += distances_processing->average_level_signal[i]/distances_processing->average_snr_signal[i];
				average_noise_count++;
			}

			level_test[i] = distances_processing->average_level_signal[i];
			snr_test[i] = distances_processing->average_snr_signal[i];
			dist_test[i] = distances_processing->clustered_distances_signal[i];
		}

		distances_processing->noise_lvl = distances_processing->noise_lvl/average_noise_count;
		return RET_OK;
	}
	else{
		return RET_ERROR;
	}
}
