/*
 * Copyright (c) 2016, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * data_storage.c
 *
 *  Created on: 30/6/2016
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@die.upm.es>
 *
 */
/**
 * @file data_storage.c
 */

#include "data_storage.h"

data_storage_t* new_data_storage(uint16_t buffer_size){

	data_storage_t* new_data_storage = (data_storage_t*) sdramMalloc(sizeof(data_storage_t));
	uint16_t i;

	new_data_storage->buffer_size = buffer_size;
	new_data_storage->current_index = 65535;

	new_data_storage->distances_circular_buffer = sdramMalloc(buffer_size * sizeof(float32_t*));
	new_data_storage->levels_circular_buffer = sdramMalloc(buffer_size * sizeof(float32_t*));
	new_data_storage->snr_circular_buffer = sdramMalloc(buffer_size * sizeof(float32_t*));

	new_data_storage->timestamps_circular_buffer = sdramMalloc(buffer_size * sizeof(uint64_t));
	new_data_storage->numDistances_circular_buffer = sdramMalloc(buffer_size * sizeof(uint16_t));

	for(i=0; i<buffer_size; i++){
		new_data_storage->distances_circular_buffer[i] = NULL;
		new_data_storage->levels_circular_buffer[i] = NULL;
		new_data_storage->snr_circular_buffer[i] = NULL;
	}

	return new_data_storage;
}
retval_t remove_data_storage(data_storage_t* data_storage){

	uint16_t i;

	for (i=0 ; i<data_storage->buffer_size; i++){

		if(data_storage->distances_circular_buffer[i] != NULL){
			sdramFree(data_storage->distances_circular_buffer[i]);
			data_storage->distances_circular_buffer[i] = NULL;
		}
		if(data_storage->levels_circular_buffer[i] != NULL){
			sdramFree(data_storage->levels_circular_buffer[i]);
			data_storage->levels_circular_buffer[i] = NULL;
		}
		if(data_storage->snr_circular_buffer[i] != NULL){
			sdramFree(data_storage->snr_circular_buffer[i]);
			data_storage->snr_circular_buffer[i] = NULL;
		}

	}

	sdramFree(data_storage->distances_circular_buffer);
	data_storage->distances_circular_buffer = NULL;

	sdramFree(data_storage->levels_circular_buffer);
	data_storage->levels_circular_buffer = NULL;

	sdramFree(data_storage->snr_circular_buffer);
	data_storage->snr_circular_buffer = NULL;

	sdramFree(data_storage->timestamps_circular_buffer);
	data_storage->timestamps_circular_buffer = NULL;

	sdramFree(data_storage->numDistances_circular_buffer);
	data_storage->numDistances_circular_buffer = NULL;

	data_storage->buffer_size = 0;
	data_storage->current_index = 0;

	return RET_OK;

}

retval_t store_data(data_storage_t* data_storage, preprocessing_block_t* preprocessing, uint64_t timestamp){

	uint16_t index;
	uint16_t num_dist;
	uint16_t i;

	if(data_storage->current_index < (data_storage->buffer_size-1)){
		data_storage->current_index++;
	}
	else{
		data_storage->current_index = 0;
	}

	index = data_storage->current_index;
	num_dist = preprocessing->distances_selection->current_num_distances;


	if(data_storage->distances_circular_buffer[index] == NULL){
		data_storage->distances_circular_buffer[index] = (float32_t*) sdramMalloc(num_dist*sizeof(float32_t));
	}
	else{
		if(data_storage->numDistances_circular_buffer[index] != num_dist){
			sdramFree(data_storage->distances_circular_buffer[index]);
			data_storage->distances_circular_buffer[index] = (float32_t*) sdramMalloc(num_dist*sizeof(float32_t));
		}
	}

	if(data_storage->levels_circular_buffer[index] == NULL){
		data_storage->levels_circular_buffer[index] = (float32_t*) sdramMalloc(num_dist*sizeof(float32_t));
	}
	else{
		if(data_storage->numDistances_circular_buffer[index] != num_dist){
			sdramFree(data_storage->levels_circular_buffer[index]);
			data_storage->levels_circular_buffer[index] = (float32_t*) sdramMalloc(num_dist*sizeof(float32_t));
		}
	}

	if(data_storage->snr_circular_buffer[index] == NULL){
		data_storage->snr_circular_buffer[index] = (float32_t*) sdramMalloc(num_dist*sizeof(float32_t));
	}
	else{
		if(data_storage->numDistances_circular_buffer[index] != num_dist){
			sdramFree(data_storage->snr_circular_buffer[index]);
			data_storage->snr_circular_buffer[index] = (float32_t*) sdramMalloc(num_dist*sizeof(float32_t));
		}
	}

	for(i=0; i<num_dist; i++){
		data_storage->distances_circular_buffer[index][i] = preprocessing->distances_selection->distances[i];
		data_storage->levels_circular_buffer[index][i] = preprocessing->distances_selection->distances_levels[i];
		data_storage->snr_circular_buffer[index][i] = preprocessing->distances_selection->distances_snr[i];
	}


	data_storage->timestamps_circular_buffer[index] = timestamp;
	data_storage->numDistances_circular_buffer[index] = num_dist;

	return RET_OK;
}
