/*
 * Copyright (c) 2016, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * distances_processing.h
 *
 *  Created on: 5/7/2016
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@die.upm.es>
 *
 */
/**
 * @file distances_processing.h
 */

#ifndef APPLICATION_USER_S4BIM_DSP_CODE_POSTPROCESSING_DISTANCES_PROCESSING_H_
#define APPLICATION_USER_S4BIM_DSP_CODE_POSTPROCESSING_DISTANCES_PROCESSING_H_

#include "DSP_conf.h"
#include "arm_math.h"
#include "data_storage.h"

#define DEFAULT_NUM_DISTANCES_SAMPLES	32
#define	DEFAULT_ROUNDING_STEP			0.5f
#define DEFAULT_MAX_DISTANCE			30
#define DEFAULT_CLUSTER_SIZE			1

typedef enum{
	DATA_READY,
	NO_DATA_READY,
}data_ready_t;

typedef struct distances_processing{

	float32_t** rounded_distances_samples;
	uint16_t*	numDistances;
	uint16_t 	num_distances_samples;
	uint16_t 	prev_index;
	uint16_t 	current_index;
	uint16_t*	distances_signal;
	uint16_t*	clustered_distances_signal;
	float32_t*	average_level_signal;
	float32_t*	average_snr_signal;
	float32_t	noise_lvl;
	uint16_t	cluster_size;
	uint16_t	num_distance_signal_samples;
	float32_t 	rounding_step;
	float32_t 	max_distance;
	data_ready_t data_ready_flag;
	data_ready_t output_data_ready_flag;
}distances_processing_t;


distances_processing_t* new_distances_processing(uint16_t num_distances_samples, float32_t rounding_step, float32_t max_distance, uint16_t cluster_size);
retval_t remove_distances_processing(distances_processing_t* distances_processing);

retval_t data_ready_check(distances_processing_t* distances_processing, data_storage_t* data_storage);
retval_t round_samples(distances_processing_t* distances_processing, data_storage_t* data_storage);

#endif /* APPLICATION_USER_S4BIM_DSP_CODE_POSTPROCESSING_DISTANCES_PROCESSING_H_ */
