/*
 * Copyright (c) 2016, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * post_distance_selection.h
 *
 *  Created on: 6/7/2016
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@die.upm.es>
 *
 */
/**
 * @file post_distance_selection.h
 */

#ifndef APPLICATION_USER_S4BIM_DSP_CODE_POSTPROCESSING_POST_DISTANCES_SELECTION_H_
#define APPLICATION_USER_S4BIM_DSP_CODE_POSTPROCESSING_POST_DISTANCES_SELECTION_H_

#include "DSP_conf.h"
#include "arm_math.h"
#include "distances_processing.h"
#include "post_oscfar.h"

#define DEFAULT_MIN_THRESHOLD_VALUE	9

typedef struct post_distances_selection{
	int16_t* distances_positions;
	float32_t* distances;
	float32_t* levels;
	uint16_t current_num_distances;
	uint16_t max_num_distances;
	float32_t threeshold_value;
}post_distances_selection_t;


post_distances_selection_t* new_post_distances_selection(uint16_t max_num_distances);
retval_t remove_post_distances_selection(post_distances_selection_t* post_distances_selection);

retval_t set_max_num_post_distances(post_distances_selection_t* post_distances_selection, uint16_t new_max_num_distances);

retval_t do_post_distances_selection(post_distances_selection_t* post_distances_selection, distances_processing_t* distances_processing, post_os_cfar_t* os_cfar);


#endif /* APPLICATION_USER_S4BIM_DSP_CODE_POSTPROCESSING_POST_DISTANCES_SELECTION_H_ */
