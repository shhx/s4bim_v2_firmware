/*
 * Copyright (c) 2016, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * post_distance_selection.c
 *
 *  Created on: 6/7/2016
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@die.upm.es>
 *
 */
/**
 * @file post_distance_selection.c
 */

#include "post_distances_selection.h"


post_distances_selection_t* new_post_distances_selection(uint16_t max_num_distances){

	post_distances_selection_t* new_post_distances_selection = (post_distances_selection_t*) sdramMalloc(sizeof(post_distances_selection_t));

	new_post_distances_selection->distances_positions = (int16_t*) sdramMalloc(max_num_distances*sizeof(int16_t));
	new_post_distances_selection->distances = (float32_t*) sdramMalloc(max_num_distances*sizeof(float32_t));
	new_post_distances_selection->levels = (float32_t*) sdramMalloc(max_num_distances*sizeof(float32_t));
	new_post_distances_selection->threeshold_value = DEFAULT_MIN_THRESHOLD_VALUE;

	new_post_distances_selection->current_num_distances = 0;
	new_post_distances_selection->max_num_distances = max_num_distances;

	return new_post_distances_selection;
}


retval_t remove_post_distances_selection(post_distances_selection_t* post_distances_selection){
	if(post_distances_selection != NULL){
		sdramFree(post_distances_selection->distances_positions);
		sdramFree(post_distances_selection->distances);
		sdramFree(post_distances_selection->levels);
		sdramFree(post_distances_selection);
		post_distances_selection = NULL;
		return RET_OK;
	}
	else{
		return RET_ERROR;
	}
}

retval_t set_max_num_post_distances(post_distances_selection_t* post_distances_selection, uint16_t new_max_num_distances){
	if(post_distances_selection != NULL){
		sdramFree(post_distances_selection->distances_positions);
		sdramFree(post_distances_selection->distances);
		sdramFree(post_distances_selection->levels);

		post_distances_selection->max_num_distances = new_max_num_distances;
		post_distances_selection->current_num_distances = 0;

		post_distances_selection->distances_positions = (int16_t*) sdramMalloc(new_max_num_distances*sizeof(int16_t));
		post_distances_selection->distances = (float32_t*) sdramMalloc(new_max_num_distances*sizeof(float32_t));
		post_distances_selection->levels = (float32_t*) sdramMalloc(new_max_num_distances*sizeof(float32_t));

		return RET_OK;
	}
	else{
		return RET_ERROR;
	}
}


retval_t do_post_distances_selection(post_distances_selection_t* post_distances_selection, distances_processing_t* distances_processing, post_os_cfar_t* os_cfar){

	if((post_distances_selection != NULL) && (distances_processing != NULL) && (os_cfar != NULL)){
		int16_t i = 0;
		uint32_t sizeCount = 0;
		float32_t* tempDist = (float32_t*) sdramMalloc(distances_processing->num_distance_signal_samples*sizeof(float32_t));

		uint16_t signalSize = distances_processing->num_distance_signal_samples;
		float32_t* threesholdInput = os_cfar->signal_threshold_output;
		uint16_t* signalInput = (uint16_t*) distances_processing->clustered_distances_signal;

		for(i=0; i<signalSize; i++){

			if((signalInput[i] > threesholdInput[i])  && (signalInput[i] > post_distances_selection->threeshold_value) && (i>2)){
				tempDist[i] = signalInput[i];
			}
			else{
				tempDist[i] = 0;
			}

		}

		for(i=0; i<signalSize-2; i++){

			if (tempDist[i] != 0){
				if(tempDist[i+1] != 0){
					if(tempDist[i+1]>tempDist[i]){
						post_distances_selection->distances_positions[sizeCount] = (i+1);
						post_distances_selection->levels[sizeCount] = distances_processing->average_level_signal[i+1];
						sizeCount++;
						if(tempDist[i+2] != 0){
							i++;
							if(tempDist[i+2] != 0){
								i++;
							}
						}
					}
					else{
						post_distances_selection->distances_positions[sizeCount] = i;
						post_distances_selection->levels[sizeCount] = distances_processing->average_level_signal[i];
						sizeCount++;
						if(tempDist[i+2] != 0){
							i++;
							if(tempDist[i+2] != 0){
								i++;
							}
						}
					}
					i++;
				}
				else{
					post_distances_selection->distances_positions[sizeCount] = i;
					post_distances_selection->levels[sizeCount] = distances_processing->average_level_signal[i];
					sizeCount++;
				}
			}

			if(sizeCount >= post_distances_selection->max_num_distances){
				break;
			}

		}

		post_distances_selection->current_num_distances = sizeCount;

		for(i=0; i<sizeCount; i++){
			post_distances_selection->distances[i] = post_distances_selection->distances_positions[i] * distances_processing->rounding_step;
		}

		sdramFree(tempDist);
		return RET_OK;
	}
	else{
		return RET_ERROR;
	}
}
