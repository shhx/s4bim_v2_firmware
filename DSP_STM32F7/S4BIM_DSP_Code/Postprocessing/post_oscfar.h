/*
 * Copyright (c) 2016, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * post_oscfar.h
 *
 *  Created on: 5/7/2016
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@die.upm.es>
 *
 */
/**
 * @file post_oscfar.h
 */

#ifndef APPLICATION_USER_S4BIM_DSP_CODE_POSTPROCESSING_POST_OSCFAR_H_
#define APPLICATION_USER_S4BIM_DSP_CODE_POSTPROCESSING_POST_OSCFAR_H_

#include "DSP_conf.h"
#include "distances_processing.h"
#include <stdlib.h>

#define DEFAULT_POST_WINDOW_SIZE		6
#define DEFAULT_POST_K_ORDER			5
#define DEFAULT_POST_OSCFAR_ALPHA	0.98f

typedef enum{
	Standard_post_os_cfar,
	No_post_os_cfar,
}post_os_cfar_type_t;

typedef struct post_os_cfar{
	retval_t (*post_os_cfar_func_ptr)(struct post_os_cfar* post_os_cfar, distances_processing_t* distances_processing);
	float32_t* signal_threshold_output;
	uint16_t effective_sample_number;
	uint16_t window_size;
	uint16_t k_order;
	float32_t alpha;
	post_os_cfar_type_t post_os_cfar_type;
}post_os_cfar_t;


post_os_cfar_t* new_post_os_cfar(uint16_t input_sample_number, uint16_t effective_sample_number, post_os_cfar_type_t new_post_os_cfar_type);
retval_t remove_post_os_cfar(post_os_cfar_t* post_os_cfar);

retval_t change_post_os_cfar_type(post_os_cfar_t* post_os_cfar, post_os_cfar_type_t new_post_os_cfar_type);

retval_t change_post_cfar_effective_sample_number(post_os_cfar_t* post_os_cfar, uint16_t effective_sample_number);

#endif /* APPLICATION_USER_S4BIM_DSP_CODE_POSTPROCESSING_POST_OSCFAR_H_ */
