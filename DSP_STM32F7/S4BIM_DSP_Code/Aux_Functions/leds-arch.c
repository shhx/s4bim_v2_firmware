/*
 * Copyright (c) 2016, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * leds-arch.c
 *
 *  Created on: 26/5/2016
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@die.upm.es>
 *
 */
/**
 * @file leds-arch.c
 */

#include "DSP_conf.h"
#include "leds.h"
#include "gpio.h"

void leds_arch_init(void)
{
	GPIO_InitTypeDef GPIO_InitStruct;
	/* set gpio func_sel to gpio (3) */
	  __GPIOB_CLK_ENABLE();
	  /*Configure GPIO pins : PB4 PB5 */
	  GPIO_InitStruct.Pin = LEDS_BLUE_PIN|LEDS_GREEN_PIN;
	  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	  GPIO_InitStruct.Pull = GPIO_NOPULL;
	  GPIO_InitStruct.Speed = GPIO_SPEED_LOW;
	  HAL_GPIO_Init(LEDS_PORT, &GPIO_InitStruct);
}

unsigned char leds_arch_get(void)
{

	return((HAL_GPIO_ReadPin(LEDS_PORT, LEDS_BLUE_PIN) ? LEDS_BLUE : 0)
			|(HAL_GPIO_ReadPin(LEDS_PORT, LEDS_GREEN_PIN) ? LEDS_GREEN: 0));

}

void leds_arch_set(unsigned char leds)
{

	if(leds & LEDS_GREEN) { HAL_GPIO_WritePin(LEDS_PORT, LEDS_GREEN_PIN, GPIO_PIN_SET); } else {  HAL_GPIO_WritePin(LEDS_PORT, LEDS_GREEN_PIN, GPIO_PIN_RESET);}
	if(leds & LEDS_BLUE)  { HAL_GPIO_WritePin(LEDS_PORT, LEDS_BLUE_PIN, GPIO_PIN_SET); } else {  HAL_GPIO_WritePin(LEDS_PORT, LEDS_BLUE_PIN, GPIO_PIN_RESET);}
}



